#!/bin/bash

usage()
{
	echo "USAGE : iblog.sh [ status | start | stop | clean_stop | kill | restart | restart_graceful ]"
}

if [ $# -eq 0 ] ; then
	usage
	exit 9
fi

case $1 in
	status)
		ps -f -u $USER | grep "iblog -f iblog.conf" | grep -v grep | awk '{if($3=="1")print $0}'
		ps -f -u $USER | grep "iblog -f iblog.conf" | grep -v grep | awk '{if($3!="1")print $0}'
		;;
	start)
		if [ x"$IBP_LOGDIR" != x"" ] ; then
			if [ ! -d $IBP_LOGDIR ] ; then
				mkdir -p $IBP_LOGDIR
			fi
		else
			if [ ! -d $HOME/log ] ; then
				mkdir -p $HOME/log 
			fi
		fi
		
		PID=`ps -f -u $USER | grep "iblog -f iblog.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" != x"" ] ; then
			echo "*** WARN : iblog existed"
			exit 1
		fi
		iblog -f iblog.conf -a start
		NRET=$?
		if [ $NRET -ne 0 ] ; then
			echo "*** ERROR : iblog start error[$NRET]"
			exit 1
		fi
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "iblog -f iblog.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" != x"" ] ; then
				break
			fi
		done
		echo "iblog start ok"
		iblog.sh status
		;;
	stop)
		iblog.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "iblog -f iblog.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : iblog not existed"
			exit 1
		fi
		kill $PID
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "iblog -f iblog.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" = x"" ] ; then
				break
			fi
		done
		echo "iblog end ok"
		;;
	kill)
		iblog.sh status
		killall -9 iblog
		;;
	restart)
		iblog.sh stop
		sleep 1
		iblog.sh start
		;;
	*)
		usage
		;;
esac

