usage()
{
	echo "USAGE : ibma_cluster.do [ (node) | ALL ] [ start | stop | restart | kill | status | cmd ]"
}

if [ $# -eq 0 ] ; then
	usage
	exit 7 ;
fi

NODE=$1
ACTION=$2
shift
shift

function rsh_call()
{
	H=$1
	U=$2
	CMD=$3
	if [ x"$U" != x"" ] && [ x"$H" != x"" ] ; then
		echo "$U@$H $ $CMD"
		rsh -l $U $H ". ~/.bash_profile;$CMD"
	fi
}

if [ ! -f $HOME/etc/ibma_cluster.conf ] ; then
	echo "ERROR : $HOME/etc/ibma_cluster.conf not found"
	exit 1
fi

case $ACTION in
	start)
		exec 3<$HOME/etc/ibma_cluster.conf
		while read -u3 LINE ; do
			echo $LINE | grep -E "^$|^#" >/dev/null 2>&1
			if [ $? -eq 0 ] ; then
				continue
			fi
			N=`echo $LINE | awk '{print $1}'`
			H=`echo $LINE | awk '{print $2}'`
			U=`echo $LINE | awk '{print $3}'`
			if [ x"$NODE" = x"ALL" ] || [ x"$NODE" = x"$N" ] ; then
				rsh_call "$H" "$U" "ibma.sh start"
			fi
		done
		exec 3<&-
		;;
	stop)
		exec 3<$HOME/etc/ibma_cluster.conf
		while read -u3 LINE ; do
			echo $LINE | grep -E "^$|^#" >/dev/null 2>&1
			if [ $? -eq 0 ] ; then
				continue
			fi
			N=`echo $LINE | awk '{print $1}'`
			H=`echo $LINE | awk '{print $2}'`
			U=`echo $LINE | awk '{print $3}'`
			if [ x"$NODE" = x"ALL" ] || [ x"$NODE" = x"$N" ] ; then
				rsh_call "$H" "$U" "ibma.sh stop"
			fi
		done
		exec 3<&-
		;;
	restart)
		ibma_cluster.sh stop
		sleep 1
		ibma_cluster.sh status
		ibma_cluster.sh start $*
		;;
	kill)
		tac $HOME/etc/ibma_cluster.conf > $HOME/etc/ibma_cluster.conf.tac
		exec 3<$HOME/etc/ibma_cluster.conf.tac
		while read -u3 LINE ; do
			echo $LINE | grep -E "^$|^#" >/dev/null 2>&1
			if [ $? -eq 0 ] ; then
				continue
			fi
			N=`echo $LINE | awk '{print $1}'`
			H=`echo $LINE | awk '{print $2}'`
			U=`echo $LINE | awk '{print $3}'`
			if [ x"$NODE" = x"ALL" ] || [ x"$NODE" = x"$N" ] ; then
				rsh_call "$H" "$U" "ibma.sh kill"
			fi
		done
		exec 3<&-
		rm -f $HOME/etc/ibma_cluster.conf.tac
		;;
	status)
		exec 3<$HOME/etc/ibma_cluster.conf
		while read -u3 LINE ; do
			echo $LINE | grep -E "^$|^#" >/dev/null 2>&1
			if [ $? -eq 0 ] ; then
				continue
			fi
			N=`echo $LINE | awk '{print $1}'`
			H=`echo $LINE | awk '{print $2}'`
			U=`echo $LINE | awk '{print $3}'`
			if [ x"$NODE" = x"ALL" ] || [ x"$NODE" = x"$N" ] ; then
				rsh_call "$H" "$U" "ibma.sh status"
			fi
		done
		exec 3<&-
		;;
	cmd)
		exec 3<$HOME/etc/ibma_cluster.conf
		while read -u3 LINE ; do
			echo $LINE | grep -E "^$|^#" >/dev/null 2>&1
			if [ $? -eq 0 ] ; then
				continue
			fi
			N=`echo $LINE | awk '{print $1}'`
			H=`echo $LINE | awk '{print $2}'`
			U=`echo $LINE | awk '{print $3}'`
			if [ x"$NODE" = x"ALL" ] || [ x"$NODE" = x"$N" ] ; then
				rsh_call "$H" "$U" "$*"
			fi
		done
		exec 3<&-
		;;
	*)
		usage
		;;
esac

