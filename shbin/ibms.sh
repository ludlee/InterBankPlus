#!/bin/bash

usage()
{
	echo "USAGE : ibms.sh [ status | start | stop | kill | restart | reload_log | show_nodes | show_apps | show_node_nodes_rlt | show_node_apps_rlt ]"
}

if [ $# -eq 0 ] ; then
	usage
	exit 9
fi

case $1 in
	status)
		ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3=="1")print $0}'
		ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3!="1")print $0}'
		;;
	start)
		if [ x"$IBP_LOGDIR" != x"" ] ; then
			if [ ! -d $IBP_LOGDIR ] ; then
				mkdir -p $IBP_LOGDIR
			fi
		else
			if [ ! -d $HOME/log ] ; then
				mkdir -p $HOME/log 
			fi
		fi
		
		PID=`ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" != x"" ] ; then
			echo "*** WARN : ibms existed"
			exit 1
		fi
		ibms -f ibms.conf -a start
		NRET=$?
		if [ $NRET -ne 0 ] ; then
			echo "*** ERROR : ibms start error[$NRET]"
			exit 1
		fi
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" != x"" ] ; then
				break
			fi
		done
		echo "ibms start ok"
		ibms.sh status
		;;
	stop)
		ibms.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibms not existed"
			exit 1
		fi
		kill $PID
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" = x"" ] ; then
				break
			fi
		done
		echo "ibms end ok"
		;;
	kill)
		ibms.sh status
		killall -9 ibms
		;;
	restart)
		ibms.sh stop
		sleep 1
		ibms.sh start
		;;
	reload_log)
		ibms.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibms -f ibms.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibms not existed"
			exit 1
		fi
		kill -USR1 $PID
		;;
	show_nodes)
		ibms -f ibms.conf -s nodes
		;;
	show_apps)
		ibms -f ibms.conf -s apps
		;;
	show_node_nodes_rlt)
		ibms -f ibms.conf -s node_nodes_rlt
		;;
	show_node_apps_rlt)
		ibms -f ibms.conf -s node_apps_rlt
		;;
	*)
		usage
		;;
esac

