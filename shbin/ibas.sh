#!/bin/bash

usage()
{
	echo "USAGE : ibas.sh [ status | start | stop | kill | restart | restart_graceful | reload_log ]"
}

if [ $# -eq 0 ] ; then
	usage
	exit 9
fi

case $1 in
	status)
		ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3=="1")print $0}'
		ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3!="1")print $0}'
		;;
	start)
		if [ x"$IBP_LOGDIR" != x"" ] ; then
			if [ ! -d $IBP_LOGDIR ] ; then
				mkdir -p $IBP_LOGDIR
			fi
		else
			if [ ! -d $HOME/log ] ; then
				mkdir -p $HOME/log 
			fi
		fi
		
		if [ x"$IBP_FILEDIR" != x"" ] ; then
			if [ ! -d $IBP_FILEDIR ] ; then
				mkdir -p $IBP_FILEDIR
			fi
		else
			if [ ! -d $HOME/file ] ; then
				mkdir -p $HOME/file 
			fi
		fi
		
		PID=`ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" != x"" ] ; then
			echo "*** WARN : ibas existed"
			exit 1
		fi
		ibas -f ibas.conf -a start
		NRET=$?
		if [ $NRET -ne 0 ] ; then
			echo "*** ERROR : ibas start error[$NRET]"
			exit 1
		fi
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" != x"" ] ; then
				break
			fi
		done
		echo "ibas start ok"
		ibas.sh status
		;;
	stop)
		ibas.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibas not existed"
			exit 1
		fi
		kill $PID
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" = x"" ] ; then
				break
			fi
		done
		echo "ibas end ok"
		;;
	kill)
		ibas.sh status
		killall -9 ibas
		;;
	restart)
		ibas.sh stop
		sleep 1
		ibas.sh start
		;;
	restart_graceful)
		ibas.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -ef | grep -w ibas | grep -v "ibas.sh" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibas not existed"
			exit 1
		fi
		kill -USR2 $PID
		while [ 1 ] ; do
			sleep 1
			PID2=`ps -ef | grep -w ibas | grep -v "ibas.sh" | grep -v grep | awk -v pid="$PID" '{if($3=="1"&&$2!=pid)print $2}'`
			if [ x"$PID2" != x"" ] ; then
				break
			fi
		done
		echo "new ibas pid[$PID2] start ok"
		kill $PID
		while [ 1 ] ; do
			sleep 1
			PID3=`ps -ef | grep -w ibas | grep -v "ibas.sh" | grep -v grep | awk -v pid="$PID" '{if($3=="1"&&$2==pid)print $2}'`
			if [ x"$PID3" = x"" ] ; then
				break
			fi
		done
		echo "old ibas pid[$PID] end ok"
		ibas.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		;;
	reload_log)
		ibas.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibas -f ibas.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibas not existed"
			exit 1
		fi
		kill -USR1 $PID
		;;
	*)
		usage
		;;
esac

