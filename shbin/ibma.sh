#!/bin/bash

usage()
{
	echo "USAGE : ibma.sh [ status | start | stop | clean_stop | kill | restart | reload_log | this_node | nodes | apps ]"
}

if [ $# -eq 0 ] ; then
	usage
	exit 9
fi

case $1 in
	status)
		ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $0}'
		ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3!="1")print $0}'
		;;
	start)
		if [ x"$IBP_LOGDIR" != x"" ] ; then
			if [ ! -d $IBP_LOGDIR ] ; then
				mkdir -p $IBP_LOGDIR
			fi
		else
			if [ ! -d $HOME/log ] ; then
				mkdir -p $HOME/log 
			fi
		fi
		
		if [ x"$IBP_SODIR" != x"" ] ; then
			if [ ! -d $IBP_SODIR ] ; then
				mkdir -p $IBP_SODIR
			fi
		else
			if [ ! -d $HOME/so ] ; then
				mkdir -p $HOME/so 
			fi
		fi
		
		PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" != x"" ] ; then
			echo "*** WARN : ibma existed"
			exit 1
		fi
		ibma -f ibma.conf -a start
		NRET=$?
		if [ $NRET -ne 0 ] ; then
			echo "*** ERROR : ibma start error[$NRET]"
			exit 1
		fi
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" != x"" ] ; then
				break
			fi
		done
		echo "ibma start ok"
		ibma.sh status
		;;
	stop)
		ibma.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibma not existed"
			exit 1
		fi
		kill $PID
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" = x"" ] ; then
				break
			fi
		done
		echo "ibma end ok"
		;;
	clean_stop)
		ibma.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibma not existed"
			exit 1
		fi
		kill -USR2 $PID
		while [ 1 ] ; do
			sleep 1
			PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
			if [ x"$PID" = x"" ] ; then
				break
			fi
		done
		echo "ibma end ok"
		;;
	kill)
		ibma.sh status
		killall -9 ibma
		;;
	restart)
		ibma.sh stop
		sleep 1
		ibma.sh start
		;;
	reload_log)
		ibma.sh status
		if [ $? -ne 0 ] ; then
			exit 1
		fi
		PID=`ps -f -u $USER | grep "ibma -f ibma.conf" | grep -v grep | awk '{if($3=="1")print $2}'`
		if [ x"$PID" = x"" ] ; then
			echo "*** WARN : ibma not existed"
			exit 1
		fi
		kill -USR1 $PID
		;;
	this_node)
		ibma -f ibma.conf -s this_node
		;;
	nodes)
		ibma -f ibma.conf -s nodes
		;;
	apps)
		ibma -f ibma.conf -s apps
		;;
	*)
		usage
		;;
esac

