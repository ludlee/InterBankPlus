ibms -a add_node --node ibma-9 --invalid 0
ibms -s nodes
ibms -a set_node --node ibma-9 --invalid 1
ibms -s nodes
ibms -a remove_node --node ibma-9
ibms -s nodes

ibms -a add_node --node ibma-9 --invalid 0
ibms -s nodes
ibms -a add_node_host --node ibma-9 --ip 127.0.0.1 --port 18901
ibms -a add_node_host --node ibma-9 --ip 127.0.0.1 --port 18902
ibms -s nodes
ibms -a remove_node_host --node ibma-9 --ip 127.0.0.1 --port 18901
ibms -a remove_node_host --node ibma-9 --ip 127.0.0.1 --port 18902
ibms -s nodes
ibms -a remove_node --node ibma-9
ibms -s nodes

ibms -a add_node --node ibma-7 --invalid 0
ibms -s nodes
ibms -a add_node_host --node ibma-7 --ip 127.0.0.1 --port 18801
ibms -a add_node_host --node ibma-7 --ip 127.0.0.1 --port 18802
ibms -s nodes

ibms -a add_node --node ibma-8 --invalid 0
ibms -s nodes
ibms -a add_node_host --node ibma-8 --ip 127.0.0.1 --port 18801
ibms -a add_node_host --node ibma-8 --ip 127.0.0.1 --port 18802
ibms -s nodes

ibms -a add_app --app TESTECHO9 --desc "测试回射服务9" --bin TESTECHO9.so --timeout 60 --invalid 0
ibms -s apps
ibms -a set_app --app TESTECHO9 --desc "测试回射服务9" --bin TESTECHO9.so --timeout 600 --invalid 1
ibms -s apps
ibms -a remove_app --app TESTECHO9
ibms -s apps

ibms -a add_app --app TESTECHO7 --desc "测试回射服务7" --bin TESTECHO9.so --timeout 60 --invalid 0
ibms -s apps
ibms -a add_app --app TESTECHO8 --desc "测试回射服务8" --bin TESTECHO9.so --timeout 60 --invalid 0
ibms -s apps

ibms -a add_project --project project-9 --nodes "ibma-1" --apps "TESTECHO"
ibms -s projects
ibms -a set_project --project project-9 --nodes "ibma-1 ibma-2 ibma-3" --apps "TESTECHO TESTECHO2 TESTECHO3"
ibms -s projects
ibms -a remove_project --project project-9
ibms -s projects

ibms -a add_project --project project-7 --nodes "ibma-1 ibma-2 ibma-3" --apps "TESTECHO TESTECHO2 TESTECHO3"
ibms -s projects
ibms -a add_project --project project-8 --nodes "ibma-1 ibma-2 ibma-3" --apps "TESTECHO TESTECHO2 TESTECHO3"
ibms -s projects



ibms -a set_node --node ibma-1 --invalid 1
ibms -s nodes
ibms -s nodes_changed
ibms -a remove_node_host --node ibma-1 --ip 127.0.0.1 --port 18101
ibms -a add_node_host --node ibma-1 --ip 127.0.0.1 --port 18101
ibms -a add_node_host --node ibma-1 --ip 127.0.0.1 --port 18888
ibms -s nodes
ibms -s nodes_changed
ibms -s nodes
ibms -a remove_node --node ibma-2
ibms -s nodes
ibms -s nodes_changed
ibms -a add_project --project project-7 --nodes "ibma-1 ibma-2 ibma-3" --apps "TESTECHO TESTECHO2 TESTECHO3"
ibms -s nodes
ibms -s nodes_changed

ibms -a commit_changed

