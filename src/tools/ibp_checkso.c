#include "ibas_api.h"

static int ibp_checkso( char *so_filename , int rm_on_failure_flag )
{
	void		*so_handle = NULL ;
	funcIbasSomain	*pfunc = NULL ;
	
	so_handle = dlopen( so_filename , RTLD_NOW ) ;
	if( so_handle == NULL )
	{
		printf( "dlopen[%s] failed , dlerror[%s]\n" , so_filename , dlerror() );
		if( rm_on_failure_flag )
			unlink( so_filename );
		return -1;
	}
	
	pfunc = dlsym( so_handle , IBAS_SOMAIN ) ;
	if( pfunc == NULL )
	{	
		printf( "dlsym[%s][%s] failed , dlerror[%s]\n" , so_filename , IBAS_SOMAIN , dlerror() );
		dlclose( so_handle );
		if( rm_on_failure_flag )
			unlink( so_filename );
		return -2;
	}
	
	dlclose( so_handle );
	
	printf( "ok!\n" );
		
	return 0;
}

static void usage()
{
	printf( "ibp_checkso v1.0.0.0\n" );
	printf( "USAGE : ibp_checkso so_filename [ -r ]\n" );
	return;
}

int main( int argc , char *argv[] )
{
	int	nret ;
	
	if( argc == 1 + 1 )
	{
		nret = ibp_checkso( argv[1] , 0 ) ;
	}
	else if( argc == 1 + 2 )
	{
		if( STRCMP( argv[2] , == , "-r" ) )
		{
			nret = ibp_checkso( argv[1] , 1 ) ;
		}
		else
		{
			usage();
			exit(7);
		}
	}
	else
	{
		usage();
		exit(7);
	}
	
	return -nret;
}

