#include "ibp_util.h"

static int ibp_genrsakeys( char *node )
{
	RSA		*rsa = NULL ;
	char		publibkey_filename[ IBP_MAXLEN_FILENAME + 1 ] ;
	char		privatekey_filename[ IBP_MAXLEN_FILENAME + 1 ] ;
	
	int		nret = 0 ;
	
	rsa = GenerateRsaKey() ;
	if( rsa == NULL )
	{
		printf( "GenerateRsaKey failed , errno[%d]\n" , errno );
		return -1;
	}
	
	memset( publibkey_filename , 0x00 , sizeof(publibkey_filename) );
	snprintf( publibkey_filename , sizeof(publibkey_filename)-1 , "%s.pubkey" , node );
	nret = WritePublicKeyFile( rsa , publibkey_filename ) ;
	if( nret )
	{
		printf( "WritePublicKeyFile failed[%d] , errno[%d]\n" , nret , errno );
		FreeRsaKey( rsa );
		return -2;
	}
	
	memset( privatekey_filename , 0x00 , sizeof(privatekey_filename) );
	snprintf( privatekey_filename , sizeof(privatekey_filename)-1 , "%s.prikey" , node );
	nret = WritePrivateKeyFile( rsa , privatekey_filename ) ;
	if( nret )
	{
		printf( "WritePrivateKeyFile failed[%d] , errno[%d]\n" , nret , errno );
		FreeRsaKey( rsa );
		return -3;
	}
	
	FreeRsaKey( rsa );
	
	return 0;
}

int main( int argc , char *argv[] )
{
	int		nret = 0 ;
	
	if( argc == 1 + 1 )
	{
		nret = ibp_genrsakeys( argv[1] ) ;
		if( nret == 0 )
			printf( "OK\n" );
	}
	else
	{
		printf( "USAGE : ibp_genrsakeys node\n" );
		exit(7);
	}
	
	return -nret;
}

