IBP_ERR_TABLE_C=ibp_err_table.c

>$IBP_ERR_TABLE_C
echo "#include \"ibp_err.h\"" >>$IBP_ERR_TABLE_C
echo "">>$IBP_ERR_TABLE_C
echo "struct IbpErr	g_astIbpErrTable[] =	{">>$IBP_ERR_TABLE_C
cat ibp_err.h | awk '{if($4=="/*")printf "					{ %d , \"%s\" } ,\n",$3,$5}' >>$IBP_ERR_TABLE_C
echo "					{ 0 , NULL }">>$IBP_ERR_TABLE_C
echo "					} ;">>$IBP_ERR_TABLE_C
echo "">>$IBP_ERR_TABLE_C
echo "struct IbpErr	g_astFasterhttpErrTable[] =	{">>$IBP_ERR_TABLE_C
cat fasterhttp.h | awk '{if($4=="/*")printf "					{ %d , \"%s\" } ,\n",$3,$5}' >>$IBP_ERR_TABLE_C
echo "					{ 0 , NULL }">>$IBP_ERR_TABLE_C
echo "					} ;">>$IBP_ERR_TABLE_C

