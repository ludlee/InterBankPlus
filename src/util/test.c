#include "ibp_util.h"

void test_funcstack()
{
	char		*p = NULL ;
	
	p = DumpFuncStack() ;
	if( p == NULL )
	{
		printf( "DumpFuncStack failed , errno[%d]\n" , errno );
		return;
	}
	
	printf( "%s\n" , p );
	
	free( p );
	
	return;
}

void test_IsMatchString()
{
	printf( "IsMatchString return[%d]\n" , IsMatchString( "*" , "ABCD1001" , '*' , '?' ) );
	printf( "IsMatchString return[%d]\n" , IsMatchString( "E*" , "ABCD1001" , '*' , '?' ) );
	
	return;
}

void test_sscanf()
{
	char	buf[] = "project-9" ;
	char	project[ IBP_MAXLEN_PROJECT + 1 ] ;
	int	nret = 0 ;
	
	nret = sscanf( buf , "%s" , project );
	printf( "buf[%s] project[%s] - nret[%d]\n" , buf , project , nret );
	
	return;
}

int main()
{
	test_funcstack();
	
	return 0;
}

