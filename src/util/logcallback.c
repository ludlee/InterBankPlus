#include "ibp_util.h"

static struct NetAddress	sg_iblog_server_netaddr = { "" , 0 , -1 } ;
static int			sg_iblog_connecting_timeout = 10 ;

int IBPConvertLogLevel( char *log_level_str )
{
	int	log_level ;
	
	int	nret ;
	
	nret = ConvertLogLevel_atoi( log_level_str , & log_level ) ;
	if( nret )
		return LOG_LEVEL_DEBUG;
	
	return log_level;
}

static int ConnectIblogServer()
{
	int		nret = 0 ;
	
	if( sg_iblog_server_netaddr.sock != -1 )
		return 0;
	
	/* 连接日志服务器 */
	sg_iblog_server_netaddr.sock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ;
	if( sg_iblog_server_netaddr.sock == -1 )
	{
		return -1;
	}
	
	SetHttpReuseAddr( sg_iblog_server_netaddr.sock );
	SetHttpNodelay( sg_iblog_server_netaddr.sock , 1 );
	SetHttpNonblock( sg_iblog_server_netaddr.sock );
	
	SETNETADDRESS( sg_iblog_server_netaddr )
	nret = connect( sg_iblog_server_netaddr.sock , (struct sockaddr *) & (sg_iblog_server_netaddr.addr) , sizeof(struct sockaddr) ) ;
	if( nret == -1 )
	{
		fd_set		writefds ;
		struct timeval	timeout ;
		int		error , code ;
		SOCKLEN_T	addr_len ;
		
		FD_ZERO( & writefds );
		FD_SET( sg_iblog_server_netaddr.sock , & writefds );
		timeout.tv_sec = sg_iblog_connecting_timeout ;
		timeout.tv_usec = 0 ;
		nret = select( sg_iblog_server_netaddr.sock+1 , NULL , & writefds , NULL , & timeout ) ;
		addr_len = sizeof(int) ;
		code = getsockopt( sg_iblog_server_netaddr.sock , SOL_SOCKET , SO_ERROR , & error , & addr_len ) ;
		if( nret != 1 || code < 0 || error )
		{
			close( sg_iblog_server_netaddr.sock );
			sg_iblog_server_netaddr.sock = -1 ;
			return -2;
		}
	}
	
	SetHttpBlock( sg_iblog_server_netaddr.sock );
	
	return 0;
}

static int OpenLogProc( LOG *g , char *log_pathfilename , void **open_handle )
{
	int		nret = 0 ;
	
	if( g->log_pathfilename[0] == '\0' )
	{
		return 0;
	}
	else if( MEMCMP( g->log_pathfilename , == , "file::" , 6 ) )
	{
		return 0;
	}
	else if( MEMCMP( g->log_pathfilename , == , "iblog::" , 7 ) )
	{
	}
	else
	{
		return -1;
	}
	
	SetOpenFlag( g , 1 );
	
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		return 0;
	
	nret = ConnectIblogServer() ;
	if( nret )
		return 0;
	
	if( log_pathfilename[0] )
	{
		char		comm_buf[ 1024 + 1 ] ;
		int		len , len2 ;
	
		/* 发送"设置日志文件"通讯报文 */
		len = snprintf( comm_buf , sizeof(comm_buf)-1 , "    0%s" , log_pathfilename+7 ) ;
		*((uint32_t*)comm_buf) = htonl( len-4 ) ;
		
		len2 = send( sg_iblog_server_netaddr.sock , comm_buf , len , 0 ) ;
		if( len2 != len )
		{
			close( sg_iblog_server_netaddr.sock );
			sg_iblog_server_netaddr.sock = -1 ;
			return 0;
		}
	}
	
	return 0;
}

static int OpenLogFirstProc( LOG *g , char *log_pathfilename , void **open_handle )
{
	return OpenLogProc( g , log_pathfilename , open_handle );
}

static int WriteLogProc( LOG *g , void **open_handle , int log_level , char *buf , long len , long *writelen )
{
	static char		log_pathfilename[ MAXLEN_FILENAME + 1 ] = "" ;
	int			len2 ;
	
	int			nret = 0 ;
	
	if( g->log_pathfilename[0] == '\0' )
	{
		return 0;
	}
	else if( MEMCMP( g->log_pathfilename , == , "file::" , 6 ) )
	{
		goto ERR;
	}
	else if( MEMCMP( g->log_pathfilename , == , "iblog::" , 7 ) )
	{
	}
	else
	{
		return -1;
	}
	
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		goto ERR;
	
	if( sg_iblog_server_netaddr.sock == -1 )
	{
		nret = ConnectIblogServer() ;
		if( nret )
			goto ERR;
		memset( log_pathfilename , 0x00 , sizeof(log_pathfilename) );
	}
	
	if( STRCMP( g->log_pathfilename , != , log_pathfilename ) )
	{
		char		comm_buf[ 1024 + 1 ] ;
		int		len , len2 ;
	
		/* 发送"设置日志文件"通讯报文 */
		len = snprintf( comm_buf , sizeof(comm_buf)-1 , "    0%s" , GetLogPathfilename(g)+7 ) ;
		*((uint32_t*)comm_buf) = htonl( len-4 ) ;
		
		len2 = send( sg_iblog_server_netaddr.sock , comm_buf , len , 0 ) ;
		if( len2 != len )
		{
			close( sg_iblog_server_netaddr.sock );
			sg_iblog_server_netaddr.sock = -1 ;
			goto ERR;
		}
		
		strcpy( log_pathfilename , g->log_pathfilename );
	}
	
	/* 发送"输出日志文件"通讯报文 */
	*((uint32_t*)buf) = htonl( len-4 ) ;
	len2 = send( sg_iblog_server_netaddr.sock , buf , len , 0 ) ;
	if( len2 != len )
	{
		close( sg_iblog_server_netaddr.sock );
		sg_iblog_server_netaddr.sock = -1 ;
		goto ERR;
	}
	
	(*writelen) = len2 ;
	
	return 0;

ERR :

	{
	char	local_log_pathfilename[ MAXLEN_FILENAME + 1 ] ;
	int	fd ; 
	
	memset( local_log_pathfilename , 0x00 , sizeof(local_log_pathfilename) );
	snprintf( local_log_pathfilename , sizeof(local_log_pathfilename) , "%s/%s" , getenv("HOME") , g->log_pathfilename+6 );
	fd = open( local_log_pathfilename , O_CREAT|O_APPEND|O_WRONLY , S_IRWXU|S_IRWXG|S_IRWXO ) ;
	if( fd == -1 )
		return -2;
	write( fd , buf+6 , len-6 );
	close( fd );
	}
	
	return 0;
}

static int CloseLogProc( LOG *g , void **open_handle )
{
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		return 0;
	
	if( sg_iblog_server_netaddr.sock != -1 )
	{
		close( sg_iblog_server_netaddr.sock );
		sg_iblog_server_netaddr.sock = -1 ;
	}
	
	SetOpenFlag( g , 0 );
	
	return 0;
}

static int CloseLogFinallyProc( LOG *g , void **open_handle )
{
	return CloseLogProc( g , open_handle );
}

static int LogStylePrefixProc( LOG *g , LOGBUF *logbuf , char *c_filename , long c_fileline , int log_level , char *format , va_list valist )
{
	CleanLogBuffer( g , GetLogBuffer(g) );
	FormatLogBuffer( g , GetLogBuffer(g) , "    1%c" , log_level+'0' );
	
	CleanLogBuffer( g , GetHexLogBuffer(g) );
	FormatLogBuffer( g , GetHexLogBuffer(g) , "    1%c" , log_level+'0' );
	
	return 0;
}

int IBPInitLogEnv( char *iblog_server , char *service_name , char *event_output , int process_loglevel , char *process_output_format , ... )
{
	va_list			valist ;
	char			process_output[ MAXLEN_FILENAME + 1 ] ;
	LOG			*g = NULL ;
	
	int			nret = 0 ;
	
	/* 解析日志服务配置 "ip:port" */
	memset( & sg_iblog_server_netaddr , 0x00 , sizeof(struct NetAddress) );
	sscanf( iblog_server , "%[^:]:%d" , sg_iblog_server_netaddr.ip , & (sg_iblog_server_netaddr.port) );
	
	/* 创建日志句柄集合 */
	CreateLogsHandleG();
	
	/* 创建event日志句柄 */
	g = CreateLogHandle() ;
	if( g == NULL )
		return -11;
	
	nret = SetLogOutput( g , LOG_OUTPUT_CALLBACK , event_output , & OpenLogFirstProc , & OpenLogProc , & WriteLogProc , NULL , & CloseLogProc , & CloseLogFinallyProc ) ;
	if( nret )
		return -12;
	
	SetLogLevel( g , LOG_LEVEL_NOTICE );
	SetLogStylesEx( g , LOG_STYLE_DATETIME|LOG_STYLE_LOGLEVEL|LOG_STYLE_PID|LOG_STYLE_CUSTLABEL1|LOG_STYLE_FORMAT|LOG_STYLE_NEWLINE , NULL , & LogStylePrefixProc );
	SetLogCustLabel( g , 1 , service_name );
	
	nret = AddLogToLogsG( "event" , g ) ;
	if( nret )
		return -13;
	
	/* 创建process日志句柄 */
	g = CreateLogHandle() ;
	if( g == NULL )
		return -21;
	
	va_start( valist , process_output_format );
	memset( process_output , 0x00 , sizeof(process_output) );
	vsnprintf( process_output , sizeof(process_output)-1 , process_output_format , valist );
	va_end( valist );
	
	nret = SetLogOutput( g , LOG_OUTPUT_CALLBACK , process_output , & OpenLogFirstProc , & OpenLogProc , & WriteLogProc , NULL , & CloseLogProc , & CloseLogFinallyProc ) ;
	if( nret )
		return -22;
	
	SetLogLevel( g , process_loglevel );
	SetLogStylesEx( g , LOG_STYLE_DATETIMEMS|LOG_STYLE_LOGLEVEL|LOG_STYLE_PID|LOG_STYLE_CUSTLABEL1|LOG_STYLE_SOURCE|LOG_STYLE_FORMAT|LOG_STYLE_NEWLINE , NULL , & LogStylePrefixProc );
	SetLogCustLabel( g , 1 , service_name );
	
	nret = AddLogToLogsG( "process" , g ) ;
	if( nret )
		return -23;
	
	/* 创建app日志句柄 */
	g = CreateLogHandleG() ;
	if( g == NULL )
		return -41;
	
	nret = SetLogOutputG( LOG_OUTPUT_CALLBACK , process_output , & OpenLogFirstProc , & OpenLogProc , & WriteLogProc , NULL , & CloseLogProc , & CloseLogFinallyProc ) ;
	if( nret )
		return -42;
	
	SetLogLevelG( LOG_LEVEL_DEBUG );
	SetLogStylesExG( LOG_STYLE_DATETIMEMS|LOG_STYLE_LOGLEVEL|LOG_STYLE_PID|LOG_STYLE_SOURCE|LOG_STYLE_FORMAT|LOG_STYLE_NEWLINE , NULL , & LogStylePrefixProc );
	
	return 0;
}

int IBPChangeProcessLogFilename( char *log_filename_format , ... )
{
	LOG			*g = NULL ;
	
	va_list			valist ;
	char			log_pathfilename[ MAXLEN_FILENAME + 1 ] ;
	
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		return 0;
	
	g = GetLogFromLogsG( "process" ) ;
	if( g == NULL )
		return -1;
	
	va_start( valist , log_filename_format );
	memset( log_pathfilename , 0x00 , sizeof(log_pathfilename) );
	vsnprintf( log_pathfilename , sizeof(log_pathfilename)-1 , log_filename_format , valist );
	va_end( valist );
	
	strcpy( GetLogPathfilename(g) , log_pathfilename );
	
	return 0;
}

int IBPChangeProcessLogLevel( int process_loglevel )
{
	LOG			*g = NULL ;
	
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		return 0;
	
	g = GetLogFromLogsG( "process" ) ;
	if( g == NULL )
		return -1;
	
	SetLogLevel( g , process_loglevel );
	
	return 0;
}

int IBPChangeAppLogFilename( char *log_filename_format , ... )
{
	va_list			valist ;
	char			log_pathfilename[ MAXLEN_FILENAME + 1 ] ;
	
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		return 0;
	
	va_start( valist , log_filename_format );
	memset( log_pathfilename , 0x00 , sizeof(log_pathfilename) );
	vsnprintf( log_pathfilename , sizeof(log_pathfilename)-1 , log_filename_format , valist );
	va_end( valist );
	
	strcpy( GetLogPathfilename(GetLogHandleG()) , log_pathfilename );
	
	return 0;
}

int IBPChangeAppLogLevel( int app_loglevel )
{
	if( sg_iblog_server_netaddr.ip[0] == '\0' )
		return 0;
	
	SetLogLevelG( app_loglevel );
	
	return 0;
}

void IBPCleanLogEnv()
{
	DestroyLogsHandleG();
	DestroyLogHandleG();
	
	return;
}
 
