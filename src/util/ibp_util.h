#ifndef _H_IBP_UTIL_
#define _H_IBP_UTIL_

#include "ibp_idl.h"

#include "ibp_base.h"
#include "ibp_api.h"
#include "ibp_err.h"

extern char     *g_ibp_util_kernel_version ;

/*
 * time
 */

void DiffTimeval( struct timeval *p_tv1 , struct timeval *p_tv2 , struct timeval *p_diff );

/*
 * macro for all
 */

#define IBP_URI_ALIVE_REPORT_FIRST	"/alive_report_first"
#define IBP_URI_ALIVE_REPORT		"/alive_report"
#define IBP_URI_ALIVE_NOTICE		"/alive_notice"
#define IBP_URI_FETCH_CONFIG		"/fetch_config"
#define IBP_URI_DOWNLOAD_CONFIG		"/download_config"
#define IBP_URI_FETCH_BIN		"/fetch_bin"
#define IBP_URI_NULL			"/null"
#define IBP_URI_PRE_TRANSACTION		"/pre_transaction"
#define IBP_URI_TRANSACTION		"/transaction"
#define IBP_URI_PUT_FILE		"/put_file"
#define IBP_URI_GET_FILE		"/get_file"

#define IBP_HTTPHEADER_IBP_CONTENT_SIGN			"Ibp-Content-Sign"
#define IBP_HTTPHEADER_IBP_CONTENT_SIGN_MD5		"Ibp-Content-Sign-Md5"
#define IBP_HTTPHEADER_IBP_CONTENT_ENCRYPT		"Ibp-Content-Encrypt"
#define IBP_HTTPHEADER_IBP_3DES_KEY			"Ibp-3Des-Key"
#define IBP_HTTPHEADER_IBP_CONTENT_COMPRESS		"Ibp-Content-Compress"
#define IBP_HTTPHEADER_IBP_DECOMPRESS_CONTENT_LENGTH	"Ibp-Decompress-Content-Length"
#define IBP_HTTPHEADER_IBP_ORIGIN_CONTENT_LENGTH	"Ibp-Origin-Content-Length"
#define IBP_HTTPHEADER_IBP_CLIENT_NODE			"Ibp-Client-Node"
#define IBP_HTTPHEADER_IBP_SERVER_NODE			"Ibp-Server-Node"
#define IBP_HTTPHEADER_IBP_APP_CODE			"Ibp-App-Code"
#define IBP_HTTPHEADER_IBP_TRANS_TIMEOUT		"Ibp-Trans-Timeout"
#define IBP_HTTPHEADER_IBP_FILE_NAME_1			"Ibp-File-Name-1"
#define IBP_HTTPHEADER_IBP_FILE_NAME_2			"Ibp-File-Name-2"
#define IBP_HTTPHEADER_IBP_FILE_NAME_3			"Ibp-File-Name-3"
#define IBP_HTTPHEADER_IBP_FILE_NAME_4			"Ibp-File-Name-4"
#define IBP_HTTPHEADER_IBP_FILE_NAME			"Ibp-File-Name"

#define IBP_MAXCNT_REQUEST_FILES			4
#define IBP_MAXCNT_RESPONSE_FILES			4

#define SETNETADDRESS(_netaddr_) \
	memset( & ((_netaddr_).addr) , 0x00 , sizeof(struct sockaddr_in) ); \
	(_netaddr_).addr.sin_family = AF_INET ; \
	if( (_netaddr_).ip[0] == '\0' ) \
		(_netaddr_).addr.sin_addr.s_addr = INADDR_ANY ; \
	else \
		(_netaddr_).addr.sin_addr.s_addr = inet_addr((_netaddr_).ip) ; \
	(_netaddr_).addr.sin_port = htons( (unsigned short)((_netaddr_).port) );

#define GETNETADDRESS(_netaddr_) \
	strcpy( (_netaddr_).ip , inet_ntoa((_netaddr_).addr.sin_addr) ); \
	(_netaddr_).port = (int)ntohs( (_netaddr_).addr.sin_port ) ;

#define GETNETADDRESS_LOCAL(_netaddr_) \
	{ \
	socklen_t	socklen = sizeof(struct sockaddr) ; \
	int		nret = 0 ; \
	nret = getsockname( (_netaddr_).sock , (struct sockaddr*)&((_netaddr_).local_addr) , & socklen ) ; \
	if( nret == 0 ) \
	{ \
		strcpy( (_netaddr_).local_ip , inet_ntoa((_netaddr_).local_addr.sin_addr) ); \
		(_netaddr_).local_port = (int)ntohs( (_netaddr_).local_addr.sin_port ) ; \
	} \
	}

#define GETNETADDRESS_REMOTE(_netaddr_) \
	{ \
	socklen_t	socklen = sizeof(struct sockaddr) ; \
	int		nret = 0 ; \
	nret = getpeername( (_netaddr_).sock , (struct sockaddr*)&((_netaddr_).remote_addr) , & socklen ) ; \
	if( nret == 0 ) \
	{ \
		strcpy( (_netaddr_).remote_ip , inet_ntoa((_netaddr_).remote_addr.sin_addr) ); \
		(_netaddr_).remote_port = (int)ntohs( (_netaddr_).remote_addr.sin_port ) ; \
	} \
	}

/*
 * structure for communications
 */

struct NetAddress
{
	char			ip[ IBP_MAXLEN_NODE_IP + 1 ] ;
	int			port ;
	SOCKET			sock ;
	struct sockaddr_in	addr ;
	
	struct sockaddr_in	local_addr ;
	char			local_ip[ IBP_MAXLEN_NODE_IP + 1 ] ;
	int			local_port ;
	
	struct sockaddr_in	remote_addr ;
	char			remote_ip[ IBP_MAXLEN_NODE_IP + 1 ] ;
	int			remote_port ;
} ;

/*
 * use for ibma iba_api
 */

struct ShareMemory
{
	int			proj_id ;
	key_t			shmkey ;
	int			shmid ;
	void			*base ;
	int			size ;
} ;

struct Semaphore
{
	int			proj_id ;
	key_t			semkey ;
	int			semid ;
} ;

struct NodeSpaceUnit
{
	char			node[ IBP_MAXLEN_NODE + 1 ] ;
	char			invalid ;
	
	int			host_space_base_offset ;
	int			host_count ;
	
	char			key[ 1024 + 1 ] ;
	int			key_len ;
} ;

struct HostSpaceUnit
{
	char			ip[ IBP_MAXLEN_NODE_IP + 1 ] ;
	int			port ;
	int			load ;
	char			invalid ;
	time_t			cannot_connect_timestamp ;
} ;

struct AppSpaceUnit
{
	char			app[ IBP_MAXLEN_APP + 1 ] ;
	char			desc[ IBP_MAXLEN_APP_DESC + 1 ] ;
	char			bin[ IBP_MAXLEN_APP_BIN + 1 ] ;
	int			timeout ;
	int			timeout2 ;
} ;

struct IndexSpaceAttaching
{
	key_t			shmkey ;
	
	char			node[ IBP_MAXLEN_NODE + 1 ] ;
	int			retry_connect_timeval ;
} ;

#define HASH_EXPANSION_FACTOR	4

struct ConfigSpaceAttaching
{
	int			nodes_hash_size ;
	int			hosts_array_size ;
	int			apps_hash_size ;
} ;

struct ConfigSpaceAddresses
{
	struct NodeSpaceUnit	*nodes_hash_base ;
	struct NodeSpaceUnit	*last_node_unit ;
	struct HostSpaceUnit	*hosts_array_base ;
	struct AppSpaceUnit	*apps_hash_base ;
	struct AppSpaceUnit	*last_app_unit ;
} ;

void LocateIndexSpaceAddress( void *base , struct IndexSpaceAttaching **pp_index_space_attaching );
void LocateConfigSpaceAddress( void *base , struct ConfigSpaceAttaching **pp_config_space_attaching , struct ConfigSpaceAddresses *p_config_space_adresses );

struct NodeSpaceUnit *TravelNodes( struct ConfigSpaceAddresses *p_config_space_addresses , struct NodeSpaceUnit *p_node_unit );
struct NodeSpaceUnit *QueryNode( struct ConfigSpaceAttaching *p_config_space_attaching , struct ConfigSpaceAddresses *p_config_space_addresses , char *node );
char *GetNodePtr( struct NodeSpaceUnit *p_node_unit );
char GetNodeInvalid( struct NodeSpaceUnit *p_node_unit );
int GetNodeHostCount( struct NodeSpaceUnit *p_node_unit );
char *GetNodeKey( struct NodeSpaceUnit *p_node_unit , int *p_key_len );

struct HostSpaceUnit *TravelNodeHosts( struct ConfigSpaceAddresses *p_config_space_addresses , struct NodeSpaceUnit *p_node_unit , struct HostSpaceUnit *p_host_unit );
struct HostSpaceUnit *QueryNodeHost( struct ConfigSpaceAddresses *p_config_space_addresses , struct NodeSpaceUnit *p_node_unit , char *ip , int port );
char *GetHostIpPtr( struct HostSpaceUnit *p_host_unit );
int GetHostPort( struct HostSpaceUnit *p_host_unit );
int GetHostLoad( struct HostSpaceUnit *p_host_unit );
#if 0
char GetHostInvalid( struct HostSpaceUnit *p_host_unit );
#endif

struct AppSpaceUnit *TravelApps( struct ConfigSpaceAddresses *p_config_space_addresses , struct AppSpaceUnit *p_app_unit );
struct AppSpaceUnit *QueryApp( struct ConfigSpaceAttaching *p_config_space_attaching , struct ConfigSpaceAddresses *p_config_space_addresses , char *app );
char *GetAppPtr( struct AppSpaceUnit *p_app_unit );
char *GetAppDescPtr( struct AppSpaceUnit *p_app_unit );
char *GetAppBinPtr( struct AppSpaceUnit *p_app_unit );
int GetAppTimeout( struct AppSpaceUnit *p_app_unit );
int GetAppTimeout2( struct AppSpaceUnit *p_app_unit );

/*
 * tools utility
 */

char *StrdupEntireFile( char *pathfilename , int *p_file_len );
int WriteEntireFile( char *pathfilename , char *file_content , int file_len );
int Md5File( char *pathfilename , char *md5_result_exp );
void Md5Data( char *data , int data_len , char *md5_result_exp );
char *TrimFileId( char *filename );

int BindDaemonServer( int (* ServerMain)( void *pv ) , void *pv );

int IsMatchString(char *pcMatchString, char *pcObjectString, char cMatchMuchCharacters, char cMatchOneCharacters);
int nstoi( char *str , int len );
char *strword( char *str , char *word );
char *gettok( char *str , const char *delim );
char *strnchr( char *str , int c , int len );
char *strnstr( char *str , char *find , int len );

unsigned long CalcHash( char *str );

/* 8�ֽڶ������� */
#define CALC_8BYTES_ALIGNMENT(_n_)	( (_n_)==0?0:(((_n_)-1)/8+1)*8 )

/*
 * http utility
 */

int HttpRequestV( struct NetAddress *p_netaddr , struct HttpEnv *http , int timeout , char *format , va_list valist );
int HttpRequest( struct NetAddress *p_netaddr , struct HttpEnv *http , int timeout , char *format , ... );

#define IBP_HTTPSECURE_SIGN_FLAG_MD5			5
#define IBP_HTTPSECURE_SIGN_FLAG_MD5_STR		"MD5"

#define IBP_HTTPSECURE_COMPRESS_FLAG_GZIP		1
#define IBP_HTTPSECURE_COMPRESS_FLAG_GZIP_STR		"GZIP"

#define IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA	13
#define IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA_STR	"3DES-FOR-RSA"

struct HttpSecureEnv ;
struct HttpSecureEnv *CreateHttpSecureEnv( int sign_flag , int compress_flag , int encrypt_flag );
void ResetHttpSecureEnv( struct HttpSecureEnv *http_secure_env );
void DestroyHttpSecureEnv( struct HttpSecureEnv *http_secure_env );

int ReloadRsaKey( struct HttpSecureEnv *http_secure_env , struct NodeSpaceUnit *p_this_node_unit , struct NodeSpaceUnit *p_remote_node_unit );

int PutMessageToHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char *msg , int msg_len );
int PutFileToHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char *pathfilename , int *p_file_len );
int ExtractMessageFromHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char **pp_msg , int *p_msg_len );
int ExtractFileFromHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char *pathfilename , int *p_file_len );

int PutMessageToHttpBody( struct HttpEnv *e , struct HttpBuffer *b , char **pp_msg , int msg_len );

/*
 * crypto
 */

#define IBP_RSA_KEYSIZE_BITS		1024
#define IBP_3DES_KEYSIZE_BYTES		24

int HexExpand( char *HexBuf , int HexBufLen , char *AscBuf );
int HexFold( char *AscBuf , int AscBufLen , char *HexBuf );

#define BASE64_ENCODING_NEWLEN(_len_)		((_len_)<=0?0:((((_len_)-1)/3+1)*4))
#define BASE64_DECODING_NEWLEN(_len_)		((_len_)<=3?0:((_len_)/4*3))
int Encoding_BASE64( char *dec , int declen , char *enc , int *enclen );
int Decoding_BASE64( char *enc , int enclen , char *dec , int *declen );

void Generate3DesKey( char *key , int key_len );

int Encrypt_3DES_192bits( char *key_192bits , char *decrypt , int decrypt_len , char *encrypt , int *encrypt_len , char *init_vector );
int Decrypt_3DES_192bits( char *key_192bits , char *encrypt , int encrypt_len , char *decrypt , int *decrypt_len , char *init_vector );

RSA *GenerateRsaKey();
RSA *ReadPublicKeyFile( char *key_filename );
RSA *ReadPrivateKeyFile( char *key_filename );
int WritePublicKeyFile( RSA *rsa , char *key_filename );
int WritePrivateKeyFile( RSA *rsa , char *key_filename );
RSA *AllocPublicKey( char *key , int key_len );
RSA *AllocPrivateKey( char *key , int key_len );
char *DumpPublicKey( RSA *rsa , int *p_key_len );
char *DumpPrivateKey( RSA *rsa , int *p_key_len );
void FreeRsaKey( RSA *rsa );

#define RSAPADDING_RESIZE(_len_,_type_)								\
	if( _type_ == RSA_PKCS1_PADDING ) _len_ -= 11 ;						\
	else if( _type_ == RSA_SSLV23_PADDING ) _len_ -= 11 ;					\
	else if( _type_ == RSA_X931_PADDING ) _len_ -= 2 ;					\
	else if( _type_ == RSA_NO_PADDING ) ;							\
	else if( _type_ == RSA_PKCS1_OAEP_PADDING ) _len_ -= ( 2 * SHA_DIGEST_LENGTH - 2 ) ;	\
	else _len_ = -1 ;									\

#define Encrypt_RSA_PKCS1(_prsa_,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)		\
	Encrypt_RSA( _prsa_,RSA_PKCS1_PADDING,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)

#define Encrypt_RSA_SSLV23(_prsa_,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)		\
	Encrypt_RSA( _prsa_,RSA_SSLV23_PADDING,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)

#define Encrypt_RSA_X931(_prsa_,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)		\
	Encrypt_RSA( _prsa_,RSA_X931_PADDING,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)

#define Encrypt_RSA_NOPADDING(_prsa_,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)		\
	Encrypt_RSA( _prsa_,RSA_NO_PADDING,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)

#define Encrypt_RSA_PKCS1_OAEP(_prsa_,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)	\
	Encrypt_RSA( _prsa_,RSA_PKCS1_OAEP_PADDING,_decrypt_,_decrypt_len_,_encrypt_,_pencrypt_len_)

#define Decrypt_RSA_PKCS1(_prsa_,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)		\
	Decrypt_RSA( _prsa_,RSA_PKCS1_PADDING,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)

#define Decrypt_RSA_SSLV23(_prsa_,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)		\
	Decrypt_RSA( _prsa_,RSA_SSLV23_PADDING,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)

#define Decrypt_RSA_X931(_prsa_,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)		\
	Decrypt_RSA( _prsa_,RSA_X931_PADDING,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)

#define Decrypt_RSA_NOPADDING(_prsa_,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)		\
	Decrypt_RSA( _prsa_,RSA_NO_PADDING,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)

#define Decrypt_RSA_PKCS1_OAEP(_prsa_,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)	\
	Decrypt_RSA( _prsa_,RSA_PKCS1_OAEP_PADDING,_encrypt_,_encrypt_len_,_decrypt_,_pdecrypt_len_)

int Encrypt_RSA( RSA *rsa , int type , char *decrypt , int decrypt_len , char *encrypt , int *encrypt_len );
int Decrypt_RSA( RSA *rsa , int type , char *encrypt , int encrypt_len , char *decrypt , int *decrypt_len );

#endif
 
