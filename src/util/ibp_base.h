#ifndef _H_IBP_BASE_
#define _H_IBP_BASE_

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/inotify.h>
#include <sys/select.h>

#include "openssl/err.h"
#include "openssl/md5.h"
#include "openssl/des.h"
#include "openssl/rsa.h"
#include "openssl/pem.h"
#include "openssl/x509.h"
#include "openssl/pkcs12.h"
#include "openssl/objects.h"

extern char *strndup(const char *s, size_t n);
extern char *strsignal(int sig);

union semun
{
	int              val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO
				   (Linux specific) */
};

#include "list.h"
#include "rbtree.h"
#include "fasterhttp.h"
#include "LOG.h"
#include "LOGS.h"
#include "funcstack.h"

#define IBP_MAXLEN_FILENAME		256

#ifndef MAX
#define MAX(_a_,_b_) ( (_a_)>(_b_)?(_a_):(_b_) )
#endif

#ifndef MIN
#define MIN(_a_,_b_) ( (_a_)<(_b_)?(_a_):(_b_) )
#endif

#endif

