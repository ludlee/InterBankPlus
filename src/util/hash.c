#include "ibp_util.h"

unsigned long CalcHash( char *str )
{
	char		*p = str ;
	unsigned long	ul = 0 ;
	
	for( ; (*p) ; p++ )
	{
		ul  = (*p) + (ul<<6)+ (ul>>16) - ul ;
	}
	
	return ul;
}

