#ifndef _H_IBP_API_
#define _H_IBP_API_

#include "ibp_base.h"

/*
 * logcallback
 */

int IBPConvertLogLevel( char *log_level_str );
int IBPInitLogEnv( char *iblog_server , char *service_name , char *event_output , int process_loglevel , char *process_output_format , ... );
int IBPChangeProcessLogFilename( char *log_filename_format , ... );
int IBPChangeProcessLogLevel( int process_loglevel );
int IBPChangeAppLogFilename( char *log_filename_format , ... );
int IBPChangeAppLogLevel( int app_loglevel );
void IBPCleanLogEnv();

/*
 * file
 */

FILE *IBPCreateTempFile( char *file_id , char *filename , char *pathfilename , char *mode );

#endif

