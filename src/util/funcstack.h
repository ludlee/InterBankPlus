#ifndef _H_FUNCSTACK_
#define _H_FUNCSTACK_

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <execinfo.h>
#include <dlfcn.h>

char *DumpFuncStack();

#endif

