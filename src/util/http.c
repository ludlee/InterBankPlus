#include "ibp_util.h"

struct HttpSecureEnv
{
	int			sign_flag ;
	int			compress_flag ;
	int			encrypt_flag ;
	
	struct NodeSpaceUnit	*p_this_node_unit ;
	struct NodeSpaceUnit	*p_remote_node_unit ;
	RSA			*this_rsa ;
	RSA			*remote_rsa ;
	
	int			key_3des_generated_flag ;
	char			key_3des_dec[ IBP_RSA_KEYSIZE_BITS/8 + 1 ] ;
	char			key_3des_enc[ IBP_RSA_KEYSIZE_BITS/8 + 1 ] ;
	char			key_3des_enc_exp[ IBP_RSA_KEYSIZE_BITS/8*2 + 1 ] ;
	
	char			*compress_buffer ;
	int			compress_bufsize ;
	char			*decompress_buffer ;
	int			decompress_bufsize ;
	
	char 			*decrypt_buffer ;
	int			decrypt_bufsize ;
	char			*encrypt_buffer ;
	int			encrypt_bufsize ;
} ;

int HttpRequestV( struct NetAddress *p_netaddr , struct HttpEnv *http , int timeout , char *format , va_list valist )
{
	struct HttpBuffer	*b = NULL ;
	
	int			nret = 0 ;
	
	p_netaddr->sock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ;
	if( p_netaddr->sock == -1 )
	{
		ERRORLOGSG( "socket failed , errno[%d]" , errno );
		return -1;
	}
	
	SetHttpNodelay( p_netaddr->sock , 1 );
	
	SETNETADDRESS( (*p_netaddr) )
	nret = connect( p_netaddr->sock , (struct sockaddr *) & (p_netaddr->addr) , sizeof(struct sockaddr) ) ;
	if( nret == -1 )
	{
		ERRORLOGSG( "connect[%s:%d] failed , errno[%d]" , p_netaddr->ip , p_netaddr->port , errno );
		close( p_netaddr->sock ); p_netaddr->sock = -1 ;
		return -2;
	}
	
	ResetHttpEnv( http );
	
	b = GetHttpRequestBuffer( http ) ;
	nret = StrcatvHttpBuffer( b , format , valist ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatvHttpBuffer failed[%d]" , nret );
		close( p_netaddr->sock ); p_netaddr->sock = -1 ;
		return nret;
	}
	
	DEBUGHEXLOGSG( GetHttpBufferBase(b,NULL) , GetHttpBufferLength(b) , "RequestBuffer" );
	
	SetHttpTimeout( http , timeout );
	
	nret = SendHttpRequest( p_netaddr->sock , NULL , http ) ;
	if( nret )
	{
		ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
		close( p_netaddr->sock ); p_netaddr->sock = -1 ;
		return nret;
	}
	
	nret = ReceiveHttpResponse( p_netaddr->sock , NULL , http ) ;
	if( nret )
	{
		ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
		close( p_netaddr->sock ); p_netaddr->sock = -1 ;
		return nret;
	}
	
	b = GetHttpResponseBuffer( http ) ;
	DEBUGHEXLOGSG( GetHttpBufferBase(b,NULL) , GetHttpBufferLength(b) , "ResponseBuffer" );
	
	close( p_netaddr->sock ); p_netaddr->sock = -1 ;
	
	return 0;
}

int HttpRequest( struct NetAddress *p_netaddr , struct HttpEnv *http , int timeout , char *format , ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , format );
	nret = HttpRequestV( p_netaddr , http , timeout , format , valist ) ;
	va_end( valist );
	
	return nret;
}

struct HttpSecureEnv *CreateHttpSecureEnv( int sign_flag , int compress_flag , int encrypt_flag )
{
	struct HttpSecureEnv	*http_secure_env = NULL ;
	
	http_secure_env = (struct HttpSecureEnv *)malloc( sizeof(struct HttpSecureEnv) ) ;
	if( http_secure_env == NULL )
	{
		ERRORLOGSG( "malloc failed , errno[%d]" , errno );
		return NULL;
	}
	memset( http_secure_env , 0x00 , sizeof(struct HttpSecureEnv) );
	
	http_secure_env->sign_flag = sign_flag ;
	http_secure_env->compress_flag = compress_flag ;
	http_secure_env->encrypt_flag = encrypt_flag ;
	
	return http_secure_env;
}

void ResetHttpSecureEnv( struct HttpSecureEnv *http_secure_env )
{
	http_secure_env->key_3des_generated_flag = 0 ;
	
	if( http_secure_env->compress_buffer )
	{
		char	*compress_buffer = NULL ;
		int	compress_bufsize ;
		
		compress_bufsize = 1024*1024 ;
		if( http_secure_env->compress_bufsize > compress_bufsize )
		{
			compress_buffer = (char*)realloc( http_secure_env->compress_buffer , compress_bufsize ) ;
			if( compress_buffer )
			{
				http_secure_env->compress_bufsize = compress_bufsize ;
				http_secure_env->compress_buffer = compress_buffer ;
			}
		}
	}
	
	if( http_secure_env->decompress_buffer )
	{
		char	*decompress_buffer = NULL ;
		int	decompress_bufsize ;
		
		decompress_bufsize = 1024*1024 ;
		if( http_secure_env->decompress_bufsize > decompress_bufsize )
		{
			decompress_buffer = (char*)realloc( http_secure_env->decompress_buffer , decompress_bufsize ) ;
			if( decompress_buffer )
			{
				http_secure_env->decompress_bufsize = decompress_bufsize ;
				http_secure_env->decompress_buffer = decompress_buffer ;
			}
		}
	}
	
	if( http_secure_env->decrypt_buffer )
	{
		char	*decrypt_buffer = NULL ;
		int	decrypt_bufsize ;
		
		decrypt_bufsize = 1024*1024 ;
		if( http_secure_env->decrypt_bufsize > decrypt_bufsize )
		{
			decrypt_buffer = (char*)realloc( http_secure_env->decrypt_buffer , decrypt_bufsize ) ;
			if( decrypt_buffer )
			{
				http_secure_env->decrypt_bufsize = decrypt_bufsize ;
				http_secure_env->decrypt_buffer = decrypt_buffer ;
			}
		}
	}
	
	if( http_secure_env->encrypt_buffer )
	{
		char	*encrypt_buffer = NULL ;
		int	encrypt_bufsize ;
		
		encrypt_bufsize = 1024*1024 ;
		if( http_secure_env->encrypt_bufsize > encrypt_bufsize )
		{
			encrypt_buffer = (char*)realloc( http_secure_env->encrypt_buffer , encrypt_bufsize ) ;
			if( encrypt_buffer )
			{
				http_secure_env->encrypt_bufsize = encrypt_bufsize ;
				http_secure_env->encrypt_buffer = encrypt_buffer ;
			}
		}
	}
	
	return;
}

void DestroyHttpSecureEnv( struct HttpSecureEnv *http_secure_env )
{
	if( http_secure_env )
	{
		if( http_secure_env->this_rsa )
		{
			FreeRsaKey( http_secure_env->this_rsa );
			http_secure_env->this_rsa = NULL ;
		}
		
		if( http_secure_env->remote_rsa )
		{
			FreeRsaKey( http_secure_env->remote_rsa );
			http_secure_env->remote_rsa = NULL ;
		}
		
		if( http_secure_env->compress_buffer )
		{
			free( http_secure_env->compress_buffer );
			http_secure_env->compress_buffer = NULL ;
		}
		
		if( http_secure_env->decompress_buffer )
		{
			free( http_secure_env->decompress_buffer );
			http_secure_env->decompress_buffer = NULL ;
		}
		
		if( http_secure_env->encrypt_buffer )
		{
			free( http_secure_env->encrypt_buffer );
			http_secure_env->encrypt_buffer = NULL ;
		}
		
		if( http_secure_env->decrypt_buffer )
		{
			free( http_secure_env->decrypt_buffer );
			http_secure_env->decrypt_buffer = NULL ;
		}
		
		free( http_secure_env );
	}
	
	return;
}

int ReloadRsaKey( struct HttpSecureEnv *http_secure_env , struct NodeSpaceUnit *p_this_node_unit , struct NodeSpaceUnit *p_remote_node_unit )
{
	if( p_this_node_unit && p_this_node_unit != http_secure_env->p_this_node_unit )
	{
		if( http_secure_env->this_rsa )
		{
			FreeRsaKey( http_secure_env->this_rsa );
		}
		
		http_secure_env->this_rsa = AllocPrivateKey( p_this_node_unit->key , p_this_node_unit->key_len ) ;
		if( http_secure_env->this_rsa == NULL )
		{
			ERRORLOGSG( "AllocPrivateKey failed , node[%s] errno[%d]" , p_this_node_unit->node , errno );
			return IBP_ERROR_ALLOC_PRIVATE_KEY;
		}
		else
		{
			DEBUGLOGSG( "AllocPrivateKey ok , node[%s]" , p_this_node_unit->node );
		}
	}
	
	if( p_remote_node_unit && p_remote_node_unit != http_secure_env->p_remote_node_unit )
	{
		if( http_secure_env->remote_rsa )
		{
			FreeRsaKey( http_secure_env->remote_rsa );
		}
		
		http_secure_env->remote_rsa = AllocPublicKey( p_remote_node_unit->key , p_remote_node_unit->key_len ) ;
		if( http_secure_env->remote_rsa == NULL )
		{
			ERRORLOGSG( "AllocPublicKey failed , node[%s] errno[%d]" , p_remote_node_unit->node , errno );
			return IBP_ERROR_ALLOC_PUBLIC_KEY;
		}
		else
		{
			DEBUGLOGSG( "AllocPublicKey ok , node[%s]" , p_remote_node_unit->node );
		}
	}
	
	return 0;
}

int PutMessageToHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char *msg , int msg_len )
{
	char		*ptr = NULL ;
	int		len ;
	
	char		*compress_buffer = NULL ;
	int		decrypt_buflen ;
	char		*decrypt_buffer = NULL ;
	int		encrypt_buflen ;
	char		*encrypt_buffer = NULL ;
	
	int		nret = 0 ;
	
	DEBUGHEXLOGSG( msg , msg_len , "MSG [%d]BYTES" , msg_len );
	ptr = msg ;
	len = msg_len ;
	
	nret = StrcatfHttpBuffer( b , "%s: %d" HTTP_RETURN_NEWLINE , IBP_HTTPHEADER_IBP_ORIGIN_CONTENT_LENGTH , msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return -1;
	}
	
	/* 签名报文 */
	if( http_secure_env->sign_flag == IBP_HTTPSECURE_SIGN_FLAG_MD5 )
	{
		char		md5_result_exp[ MD5_DIGEST_LENGTH*2 + 1 ] ;
		
		memset( md5_result_exp , 0x00 , sizeof(md5_result_exp) );
		Md5Data( ptr , len , md5_result_exp );
		DEBUGLOGSG( "md5_result_exp[%s]" , md5_result_exp );
		
		nret = StrcatfHttpBuffer( b ,	"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						, IBP_HTTPHEADER_IBP_CONTENT_SIGN , IBP_HTTPSECURE_SIGN_FLAG_MD5_STR
						, IBP_HTTPHEADER_IBP_CONTENT_SIGN_MD5 , md5_result_exp ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return -1;
		}
	}
	
	/* 压缩报文 */
	if( http_secure_env->compress_flag == IBP_HTTPSECURE_COMPRESS_FLAG_GZIP )
	{
		z_stream	stream ;
		int		out_len ;
		unsigned char	*out_base = NULL ;
		
		stream.zalloc = NULL ;
		stream.zfree = NULL ;
		stream.opaque = NULL ;
		nret = deflateInit2( &stream , Z_DEFAULT_COMPRESSION , Z_DEFLATED , HTTP_COMPRESSALGORITHM_GZIP , MAX_MEM_LEVEL , Z_DEFAULT_STRATEGY ) ;
		if( nret != Z_OK )
		{
			ERRORLOGSG( "deflateInit2 failed , errno[%d]" , errno );
			return -1;
		}
		
		out_len = deflateBound( & stream , len ) ;
		out_len = CALC_8BYTES_ALIGNMENT( out_len ) ;
		if( http_secure_env->compress_buffer == NULL || out_len > http_secure_env->compress_bufsize-1 )
		{
			compress_buffer = (char *)realloc( http_secure_env->compress_buffer , out_len+1 ) ;
			if( compress_buffer == NULL )
			{
				ERRORLOGSG( "malloc failed , errno[%d]" , errno );
				deflateEnd( & stream );
				return -1;
			}
			http_secure_env->compress_bufsize = out_len+1 ;
			http_secure_env->compress_buffer = compress_buffer ;
		}
		memset( http_secure_env->compress_buffer , 0x00 , http_secure_env->compress_bufsize );
		
		DEBUGHEXLOGSG( ptr , len , "before compressing comm data" );
		stream.next_in = (unsigned char *)ptr ;
		stream.avail_in = len ;
		stream.next_out = (unsigned char *)(http_secure_env->compress_buffer) ;
		stream.avail_out = out_len ;
		nret = deflate( &stream , Z_FINISH ) ;
		if( nret != Z_OK && nret != Z_STREAM_END )
		{
			ERRORLOGSG( "deflate failed , errno[%d]" , errno );
			free( out_base );
			deflateEnd( & stream );
			return -1;
		}
		
		deflateEnd( & stream );
		
		nret = StrcatfHttpBuffer( b ,	"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %d" HTTP_RETURN_NEWLINE
						, IBP_HTTPHEADER_IBP_CONTENT_COMPRESS , IBP_HTTPSECURE_COMPRESS_FLAG_GZIP_STR
						, IBP_HTTPHEADER_IBP_DECOMPRESS_CONTENT_LENGTH , len ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return -1;
		}
		
		ptr = http_secure_env->compress_buffer  ;
		len = CALC_8BYTES_ALIGNMENT( stream.total_out ) ;
		DEBUGHEXLOGSG( ptr , len , "after compressing comm data" );
	}
	
	/* 加密报文 */
	if( http_secure_env->encrypt_flag == IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA )
	{
		/* 生成3DES密钥 */
		if( http_secure_env->key_3des_generated_flag == 0 )
		{
			/* 随机生成传输3DES密钥 */
			Generate3DesKey( http_secure_env->key_3des_dec , IBP_3DES_KEYSIZE_BYTES );
			
			/* 用服务端公钥加密传输3DES密钥 */
			DEBUGHEXLOGSG( http_secure_env->key_3des_dec , IBP_RSA_KEYSIZE_BITS/8 , "before encrypting comm key" );
			nret = Encrypt_RSA_PKCS1( http_secure_env->remote_rsa , http_secure_env->key_3des_dec , IBP_RSA_KEYSIZE_BITS/8 , http_secure_env->key_3des_enc , NULL ) ;
			if( nret )
			{
				ERRORLOGSG( "Encrypt_RSA_PKCS1 failed[%d] , ERR_peek_error()[%d] ERR_error_string()[%s]" , nret , ERR_peek_error() , ERR_error_string(ERR_peek_error(),NULL) );
				return -1;
			}
			DEBUGHEXLOGSG( http_secure_env->key_3des_enc , (int)sizeof(http_secure_env->key_3des_dec)-1 , "after encrypting comm key" );
			memset( http_secure_env->key_3des_enc_exp , 0x00 , sizeof(http_secure_env->key_3des_enc_exp) );
			HexExpand( http_secure_env->key_3des_enc , IBP_RSA_KEYSIZE_BITS/8 , http_secure_env->key_3des_enc_exp );
			
			nret = StrcatfHttpBuffer( b ,	"%s: %s" HTTP_RETURN_NEWLINE
							, IBP_HTTPHEADER_IBP_3DES_KEY , http_secure_env->key_3des_enc_exp ) ;
			if( nret )
			{
				ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
				return -1;
			}
			
			http_secure_env->key_3des_generated_flag = 1 ;
		}
		
		if( http_secure_env->remote_rsa == NULL )
		{
			ERRORLOGSG( "NO RSA" );
			return -1;
		}
		
		decrypt_buflen = CALC_8BYTES_ALIGNMENT(len) ;
		if( http_secure_env->decrypt_buffer == NULL || decrypt_buflen > http_secure_env->decrypt_bufsize-1 )
		{
			decrypt_buffer = (char *)realloc( http_secure_env->decrypt_buffer , decrypt_buflen+1 ) ;
			if( decrypt_buffer == NULL )
			{
				ERRORLOGSG( "realloc failed , errno[%d]" , errno );
				return -1;
			}
			http_secure_env->decrypt_bufsize = decrypt_buflen+1 ;
			http_secure_env->decrypt_buffer = decrypt_buffer ;
		}
		memset( http_secure_env->decrypt_buffer , 0x00 , http_secure_env->decrypt_bufsize );
		memcpy( http_secure_env->decrypt_buffer , ptr , len );
		ptr = http_secure_env->decrypt_buffer ;
		len = decrypt_buflen ;
		
		encrypt_buflen = CALC_8BYTES_ALIGNMENT(len) ;
		if( http_secure_env->encrypt_buffer == NULL || encrypt_buflen > http_secure_env->encrypt_bufsize-1 )
		{
			encrypt_buffer = (char *)realloc( http_secure_env->encrypt_buffer , encrypt_buflen+1 ) ;
			if( encrypt_buffer == NULL )
			{
				ERRORLOGSG( "malloc failed , errno[%d]" , errno );
				return -1;
			}
			http_secure_env->encrypt_bufsize = encrypt_buflen+1 ;
			http_secure_env->encrypt_buffer = encrypt_buffer ;
		}
		memset( http_secure_env->encrypt_buffer , 0x00 , http_secure_env->encrypt_bufsize );
		
		/* 用传输3DES密钥加密报文 */
		DEBUGHEXLOGSG( http_secure_env->key_3des_dec , IBP_RSA_KEYSIZE_BITS/8 , "key_3des_dec" );
		DEBUGHEXLOGSG( ptr , len , "before encrypting comm data" );
		nret = Encrypt_3DES_192bits( http_secure_env->key_3des_dec , ptr , len , http_secure_env->encrypt_buffer , & (http_secure_env->encrypt_bufsize) , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "Encrypt_3DES_192bits failed[%d] , errno[%d]" , nret , errno );
			return -1;
		}
		DEBUGHEXLOGSG( http_secure_env->encrypt_buffer , http_secure_env->encrypt_bufsize , "after encrypting comm data" );
		
		nret = StrcatfHttpBuffer( b ,	"%s: %s" HTTP_RETURN_NEWLINE
						, IBP_HTTPHEADER_IBP_CONTENT_ENCRYPT , IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA_STR ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return -1;
		}
		
		ptr = http_secure_env->encrypt_buffer ;
		len = http_secure_env->encrypt_bufsize ;
	}
	
	/* 追加头选项 */
	nret = StrcatfHttpBuffer( b ,	"%s: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					, HTTP_HEADER_CONTENT_LENGTH , len ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return -1;
	}
	
	/* 追加报文 */
	nret = MemcatHttpBuffer( b , ptr , len ) ;
	if( nret )
	{
		ERRORLOGSG( "MemcatHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return -1;
	}
	
	DEBUGHEXLOGSG( GetHttpBufferBase(b,NULL) , GetHttpBufferLength(b) , "HTTP [%d]BYTES" , GetHttpBufferLength(b) );
	
	return 0;
}

int PutFileToHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char *pathfilename , int *p_file_len )
{
	char		*file_content = NULL ;
	int		file_len ;
	
	int		nret = 0 ;
	
	file_content = StrdupEntireFile( pathfilename , & file_len ) ;
	if( file_content == NULL )
	{
		ERRORLOGSG( "StrdupEntireFile[%s] failed , errno[%d]" , pathfilename , errno );
		return -1;
	}
	
	nret = PutMessageToHttpBuffer( e , b , http_secure_env , file_content , file_len ) ;
	free( file_content );
	if( file_content == NULL )
	{
		ERRORLOGSG( "SetMessageToHttpBuffer failed[%d]" , nret );
		return -1;
	}
	
	if( p_file_len )
		(*p_file_len) = file_len ;
	
	return 0;
}

int ExtractMessageFromHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char **pp_msg , int *p_msg_len )
{
	char		*ptr = NULL ;
	int		len ;
	
	char		*p_ORIGIN_CONTENT_LENGTH = NULL ;
	int		ORIGIN_CONTENT_LENGTH_len ;
	int		origin_content_len ;
	
	char		*p_CONTENT_ENCRYPT = NULL ;
	int		CONTENT_ENCRYPT_len ;
	char		*p_3DES_KEY = NULL ;
	int		_3DES_KEY_len ;
	int		decrypt_buflen ;
	int		encrypt_buflen ;
	
	char		*p_CONTENT_COMPRESS = NULL ;
	int		CONTENT_COMPRESS_len ;
	char		*p_DECOMPRESS_CONTENT_LENGTH = NULL ;
	int		DECOMPRESS_CONTENT_LENGTH_len;
	int		decompress_buflen ;
	
	char		*p_CONTENT_SIGN = NULL ;
	int		CONTENT_SIGN_len ;
	char		*p_CONTENT_SIGN_MD5 = NULL ;
	int		CONTENT_SIGN_MD5_len ;
	
	int		nret = 0 ;
	
	DEBUGHEXLOGSG( GetHttpBufferBase(b,NULL) , GetHttpBufferLength(b) , "HTTP [%d]BYTES" , GetHttpBufferLength(b) );
	
	/* 准备数据 */
	ptr = GetHttpBodyPtr( e , & len ) ;
	if( ptr == NULL )
	{
		if( pp_msg )
			(*pp_msg) = ptr ;
		if( p_msg_len )
			(*p_msg_len) = len ;
		DEBUGHEXLOGSG( ptr , len , "MSG [%d]BYTES" , len );
		return 0;
	}
	
	p_ORIGIN_CONTENT_LENGTH = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_ORIGIN_CONTENT_LENGTH , & ORIGIN_CONTENT_LENGTH_len ) ;
	if( p_ORIGIN_CONTENT_LENGTH == NULL )
	{
		if( pp_msg )
			(*pp_msg) = ptr ;
		if( p_msg_len )
			(*p_msg_len) = len ;
		DEBUGHEXLOGSG( ptr , len , "MSG [%d]BYTES" , len );
		return 0;
	}
	origin_content_len = nstoi( p_ORIGIN_CONTENT_LENGTH , ORIGIN_CONTENT_LENGTH_len ) ;
	
	/* 解密报文 */
	p_CONTENT_ENCRYPT = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_CONTENT_ENCRYPT , & CONTENT_ENCRYPT_len ) ;
	if( p_CONTENT_ENCRYPT )
	{
		if( CONTENT_ENCRYPT_len == sizeof(IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA_STR)-1 && MEMCMP( p_CONTENT_ENCRYPT , == , IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA_STR , sizeof(IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA_STR)-1 ) )
		{
			p_3DES_KEY = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_3DES_KEY , & _3DES_KEY_len ) ;
			if( p_3DES_KEY )
			{
				if( _3DES_KEY_len != IBP_RSA_KEYSIZE_BITS/8*2 )
				{
					ERRORLOGSG( "HTTP header[%s][%.*s] length invalid" , IBP_HTTPHEADER_IBP_3DES_KEY , _3DES_KEY_len , p_3DES_KEY );
					return -1;
				}
				
				memset( http_secure_env->key_3des_enc , 0x00 , sizeof(http_secure_env->key_3des_enc) );
				HexFold( p_3DES_KEY , IBP_RSA_KEYSIZE_BITS/8*2 , (char*)(http_secure_env->key_3des_enc) );
				DEBUGHEXLOGSG( http_secure_env->key_3des_enc , IBP_RSA_KEYSIZE_BITS/8 , "before decrypting comm key" );
				nret = Decrypt_RSA_PKCS1( http_secure_env->this_rsa , http_secure_env->key_3des_enc , IBP_RSA_KEYSIZE_BITS/8 , http_secure_env->key_3des_dec , NULL ) ;
				if( nret )
				{
					ERRORLOGSG( "Decrypt_RSA_PKCS1 failed[%d] , ERR_peek_error()[%d] ERR_error_string()[%s]" , nret , ERR_peek_error() , ERR_error_string(ERR_peek_error(),NULL) );
					return -1;
				}
				DEBUGHEXLOGSG( http_secure_env->key_3des_dec , IBP_RSA_KEYSIZE_BITS/8 , "after decrypting comm key" );
				
				http_secure_env->key_3des_generated_flag = 1 ;
			}
			
			if( http_secure_env->key_3des_generated_flag == 0 )
			{
				ERRORLOGSG( "NO 3DES KEY" );
				return -1;
			}
			
			encrypt_buflen = CALC_8BYTES_ALIGNMENT(len) ;
			if( http_secure_env->encrypt_buffer == NULL || encrypt_buflen > http_secure_env->encrypt_bufsize-1 )
			{
				char		*encrypt_buffer = NULL ;
				
				encrypt_buffer = (char *)realloc( http_secure_env->encrypt_buffer , encrypt_buflen+1 ) ;
				if( encrypt_buffer == NULL )
				{
					ERRORLOGSG( "realloc failed , errno[%d]" , errno );
					return -1;
				}
				http_secure_env->encrypt_bufsize = encrypt_buflen+1 ;
				http_secure_env->encrypt_buffer = encrypt_buffer ;
			}
			memset( http_secure_env->compress_buffer , 0x00 , http_secure_env->compress_bufsize );
			memcpy( http_secure_env->encrypt_buffer , ptr , len );
			ptr = http_secure_env->encrypt_buffer ;
			len = encrypt_buflen ;
			
			decrypt_buflen = CALC_8BYTES_ALIGNMENT(len) ;
			if( http_secure_env->decrypt_buffer == NULL || decrypt_buflen > http_secure_env->decrypt_bufsize-1 )
			{
				char		*decrypt_buffer = NULL ;
				
				decrypt_buffer = (char *)realloc( http_secure_env->decrypt_buffer , decrypt_buflen+1 ) ;
				if( decrypt_buffer == NULL )
				{
					ERRORLOGSG( "realloc failed , errno[%d]" , errno );
					return -1;
				}
				http_secure_env->decrypt_bufsize = decrypt_buflen+1 ;
				http_secure_env->decrypt_buffer = decrypt_buffer ;
			}
			memset( http_secure_env->decrypt_buffer , 0x00 , http_secure_env->decrypt_bufsize );
			
			DEBUGHEXLOGSG( ptr , len , "before decrypting comm data" );
			nret = Decrypt_3DES_192bits( http_secure_env->key_3des_dec , ptr , len , http_secure_env->decrypt_buffer , & decrypt_buflen , NULL ) ;
			if( nret )
			{
				ERRORLOGSG( "Encrypt_3DES_192bits failed[%d] , errno[%d]" , nret , errno );
				return -1;
			}
			DEBUGHEXLOGSG( http_secure_env->decrypt_buffer , decrypt_buflen , "after decrypting comm data" );
			
			ptr = http_secure_env->decrypt_buffer ;
			len = decrypt_buflen ;
		}
		else
		{
			ERRORLOGSG( "HTTP header[%s][%.*s] invalid" , IBP_HTTPHEADER_IBP_3DES_KEY , CONTENT_ENCRYPT_len , p_CONTENT_ENCRYPT );
			return -1;
		}
	}
	
	/* 解压报文 */
	p_CONTENT_COMPRESS = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_CONTENT_COMPRESS , & CONTENT_COMPRESS_len ) ;
	if( p_CONTENT_COMPRESS )
	{
		if( CONTENT_COMPRESS_len == sizeof(IBP_HTTPSECURE_COMPRESS_FLAG_GZIP_STR)-1 && MEMCMP( p_CONTENT_COMPRESS , == , IBP_HTTPSECURE_COMPRESS_FLAG_GZIP_STR , sizeof(IBP_HTTPSECURE_COMPRESS_FLAG_GZIP_STR)-1 ) )
		{
			z_stream	stream ;
			
			p_DECOMPRESS_CONTENT_LENGTH = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_DECOMPRESS_CONTENT_LENGTH , & DECOMPRESS_CONTENT_LENGTH_len ) ;
			if( p_DECOMPRESS_CONTENT_LENGTH == NULL )
			{
				ERRORLOGSG( "HTTP header[%s] not found" , IBP_HTTPHEADER_IBP_CONTENT_COMPRESS );
				return -1;
			}
			
			decompress_buflen = nstoi( p_DECOMPRESS_CONTENT_LENGTH , DECOMPRESS_CONTENT_LENGTH_len ) ;
			if( http_secure_env->decompress_buffer == NULL || decompress_buflen > http_secure_env->decompress_bufsize-1 )
			{
				char		*decompress_buffer = NULL ;
				
				decompress_buffer = (char*)realloc( http_secure_env->decompress_buffer , decompress_buflen+1 ) ;
				if( decompress_buffer == NULL )
				{
					ERRORLOGSG( "realloc failed , errno[%d]" , errno );
					return -1;
				}
				http_secure_env->decompress_bufsize = decompress_buflen+1 ;
				http_secure_env->decompress_buffer = decompress_buffer ;
			}
			memset( http_secure_env->decompress_buffer , 0x00 , http_secure_env->decompress_bufsize );
			
			memset( & stream , 0x00 , sizeof(z_stream) );
			stream.zalloc = NULL ;
			stream.zfree = NULL ;
			stream.opaque = NULL ;
			nret = inflateInit2( & stream , HTTP_COMPRESSALGORITHM_GZIP ) ;
			if( nret != Z_OK )
			{
				ERRORLOGSG( "inflateInit2 failed[%d]" , nret );
				return -1;
			}
			
			DEBUGHEXLOGSG( ptr , len , "before decompressing comm data" );
			stream.next_in = (unsigned char *)ptr ;
			stream.avail_in = len ;
			stream.next_out = (unsigned char *)(http_secure_env->decompress_buffer) ;
			stream.avail_out = http_secure_env->decompress_bufsize ;
			nret = inflate( &stream , Z_NO_FLUSH ) ;
			if( nret != Z_OK && nret != Z_STREAM_END )
			{
				ERRORLOGSG( "inflate failed[%d]" , nret );
				inflateEnd( & stream );
				return -1;
			}
			
			inflateEnd( & stream );
			
			ptr = http_secure_env->decompress_buffer ;
			len = stream.total_out ;
			DEBUGHEXLOGSG( ptr , len , "after decompressing comm data" );
		}
		else
		{
			ERRORLOGSG( "HTTP header[%s][%.*s] invalid" , IBP_HTTPHEADER_IBP_CONTENT_COMPRESS , CONTENT_COMPRESS_len , p_CONTENT_COMPRESS );
			return -1;
		}
	}
	
	/* 验签报文 */
	p_CONTENT_SIGN = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_CONTENT_SIGN , & CONTENT_SIGN_len ) ;
	if( p_CONTENT_SIGN )
	{
		if( CONTENT_SIGN_len == sizeof(IBP_HTTPSECURE_SIGN_FLAG_MD5_STR)-1 && MEMCMP( p_CONTENT_SIGN , == , IBP_HTTPSECURE_SIGN_FLAG_MD5_STR , sizeof(IBP_HTTPSECURE_SIGN_FLAG_MD5_STR)-1 ) )
		{
			char		md5_result_exp[ MD5_DIGEST_LENGTH*2 + 1 ] ;
			
			p_CONTENT_SIGN_MD5 = QueryHttpHeaderPtr( e , IBP_HTTPHEADER_IBP_CONTENT_SIGN_MD5 , & CONTENT_SIGN_MD5_len ) ;
			if( p_CONTENT_SIGN_MD5 == NULL )
			{
				ERRORLOGSG( "HTTP header[%s] not found" , IBP_HTTPHEADER_IBP_CONTENT_SIGN_MD5 );
				return -1;
			}
			if( CONTENT_SIGN_MD5_len != MD5_DIGEST_LENGTH*2 )
			{
				ERRORLOGSG( "HTTP header[%s][%.*s] length invalid" , IBP_HTTPHEADER_IBP_CONTENT_SIGN_MD5 , CONTENT_SIGN_MD5_len , p_CONTENT_SIGN_MD5 );
				return -1;
			}
			
			memset( md5_result_exp , 0x00 , sizeof(md5_result_exp) );
			Md5Data( ptr , origin_content_len , md5_result_exp );
			DEBUGLOGSG( "md5_result_exp[%s]" , md5_result_exp );
			if( MEMCMP( p_CONTENT_SIGN_MD5 , != , md5_result_exp , CONTENT_SIGN_MD5_len ) )
			{
				ERRORLOGSG( "MD5[%s] not matched [%.*s] in HTTP header" , md5_result_exp , CONTENT_SIGN_MD5_len , p_CONTENT_SIGN_MD5 );
				return -1;
			}
			else
			{
				INFOLOGSG( "MD5[%s] matched [%.*s] in HTTP header" , md5_result_exp , CONTENT_SIGN_MD5_len , p_CONTENT_SIGN_MD5 );
			}
		}
		else
		{
			ERRORLOGSG( "HTTP header[%s][%.*s] invalid" , IBP_HTTPHEADER_IBP_CONTENT_SIGN , CONTENT_SIGN_len , p_CONTENT_SIGN );
			return -1;
		}
	}
	
	if( pp_msg )
		(*pp_msg) = ptr ;
	if( p_msg_len )
		(*p_msg_len) = origin_content_len ;
	DEBUGHEXLOGSG( ptr , origin_content_len , "MSG [%d]BYTES" , origin_content_len );
	
	return 0;
}

int ExtractFileFromHttpBuffer( struct HttpEnv *e , struct HttpBuffer *b , struct HttpSecureEnv *http_secure_env , char *pathfilename , int *p_file_len )
{
	char		*file_content = NULL ;
	int		file_len ;
	
	int		nret = 0 ;
	
	nret = ExtractMessageFromHttpBuffer( e , b , http_secure_env , & file_content , & file_len ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed[%d]" , nret );
		return -1;
	}
	
	if( access( pathfilename , R_OK ) == 0 )
	{
		unlink( pathfilename );
	}
	
	nret = WriteEntireFile( pathfilename , file_content , file_len ) ;
	if( nret )
	{
		ERRORLOGSG( "WriteEntireFile failed[%d]" , nret );
		return -1;
	}
	
	if( p_file_len )
		(*p_file_len) = file_len ;
	
	return 0;
}

int PutMessageToHttpBody( struct HttpEnv *e , struct HttpBuffer *b , char **pp_msg , int msg_len )
{
	char		*body = NULL ;
	int		body_len ;
	
	int		nret = 0 ;
	
	body = GetHttpBodyPtr( e , & body_len ) ;
	if( body == NULL )
		return 0;
	
	if( msg_len > body_len )
	{
		nret = ReallocHttpBuffer( b , GetHttpBufferSize(b)+(msg_len-body_len) ) ;
		if( nret )
		{
			ERRORLOGSG( "ReallocHttpBuffer failed[%d]" , nret );
			return -1;
		}
		
		body = GetHttpBodyPtr( e , & body_len ) ;
		memcpy( body , (*pp_msg) , msg_len );
		body[msg_len] = '\0' ;
		(*pp_msg) = body ;
	}
	else
	{
		memcpy( body , (*pp_msg) , msg_len );
		if( msg_len < body_len )
		{
			memset( body+msg_len , 0x00 , body_len-msg_len );
			body[msg_len] = '\0' ;
		}
		(*pp_msg) = body ;
	}
	
	return 0;
}

