#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ibp_err.h"

#include "ibp_err_table.c"

static struct IbpErr *GetIbpErr( struct IbpErr *array , int err )
{
	struct IbpErr	*p_ibp_err = NULL ;
	
	for( p_ibp_err = array ; p_ibp_err->desc ; p_ibp_err++ )
	{
		if( p_ibp_err->err == err )
			return p_ibp_err;
	}
	
	return NULL;
}

static int ibp_err( int err_code )
{
	int		module_no ;
	int		err_no ;
	int		fasterhttp_err_no ;
	struct IbpErr	*p_module_unit = NULL ;
	struct IbpErr	*p_err_unit = NULL ;
	struct IbpErr	*p_fasterhttp_err_unit = NULL ;
	struct IbpErr	null_module_unit = { 0 , "(δ֪ģ��)" } ;
	struct IbpErr	null_err_unit = { 0 , "(δ֪����)" } ;
	
	module_no = (int)(abs(err_code)/1000)*1000 ;
	err_no = err_code % 1000 ;
	
	p_module_unit = GetIbpErr( g_astIbpErrTable , module_no ) ;
	if( p_module_unit == NULL )
		p_module_unit = & null_module_unit ;
	if( ((int)(err_no/100)*100) != IBP_ERROR_FASTERHTTP_BASE )
	{
		p_err_unit = GetIbpErr( g_astIbpErrTable , err_no ) ;
		if( p_err_unit == NULL )
			p_err_unit = & null_err_unit ;
	}
	else
	{
		fasterhttp_err_no = err_no - IBP_ERROR_FASTERHTTP_BASE ;
		err_no = IBP_ERROR_FASTERHTTP_BASE ;
		
		p_err_unit = GetIbpErr( g_astIbpErrTable , err_no ) ;
		if( p_err_unit == NULL )
			p_err_unit = & null_err_unit ;
		p_fasterhttp_err_unit = GetIbpErr( g_astFasterhttpErrTable , fasterhttp_err_no ) ;
		if( p_fasterhttp_err_unit == NULL )
			p_fasterhttp_err_unit = & null_err_unit ;
	}
	
	printf( "%s %s %s\n" , p_module_unit->desc , p_err_unit->desc , (p_fasterhttp_err_unit?p_fasterhttp_err_unit->desc:"") );
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc == 1 + 1 )
	{
		return -ibp_err( atoi(argv[1]) );
	}
	else
	{
		printf( "USAGE : ibp_err err_code\n" );
		exit(7);
	}
}

