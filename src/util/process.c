#include "ibp_util.h"

int BindDaemonServer( int (* ServerMain)( void *pv ) , void *pv )
{
	int	pid;
	
	close(0);
	close(1);
	close(2);
	
	umask( 0 ) ;
	
	chdir( "/tmp" );
	
	pid = fork() ;
	switch( pid )
	{
		case -1:
			return -1;
		case 0:
			break;
		default		:
			return 0;
	}
	
	pid = fork() ;
	switch( pid )
	{
		case -1:
			return -2;
		case 0:
			break ;
		default:
			return 0;
	}
	
	ServerMain( pv );
	
	return 0;
}
