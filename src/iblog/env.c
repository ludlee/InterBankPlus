#include "iblog_in.h"

int InitEnvironment( struct IblogEnv *p_env )
{
	INIT_LIST_HEAD( & (p_env->accepted_session_list.this_node) );
	
	return 0;
}

void CleanEnvironment( struct IblogEnv *p_env )
{
	struct list_head		*node = NULL , *next = NULL ;
	struct AcceptedSession		*p_accepted_session = NULL ;
	
	DestroyIblogHandleTree( p_env );
	
	list_for_each_safe( node , next , & (p_env->accepted_session_list.this_node) )
	{
		list_del( node );
		p_accepted_session = list_entry( node , struct AcceptedSession , this_node ) ;
		free( p_accepted_session );
	}
	
	return;
}

