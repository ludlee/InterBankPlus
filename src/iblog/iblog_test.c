#include "ibp_util.h"

/*
iblog_test "127.0.0.1:16001" "service_name" "file::log/event.log" DEBUG "file::log/iblog_test.log"
iblog_test "127.0.0.1:16001" "service_name" "iblog::log/event.log" DEBUG "iblog::log/iblog_test.log"
iblog_test "127.0.0.1:16001" "service_name" "iblog::log/event.log" DEBUG "iblog::log/iblog_test.log" NOTICE "iblog::log/iblog_test2.log"
*/

int iblog_test( char *iblog_server , char *service_name , char *event_output , int process_loglevel , char *process_output , int change_process_loglevel , char *change_process_output )
{
	int		nret = 0 ;
	
	nret = IBPInitLogEnv( iblog_server , service_name , event_output , process_loglevel , "%s" , process_output ) ;
	if( nret )
	{
		printf( "InitLogEnv[%s][%s][%s][%d][%s] failed[%d]\n" , iblog_server , service_name , event_output , process_loglevel , process_output , nret );
		return -1;
	}
	
	DEBUGLOGSG( "DEBUGLOGSG" );
	INFOLOGSG( "INFOLOGSG" );
	NOTICELOGSG( "NOTICELOGSG" );
	WARNLOGSG( "WARNLOGSG" );
	ERRORLOGSG( "ERRORLOGSG" );
	FATALLOGSG( "FATALLOGSG" );
	
	if( change_process_output )
	{
		sleep(2);
		
		nret = IBPChangeProcessLogFilename( "%s" , change_process_output ) ;
		if( nret )
		{
			printf( "ChangeProcessLogFilename[%s] failed[%d]\n" , change_process_output , nret );
			return -2;
		}
		
		IBPChangeProcessLogLevel( change_process_loglevel );
		
		DEBUGLOGSG( "DEBUGLOGSG2" );
		INFOLOGSG( "INFOLOGSG2" );
		NOTICELOGSG( "NOTICELOGSG2" );
		WARNLOGSG( "WARNLOGSG2" );
		ERRORLOGSG( "ERRORLOGSG2" );
		FATALLOGSG( "FATALLOGSG2" );
	}
	
	IBPCleanLogEnv();
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc == 1 + 5 )
	{
		return -iblog_test( argv[1] , argv[2] , argv[3] , IBPConvertLogLevel(argv[4]) , argv[5] , 0 , NULL );
	}
	else if( argc == 1 + 7 )
	{
		return -iblog_test( argv[1] , argv[2] , argv[3] , IBPConvertLogLevel(argv[4]) , argv[5] , IBPConvertLogLevel(argv[6]) , argv[7] );
	}
	else
	{
		printf( "USAGE : iblog iblog_server service_name event_output process_loglevel process_output [ change_process_loglevel change_process_output ]\n" );
		exit(7);
	}
}

