#include "iblog_in.h"

#include "rbtree_tpl.h"

LINK_RBTREENODE_STRING( LinkIblogHandleTreeNode , struct IblogEnv , log_handle_rbtree , struct IblogHandle , log_handle_rbnode , log_pathfilename )
QUERY_RBTREENODE_STRING( QueryIblogHandleTreeNode , struct IblogEnv , log_handle_rbtree , struct IblogHandle , log_handle_rbnode , log_pathfilename )
UNLINK_RBTREENODE( UnlinkIblogHandleTreeNode , struct IblogEnv , log_handle_rbtree , struct IblogHandle , log_handle_rbnode )
DESTROY_RBTREE( DestroyIblogHandleTree , struct IblogEnv , log_handle_rbtree , struct IblogHandle , log_handle_rbnode , FREE_RBTREENODEENTRY_DIRECTLY )

