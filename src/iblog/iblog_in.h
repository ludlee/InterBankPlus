#ifndef _H_IBLOG_IN_
#define _H_IBLOG_IN_

#include "ibp_util.h"

#include "IDL_iblog_conf.dsc.h"

extern char     *g_ibp_iblog_kernel_version ;

#define MAX_EPOLL_EVENTS		10000

/*
communication protocol :
	(comm_body_len netorder 4bytes) | (type 1bytes) | (log_filename nbytes)
	(comm_body_len netorder 4bytes) | (type 1bytes) | (log_level 1bytes) | (log_content nbytes)
type :
	'0' set log filename
	'1' output log
*/

struct IblogHandle
{
	char			log_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	LOG			*g ;
	int			ref_count ;
	
	struct rb_node		log_handle_rbnode ;
} ;

struct ListenSession
{
	struct NetAddress	netaddr ;
} ;

struct PipeSession
{
	int			pipe_fds[ 2 ] ;
} ;

#define IBLOG_COMMBUFFER_INIT_SIZE	40960
#define IBLOG_COMMBUFFER_INCREASE_SIZE	40960

struct AcceptedSession
{
	struct NetAddress	netaddr ;
	
	char			*comm_buf ;
	int			comm_buf_size ;
	int			comm_data_len ;
	int			comm_body_len ;
	
	struct IblogHandle	*p_log_handle ;
	
	struct list_head	this_node ;
} ;

struct IblogConfig
{
	iblog_conf		iblog_conf ;
} ;

struct CommandParameter
{
	char			*_action ;
} ;

struct IblogEnv
{
	struct CommandParameter		cmd_param ;
	
	char				*iblog_conf_filename ;
	char				iblog_conf_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	
	iblog_conf			iblog_conf ;
	
	struct rb_root			log_handle_rbtree ;
	
	int				epoll_fd ;
	
	struct ListenSession		listen_session ;
	struct PipeSession		pipe_session ;
	struct AcceptedSession		accepted_session_list ;
} ;

int LoadConfig( struct IblogEnv *p_env );

int InitEnvironment( struct IblogEnv *p_env );
void CleanEnvironment( struct IblogEnv *p_env );

int _monitor( void *pv );

int worker( struct IblogEnv *p_env );

int OnAcceptingSocket( struct IblogEnv *p_env , struct ListenSession *p_listen_session );
void OnClosingSocket( struct IblogEnv *p_env , struct AcceptedSession *p_accepted_session );
int OnReceivingSocket( struct IblogEnv *p_env , struct AcceptedSession *p_accepted_session );

int OnAppProcess( struct IblogEnv *p_env , struct AcceptedSession *p_accepted_session );

int LinkIblogHandleTreeNode( struct IblogEnv *p_env , struct IblogHandle *p_log_handle );
struct IblogHandle *QueryIblogHandleTreeNode( struct IblogEnv *p_env , struct IblogHandle *p_log_handle );
void UnlinkIblogHandleTreeNode( struct IblogEnv *p_env , struct IblogHandle *p_log_handle );
void DestroyIblogHandleTree( struct IblogEnv *p_env );

#endif

