#include "iblog_in.h"

#define PREFIX_DSCLOG_iblog_conf		INFOLOGG(
#define NEWLINE_DSCLOG_iblog_conf	
#include "IDL_iblog_conf.dsc.LOG.c"

int LoadConfig( struct IblogEnv *p_env )
{
	char		*file_content = NULL ;
	int		file_len ;
	
	int		nret = 0 ;
	
	file_content = StrdupEntireFile( p_env->iblog_conf_pathfilename , & file_len ) ;
	if( file_content == NULL )
	{
		ERRORLOGG( "StrdupEntireFile[%s] failed" , p_env->iblog_conf_pathfilename );
		return -1;
	}
	
	nret = DSCDESERIALIZE_JSON_iblog_conf( "GB18030" , file_content , & file_len , & (p_env->iblog_conf) ) ;
	free( file_content );
	if( nret )
	{
		ERRORLOGG( "DSCDESERIALIZE_JSON_iblog_conf[%s] failed[%d]" , p_env->iblog_conf_pathfilename , nret );
		return -1;
	}
	
	DSCLOG_iblog_conf( & (p_env->iblog_conf) );
	
	return 0;
}

