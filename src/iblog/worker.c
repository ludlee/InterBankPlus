#include "iblog_in.h"

signed char		g_exit_flag = 0 ;

int worker( struct IblogEnv *p_env )
{
	struct epoll_event	event ;
	
	struct epoll_event	events[ MAX_EPOLL_EVENTS ] ;
	int			epoll_nfds ;
	int			i ;
	struct epoll_event	*p_event = NULL ;
	
	struct ListenSession	*p_listen_session = NULL ;
	struct PipeSession	*p_pipe_session = NULL ;
	struct AcceptedSession	*p_accepted_session = NULL ;
	
	int			nret = 0 ;
	
	p_env->epoll_fd = epoll_create( 1024 ) ;
	if( p_env->epoll_fd == -1 )
	{
		ERRORLOGG( "epoll_create failed , errno[%d]" , errno );
		return -1;
	}
	else
	{
		DEBUGLOGG( "epoll_create ok , fd[%d]" , p_env->epoll_fd );
	}
	
	memset( & event , 0x00 , sizeof(struct epoll_event) );
	event.events = EPOLLIN | EPOLLERR ;
	event.data.ptr = & (p_env->listen_session) ;
	nret = epoll_ctl( p_env->epoll_fd , EPOLL_CTL_ADD , p_env->listen_session.netaddr.sock , & event ) ;
	if( nret == -1 )
	{
		ERRORLOGG( "epoll_ctl[%d] add listen_session failed , errno[%d]" , p_env->epoll_fd , errno );
		close( p_env->epoll_fd );
		return -1;
	}
	else
	{
		INFOLOGG( "epoll_ctl[%d] add listen_session[%d]" , p_env->epoll_fd , p_env->listen_session.netaddr.sock );
	}
	
	memset( & event , 0x00 , sizeof(struct epoll_event) );
	event.events = EPOLLIN | EPOLLERR ;
	event.data.ptr = & (p_env->pipe_session) ;
	nret = epoll_ctl( p_env->epoll_fd , EPOLL_CTL_ADD , p_env->pipe_session.pipe_fds[0] , & event ) ;
	if( nret == -1 )
	{
		ERRORLOGG( "epoll_ctl[%d] add pipe_session[%d] failed , errno[%d]" , p_env->epoll_fd , p_env->pipe_session.pipe_fds[0] , errno );
		close( p_env->epoll_fd );
		return -1;
	}
	else
	{
		INFOLOGG( "epoll_ctl[%d] add pipe_session[%d]" , p_env->epoll_fd , p_env->pipe_session.pipe_fds[0] );
	}
	
	while( ! g_exit_flag )
	{
		INFOLOGG( "epoll_wait[%d] ..." , p_env->epoll_fd );
		memset( events , 0x00 , sizeof(events) );
		epoll_nfds = epoll_wait( p_env->epoll_fd , events , MAX_EPOLL_EVENTS , 1000 ) ;
		if( epoll_nfds == -1 )
		{
			if( errno == EINTR )
			{
				INFOLOGG( "epoll_wait[%d] interrupted" , p_env->epoll_fd );
			}
			else
			{
				ERRORLOGG( "epoll_wait[%d] failed , errno[%d]" , p_env->epoll_fd , ERRNO );
			}
			
			return -1;
		}
		else
		{
			INFOLOGG( "epoll_wait[%d] return[%d]events" , p_env->epoll_fd , epoll_nfds );
		}
		
		for( i = 0 , p_event = events ; i < epoll_nfds ; i++ , p_event++ )
		{
			if( p_event->data.ptr == & (p_env->listen_session) )
			{
				p_listen_session = (struct ListenSession *)(p_event->data.ptr) ;
				
				if( p_event->events & EPOLLIN )
				{
					nret = OnAcceptingSocket( p_env , p_listen_session ) ;
					if( nret < 0 )
					{
						FATALLOGG( "OnAcceptingSocket failed[%d]" , nret );
						return -1;
					}
					else if( nret > 0 )
					{
						INFOLOGG( "OnAcceptingSocket return[%d]" , nret );
					}
					else
					{
						DEBUGLOGG( "OnAcceptingSocket ok" );
					}
				}
				else if( ( p_event->events & EPOLLERR ) || ( p_event->events & EPOLLHUP ) )
				{
					FATALLOGG( "listen session err or hup event[0x%X]" , p_event->events );
					return -1;
				}
				else
				{
					FATALLOGG( "Unknow listen session event[0x%X]" , p_event->events );
					return -1;
				}
			}
			else if( p_event->data.ptr == & (p_env->pipe_session) )
			{
				p_pipe_session = (struct PipeSession *)(p_event->data.ptr) ;
				
				if( ( p_event->events & EPOLLIN ) || ( p_event->events & EPOLLERR ) || ( p_event->events & EPOLLHUP ) )
				{
					epoll_ctl( p_env->epoll_fd , EPOLL_CTL_DEL , p_env->pipe_session.pipe_fds[0] , NULL );
					close( p_env->pipe_session.pipe_fds[0] );
					
					g_exit_flag = 1 ;
				}
				else
				{
					FATALLOGG( "Unknow pipe session event[0x%X]" , p_event->events );
					return -1;
				}
			}
			else
			{
				p_accepted_session = (struct AcceptedSession *)(p_event->data.ptr) ;
				
				if( p_event->events & EPOLLIN )
				{
					nret = OnReceivingSocket( p_env , p_accepted_session ) ;
					if( nret < 0 )
					{
						FATALLOGG( "OnReceivingSocket failed[%d]" , nret );
						return -1;
					}
					else if( nret > 0 )
					{
						INFOLOGG( "OnReceivingSocket return[%d]" , nret );
						OnClosingSocket( p_env , p_accepted_session );
					}
					else
					{
						DEBUGLOGG( "OnReceivingSocket ok" );
					}
				}
				else if( p_event->events & EPOLLOUT )
				{
					FATALLOGG( "unexpect accepted session event[0x%X]" , p_event->events );
					OnClosingSocket( p_env , p_accepted_session );
				}
				else if( ( p_event->events & EPOLLERR ) || ( p_event->events & EPOLLHUP ) )
				{
					FATALLOGG( "accepted session err or hup event[0x%X]" , p_event->events );
					OnClosingSocket( p_env , p_accepted_session );
				}
				else
				{
					FATALLOGG( "Unknow accepted session event[0x%X]" , p_event->events );
					return -1;
				}
			}
		}
	}
	
	INFOLOGG( "close listen[%d]" , p_env->listen_session.netaddr.sock );
	close( p_env->listen_session.netaddr.sock );
	
	INFOLOGG( "close epoll_fd[%d]" , p_env->epoll_fd );
	close( p_env->epoll_fd );
	
	return 0;
}

