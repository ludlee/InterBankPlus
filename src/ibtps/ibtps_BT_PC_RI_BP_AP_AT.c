#include "ibtps_BT_PC_RI_BP_AP_AT.h"

int _IBTPSExecuteSchedule_BT_PC_RI_BP_AP_AT( struct TransProcessEnv *p_tpe , struct TransProcessSchedule_BT_PC_RI_BP_AP_AT *p_tps )
{
	int		nret = 0 ;
	
	/* 交易前处理 阶段 */
	dbBeginWork();
	nret = IBTMExecuteFuncArray( p_tpe , p_tps->before_trans_desc , p_tps->before_trans_func_array ) ;
	IBTMSetResponseCode( p_tpe , nret );
	if( nret < 0 )
	{
		dbRollback();
		return nret;
	}
	else if( nret > 0 )
	{
		dbCommit();
		goto _GOTO_AT;
	}
	else
	{
		dbCommit();
	}
	
	/* 公共检查 阶段 */
	dbBeginWork();
	nret = IBTMExecuteFuncArray( p_tpe , p_tps->public_check_desc , p_tps->public_check_func_array ) ;
	IBTMSetResponseCode( p_tpe , nret );
	if( nret < 0 )
	{
		dbRollback();
		return nret;
	}
	else if( nret > 0 )
	{
		dbCommit();
		goto _GOTO_AT;
	}
	else
	{
		dbCommit();
	}
	
	/* 远程调用 阶段 */
	dbBeginWork();
	nret = IBTMExecuteFuncArray( p_tpe , p_tps->public_check_desc , p_tps->remote_invoke_func_array ) ;
	IBTMSetResponseCode( p_tpe , nret );
	if( nret < 0 )
	{
		dbRollback();
		return nret;
	}
	else if( nret > 0 )
	{
		dbCommit();
		goto _GOTO_AT;
	}
	else
	{
		dbCommit();
	}
	
	/* 业务处理 阶段 */
	dbBeginWork();
	nret = IBTMExecuteFuncArray( p_tpe , p_tps->busi_process_desc , p_tps->busi_process_func_array ) ;
	IBTMSetResponseCode( p_tpe , nret );
	if( nret < 0 )
	{
		dbRollback();
		return nret;
	}
	else if( nret > 0 )
	{
		dbRollback();
		goto _GOTO_AT;
	}
	
	/* 账务处理 阶段 */
	nret = IBTMExecuteFuncArray( p_tpe , p_tps->account_process_desc , p_tps->account_process_func_array ) ;
	IBTMSetResponseCode( p_tpe , nret );
	if( nret < 0 )
	{
		dbRollback();
		return nret;
	}
	else if( nret > 0 )
	{
		dbRollback();
		goto _GOTO_AT;
	}
	
	dbCommit();
	
_GOTO_AT :
	
	/* 交易后处理 阶段 */
	dbBeginWork();
	nret = IBTMExecuteFuncArray( p_tpe , p_tps->after_trans_desc , p_tps->after_trans_func_array ) ;
	IBTMSetResponseCode( p_tpe , nret );
	if( nret < 0 )
	{
		dbRollback();
		return nret;
	}
	else if( nret > 0 )
	{
		dbCommit();
	}
	else
	{
		dbCommit();
	}
	
	return 0;
}

int IBTPSExecuteSchedule_BT_PC_RI_BP_AP_AT( struct TransProcessEnv *p_tpe , void *pv_tps )
{
	return _IBTPSExecuteSchedule_BT_PC_RI_BP_AP_AT( p_tpe , (struct TransProcessSchedule_BT_PC_RI_BP_AP_AT*)pv_tps );
}

