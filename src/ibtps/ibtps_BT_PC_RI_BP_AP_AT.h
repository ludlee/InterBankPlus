#include "ibtm_api.h"

struct TransProcessSchedule_BT_PC_RI_BP_AP_AT
{
	/* 交易前处理 */
	char *before_trans_desc ;	struct TransFuncArray *before_trans_func_array ;
	/* 公共检查 */
	char *public_check_desc ;	struct TransFuncArray *public_check_func_array ;
	/* 联动交易 */
	char *remote_invoke_desc ;	struct TransFuncArray *remote_invoke_func_array ;
	/* 业务处理 */
	char *busi_process_desc ;	struct TransFuncArray *busi_process_func_array ;
	/* 账务处理 */
	char *account_process_desc ;	struct TransFuncArray *account_process_func_array ;
	/* 交易后处理 */
	char *after_trans_desc ;	struct TransFuncArray *after_trans_func_array ;
} ;

int IBTPSExecuteSchedule_BT_PC_RI_BP_AP_AT( struct TransProcessEnv *p_tpe , void *pv_tps );

