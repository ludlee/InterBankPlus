#include "ibms_in.h"

int InitEnvironment( struct IbmsEnv *p_env )
{
	SSL_load_error_strings();
	
	INIT_LIST_HEAD( & (p_env->accepted_session_list.this_node) );
	
	INIT_LIST_HEAD( & (p_env->notice_nodes_changed_list.this_node) );
	
	return 0;
}

void CleanEnvironment( struct IbmsEnv *p_env )
{
	struct list_head		*node = NULL , *next = NULL ;
	struct NoticeNodeChanged	*p_notice_node_changed ;
	struct AcceptedSession		*p_accepted_session = NULL ;
	
	DestroyIbmsNodeTree( p_env );
	DestroyIbmsAppTree( p_env );
	DestroyIbmsProjectTree( p_env );
	
	list_for_each_safe( node , next , & (p_env->accepted_session_list.this_node) )
	{
		p_accepted_session = list_entry( node , struct AcceptedSession , this_node ) ;
		OnClosingSocket( p_env , p_accepted_session );
	}
	
	list_for_each_safe( node , next , & (p_env->notice_nodes_changed_list.this_node) )
	{
		list_del( node );
		p_notice_node_changed = list_entry( node , struct NoticeNodeChanged , this_node ) ;
		free( p_notice_node_changed );
	}
	
	return;
}

