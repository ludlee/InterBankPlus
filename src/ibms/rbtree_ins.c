#include "ibms_in.h"

#include "rbtree_tpl.h"

static funcFreeRbTreeNodeEntry FreeIbmsNode ;
void FreeIbmsNode( void *pv )
{
	struct IbmsNode		*p_node = (struct IbmsNode *) pv ;
	
	if( p_node )
	{
		DestroyIbmsNodeHostTree( p_node );
		free( p_node );
	}
	
	return;
}

LINK_RBTREENODE_STRING( LinkIbmsNodeTreeNode , struct IbmsEnv , nodes_rbtree , struct IbmsNode , node_rbnode , node )
QUERY_RBTREENODE_STRING( QueryIbmsNodeTreeNode , struct IbmsEnv , nodes_rbtree , struct IbmsNode , node_rbnode , node )
UNLINK_RBTREENODE( UnlinkIbmsNodeTreeNode , struct IbmsEnv , nodes_rbtree , struct IbmsNode , node_rbnode )
TRAVEL_RBTREENODE( TravelIbmsNodeTreeNode , struct IbmsEnv , nodes_rbtree , struct IbmsNode , node_rbnode )
DESTROY_RBTREE( DestroyIbmsNodeTree , struct IbmsEnv , nodes_rbtree , struct IbmsNode , node_rbnode , FreeIbmsNode )

static funcCompareRbTreeNodeEntry CompareIbmsNodeHost ;
int CompareIbmsNodeHost( void *pv1 , void *pv2 )
{
	struct IbmsNodeHost	*pst1 = (struct IbmsNodeHost *) pv1 ;
	struct IbmsNodeHost	*pst2 = (struct IbmsNodeHost *) pv2 ;
	
	if( STRCMP( pst1->ip , > , pst2->ip ) )
	{
		return 1;
	}
	else if( STRCMP( pst1->ip , < , pst2->ip ) )
	{
		return -1;
	}
	else
	{
		if( pst1->port > pst2->port )
		{
			return 1;
		}
		else if( pst1->port < pst2->port )
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}

LINK_RBTREENODE( LinkIbmsNodeHostTreeNode , struct IbmsNode , node_hosts_rbtree , struct IbmsNodeHost , node_host_rbnode , CompareIbmsNodeHost )
QUERY_RBTREENODE( QueryIbmsNodeHostTreeNode , struct IbmsNode , node_hosts_rbtree , struct IbmsNodeHost , node_host_rbnode , CompareIbmsNodeHost )
UNLINK_RBTREENODE( UnlinkIbmsNodeHostTreeNode , struct IbmsNode , node_hosts_rbtree , struct IbmsNodeHost , node_host_rbnode )
TRAVEL_RBTREENODE( TravelIbmsNodeHostTreeNode , struct IbmsNode , node_hosts_rbtree , struct IbmsNodeHost , node_host_rbnode )
DESTROY_RBTREE( DestroyIbmsNodeHostTree , struct IbmsNode , node_hosts_rbtree , struct IbmsNodeHost , node_host_rbnode , FREE_RBTREENODEENTRY_DIRECTLY )

LINK_RBTREENODE_STRING( LinkIbmsAppTreeNode , struct IbmsEnv , apps_rbtree , struct IbmsApp , app_rbnode , app )
QUERY_RBTREENODE_STRING( QueryIbmsAppTreeNode , struct IbmsEnv , apps_rbtree , struct IbmsApp , app_rbnode , app )
UNLINK_RBTREENODE( UnlinkIbmsAppTreeNode , struct IbmsEnv , apps_rbtree , struct IbmsApp , app_rbnode )
TRAVEL_RBTREENODE( TravelIbmsAppTreeNode , struct IbmsEnv , apps_rbtree , struct IbmsApp , app_rbnode )
DESTROY_RBTREE( DestroyIbmsAppTree , struct IbmsEnv , apps_rbtree , struct IbmsApp , app_rbnode , FREE_RBTREENODEENTRY_DIRECTLY )

static funcFreeRbTreeNodeEntry FreeIbmsProject ;
void FreeIbmsProject( void *pv )
{
	struct IbmsProject	*p_project = (struct IbmsProject *) pv ;
	
	if( p_project )
	{
		free( p_project->nodes );
		free( p_project->apps );
		free( p_project );
	}
	
	return;
}

LINK_RBTREENODE_STRING( LinkIbmsProjectTreeNode , struct IbmsEnv , projects_rbtree , struct IbmsProject , project_rbnode , project )
QUERY_RBTREENODE_STRING( QueryIbmsProjectTreeNode , struct IbmsEnv , projects_rbtree , struct IbmsProject , project_rbnode , project )
UNLINK_RBTREENODE( UnlinkIbmsProjectTreeNode , struct IbmsEnv , projects_rbtree , struct IbmsProject , project_rbnode )
TRAVEL_RBTREENODE( TravelIbmsProjectTreeNode , struct IbmsEnv , projects_rbtree , struct IbmsProject , project_rbnode )
DESTROY_RBTREE( DestroyIbmsProjectTree , struct IbmsEnv , projects_rbtree , struct IbmsProject , project_rbnode , FreeIbmsProject )

