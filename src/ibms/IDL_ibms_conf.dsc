STRUCT			ibms_conf
{
	STRUCT		ibms
	{
		STRUCT	server
		{
			STRING 20	ip
			INT 4		port
		}
		
		STRUCT	config
		{
			STRING	256	nodes_conf
			STRING	256	apps_conf
			STRING	256	projects_conf
		}
		
		STRUCT	log
		{
			STRING 256	iblog_server
			STRING 256	event_output
			STRING 256	main_output
			STRING 6	main_loglevel
			STRING 256	monitor_output
			STRING 6	monitor_loglevel
			STRING 256	worker_output
			STRING 6	worker_loglevel
		}
	}
}

