#include "ibms_in.h"

static int HttpClientV( struct IbmsEnv *p_env , char *format , va_list valist )
{
	struct NetAddress	netaddr ;
	struct HttpEnv		*http = NULL ;
	
	char			*statuscode = NULL ;
	int			statuscode_len ;
	char			*reasonphrase = NULL ;
	int			reasonphrase_len ;
	char			*body = NULL ;
	int			body_len ;
	
	int			nret = 0 ;
	
	memset( & netaddr , 0x00 , sizeof(struct NetAddress) );
	strcpy( netaddr.ip , p_env->ibms_conf.ibms.server.ip );
	netaddr.port = p_env->ibms_conf.ibms.server.port ;
	
	http = CreateHttpEnv() ;
	if( http == NULL )
	{
		printf( "CreateHttpEnv failed , errno[%d]\n" , errno );
		return -1;
	}
	
	nret = HttpRequestV( & netaddr , http , 60 , format , valist ) ;
	if( nret )
	{
		printf( "HttpRequest failed[%d] , errno[%d]\n" , nret , errno );
		DestroyHttpEnv( http );
		return nret;
	}
	
	statuscode = GetHttpHeaderPtr_STATUSCODE( http , & statuscode_len ) ;
	reasonphrase = GetHttpHeaderPtr_REASONPHRASE( http , & reasonphrase_len ) ;
	if( STRNCMP( statuscode , != , HTTP_OK_S , 2 ) )
	{
		printf( "%.*s %.*s\n" , statuscode_len , statuscode , reasonphrase_len , reasonphrase );
	}
	else
	{
		body = GetHttpBodyPtr( http , & body_len );
		if( body )
		{
			if( body[body_len-1] != '\n' )
				printf( "%.*s\n" , body_len , body );
			else
				printf( "%.*s" , body_len , body );
		}
	}
	
	DestroyHttpEnv( http );
	
	return nstoi(statuscode,statuscode_len);
}

static int HttpClient( struct IbmsEnv *p_env , char *format , ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , format );
	nret = HttpClientV( p_env , format , valist ) ;
	va_end( valist );
	
	return nret;
}

int ShowNodes( struct IbmsEnv *p_env )
{
	return HttpClient( p_env , "%s" , "GET " URI_SHOW_NODES " HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE );
}

int ShowApps( struct IbmsEnv *p_env )
{
	return HttpClient( p_env , "%s" , "GET " URI_SHOW_APPS " HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE );
}

int ShowProjects( struct IbmsEnv *p_env )
{
	return HttpClient( p_env , "%s" , "GET " URI_SHOW_PROJECTS " HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE );
}

int ShowNodesChanged( struct IbmsEnv *p_env )
{
	return HttpClient( p_env , "%s" , "GET " URI_SHOW_NODES_CHANGED " HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE );
}

int ShowSessions( struct IbmsEnv *p_env )
{
	return HttpClient( p_env , "%s" , "GET " URI_SHOW_SESSIONS " HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE );
}

int AddNode( struct IbmsEnv *p_env )
{
	ibms_node_reqmsg	ibms_node ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	strncpy( ibms_node.node , p_env->cmd_param.__node , sizeof(ibms_node.node)-1 );
	ibms_node.invalid = atoi(p_env->cmd_param.__invalid) ;
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_reqmsg( & ibms_node , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_ADD_NODE " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int SetNode( struct IbmsEnv *p_env )
{
	ibms_node_reqmsg	ibms_node ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	strncpy( ibms_node.node , p_env->cmd_param.__node , sizeof(ibms_node.node)-1 );
	if( p_env->cmd_param.__invalid )
		ibms_node.invalid = atoi(p_env->cmd_param.__invalid) ;
	else
		ibms_node.invalid = -1 ;
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_reqmsg( & ibms_node , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_SET_NODE " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int RemoveNode( struct IbmsEnv *p_env )
{
	ibms_node_reqmsg	ibms_node ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	strncpy( ibms_node.node , p_env->cmd_param.__node , sizeof(ibms_node.node)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_reqmsg( & ibms_node , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_REMOVE_NODE " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int AddNodeHost( struct IbmsEnv *p_env )
{
	ibms_node_host_reqmsg	ibms_node_host ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_node_host , 0x00 , sizeof(ibms_node_host_reqmsg) );
	strncpy( ibms_node_host.node , p_env->cmd_param.__node , sizeof(ibms_node_host.node)-1 );
	strncpy( ibms_node_host.ip , p_env->cmd_param.__ip , sizeof(ibms_node_host.ip)-1 );
	ibms_node_host.port = atoi(p_env->cmd_param.__port) ;
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_host_reqmsg( & ibms_node_host , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_ADD_NODE_HOST " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int RemoveNodeHost( struct IbmsEnv *p_env )
{
	ibms_node_host_reqmsg	ibms_node_host ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_node_host , 0x00 , sizeof(ibms_node_host_reqmsg) );
	strncpy( ibms_node_host.node , p_env->cmd_param.__node , sizeof(ibms_node_host.node)-1 );
	strncpy( ibms_node_host.ip , p_env->cmd_param.__ip , sizeof(ibms_node_host.ip)-1 );
	ibms_node_host.port = atoi(p_env->cmd_param.__port) ;
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_host_reqmsg( & ibms_node_host , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_REMOVE_NODE_HOST " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int AddApp( struct IbmsEnv *p_env )
{
	ibms_app_reqmsg		ibms_app ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	strncpy( ibms_app.app , p_env->cmd_param.__app , sizeof(ibms_app.app)-1 );
	strncpy( ibms_app.desc , p_env->cmd_param.__desc , sizeof(ibms_app.desc)-1 );
	strncpy( ibms_app.bin , p_env->cmd_param.__bin , sizeof(ibms_app.bin)-1 );
	ibms_app.timeout = atoi(p_env->cmd_param.__timeout) ;
	ibms_app.timeout2 = atoi(p_env->cmd_param.__timeout2) ;
	ibms_app.invalid = atoi(p_env->cmd_param.__invalid) ;
	nret = DSCSERIALIZE_JSON_DUP_ibms_app_reqmsg( & ibms_app , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_ADD_APP " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int SetApp( struct IbmsEnv *p_env )
{
	ibms_app_reqmsg		ibms_app ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	strncpy( ibms_app.app , p_env->cmd_param.__app , sizeof(ibms_app.app)-1 );
	strncpy( ibms_app.desc , p_env->cmd_param.__desc , sizeof(ibms_app.desc)-1 );
	strncpy( ibms_app.bin , p_env->cmd_param.__bin , sizeof(ibms_app.bin)-1 );
	ibms_app.timeout = atoi(p_env->cmd_param.__timeout) ;
	ibms_app.timeout2 = atoi(p_env->cmd_param.__timeout2) ;
	ibms_app.invalid = atoi(p_env->cmd_param.__invalid) ;
	nret = DSCSERIALIZE_JSON_DUP_ibms_app_reqmsg( & ibms_app , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_SET_APP " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int RemoveApp( struct IbmsEnv *p_env )
{
	ibms_app_reqmsg		ibms_app ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	strncpy( ibms_app.app , p_env->cmd_param.__app , sizeof(ibms_app.app)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_app_reqmsg( & ibms_app , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_REMOVE_APP " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int AddProject( struct IbmsEnv *p_env )
{
	ibms_project_reqmsg	ibms_project ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_project , 0x00 , sizeof(ibms_project_reqmsg) );
	strncpy( ibms_project.project , p_env->cmd_param.__project , sizeof(ibms_project.project)-1 );
	strncpy( ibms_project.nodes , p_env->cmd_param.__nodes , sizeof(ibms_project.nodes)-1 );
	strncpy( ibms_project.apps , p_env->cmd_param.__apps , sizeof(ibms_project.apps)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_project_reqmsg( & ibms_project , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_ADD_PROJECT " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int SetProject( struct IbmsEnv *p_env )
{
	ibms_project_reqmsg	ibms_project ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_project , 0x00 , sizeof(ibms_project_reqmsg) );
	strncpy( ibms_project.project , p_env->cmd_param.__project , sizeof(ibms_project.project)-1 );
	strncpy( ibms_project.nodes , p_env->cmd_param.__nodes , sizeof(ibms_project.nodes)-1 );
	strncpy( ibms_project.apps , p_env->cmd_param.__apps , sizeof(ibms_project.apps)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_project_reqmsg( & ibms_project , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_SET_PROJECT " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int RemoveProject( struct IbmsEnv *p_env )
{
	ibms_project_reqmsg	ibms_project ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_project , 0x00 , sizeof(ibms_project_reqmsg) );
	strncpy( ibms_project.project , p_env->cmd_param.__project , sizeof(ibms_project.project)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_project_reqmsg( & ibms_project , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " URI_REMOVE_PROJECT " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg ) ;
	free( req_msg );
	return nret;
}

int CommitChanged( struct IbmsEnv *p_env )
{
	return HttpClient( p_env , "%s" , "GET " URI_COMMIT_CHANGED " HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE );
}

int DownloadConfig( struct IbmsEnv *p_env )
{
	ibms_node_reqmsg	ibms_node ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	
	int			nret = 0 ;
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	strncpy( ibms_node.node , p_env->cmd_param.__node , sizeof(ibms_node.node)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_reqmsg( & ibms_node , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
		return nret;
	
	nret = HttpClient( p_env , "POST " IBP_URI_DOWNLOAD_CONFIG " HTTP/1.0" HTTP_RETURN_NEWLINE
					"Content-length: %d" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					"%s"
					, req_msg_len
					, req_msg );
	free( req_msg );
	return nret;
}

