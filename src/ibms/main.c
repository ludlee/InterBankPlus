#include "ibms_in.h"

char		*g_ibp_ibms_kernel_version = IBP_VERSION ;

static void version()
{
	printf( "ibms v%s build %s %s ( ibidl v%s, ibutil v%s )\n" , g_ibp_ibms_kernel_version , __DATE__ , __TIME__ , g_ibp_idl_kernel_version , g_ibp_util_kernel_version );
	printf( "Copyright by calvin 2017\n" );
	printf( "Email : calvinwilliams@163.com\n" );
	return;
}

static void usage()
{
	printf( "USAGE : ibms -f ibms.conf -a start\n" );
	printf( "                          -s nodes\n" );
	printf( "                          -a add_node --node (node) --invalid (invalid)\n" );
	printf( "                          -a set_node --node (node) [ --invalid (invalid) ]\n" );
	printf( "                          -a remove_node --node (node)\n" );
	printf( "                          -a add_node_host --node (node) --ip (ip) --port (port)\n" );
	printf( "                          -a remove_node_host --node (node) --ip (ip) --port (port)\n" );
	printf( "                          -s apps\n" );
	printf( "                          -a add_app --app (app) --desc (desc) --bin (bin) --timeout (timeout) --timeout2 (timeout2) --invalid (invalid)\n" );
	printf( "                          -a set_app --app (app) --desc (desc) --bin (bin) --timeout (timeout) --timeout2 (timeout2) --invalid (invalid)\n" );
	printf( "                          -a remove_app --app (app)\n" );
	printf( "                          -s projects\n" );
	printf( "                          -a add_project --project (project) --nodes (nodes) --apps (apps)\n" );
	printf( "                          -a set_project --project (project) --nodes (nodes) --apps (apps)\n" );
	printf( "                          -a remove_project --project (project)\n" );
	printf( "                          -a commit_changed\n" );
	printf( "                          -a download_config --node (node)\n" );
	printf( "kill -USR1 (pid) : reload log output\n" );
	printf( "kill -USR2 (pid) : reload nodes,apps,projects config\n" );
	
	return;
}

int main( int argc , char *argv[] )
{
	struct IbmsEnv		*p_env = NULL ;
	int			i ;
	
	int			nret = 0 ;
	
	if( argc > 1 )
	{
		p_env = (struct IbmsEnv *)malloc( sizeof(struct IbmsEnv) ) ;
		if( p_env == NULL )
		{
			printf( "malloc failed , errno[%d]\n" , errno );
			return 1;
		}
		memset( p_env , 0x00 , sizeof(struct IbmsEnv) );
		
		for( i = 1 ; i < argc ; i++ )
		{
			if( strcmp( argv[i] , "-v" ) == 0 )
			{
				version();
				exit(0);
			}
			else if( strcmp( argv[i] , "-f" ) == 0 && i + 1 < argc )
			{
				p_env->ibms_conf_filename = argv[++i] ;
			}
			else if( strcmp( argv[i] , "-a" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param._action = argv[++i] ;
			}
			else if( strcmp( argv[i] , "-s" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param._show = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--node" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__node = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--invalid" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__invalid = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--ip" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__ip = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--port" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__port = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--app" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__app = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--desc" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__desc = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--bin" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__bin = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--timeout" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__timeout = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--timeout2" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__timeout2 = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--project" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__project = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--nodes" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__nodes = argv[++i] ;
			}
			else if( strcmp( argv[i] , "--apps" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param.__apps = argv[++i] ;
			}
			else
			{
				printf( "Invalid opt[%s]\r\n" , argv[i] );
				usage();
				exit(7);
			}
		}
		
		if( p_env->ibms_conf_filename == NULL )
			p_env->ibms_conf_filename = "ibms.conf" ;
		
		memset( p_env->ibms_conf_pathfilename , 0x00 , sizeof(p_env->ibms_conf_pathfilename) );
		snprintf( p_env->ibms_conf_pathfilename , sizeof(p_env->ibms_conf_pathfilename)-1 , "%s/etc/%s" , getenv("HOME") , p_env->ibms_conf_filename );
		
		nret = LoadConfig( p_env ) ;
		if( nret )
		{
			printf( "LoadConfig[%s] failed[%d]\n" , p_env->ibms_conf_pathfilename , nret );
			return 1;
		}
		
		IBPInitLogEnv( p_env->ibms_conf.ibms.log.iblog_server , "ibms.main" , p_env->ibms_conf.ibms.log.event_output , IBPConvertLogLevel(p_env->ibms_conf.ibms.log.main_loglevel) , "%s" , p_env->ibms_conf.ibms.log.main_output );
		
		if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "start" ) )
		{
			nret = InitEnvironment( p_env ) ;
			if( nret )
			{
				printf( "InitEnvironment failed[%d]\n" , nret );
				IBPCleanLogEnv();
				return 1;
			}
			
			nret = BindDaemonServer( & _monitor , p_env ) ;
			if( nret < 0 )
			{
				printf( "BindDaemonServer failed[%d]\n" , nret );
				ERRORLOGSG( "--- ibms end --- " );
			}
			
			CleanEnvironment( p_env );
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "download_config" ) && p_env->cmd_param.__node )
		{
			nret = DownloadConfig( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "nodes" ) )
		{
			nret = ShowNodes( p_env ) ;
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "apps" ) )
		{
			nret = ShowApps( p_env ) ;
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "projects" ) )
		{
			nret = ShowProjects( p_env ) ;
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "nodes_changed" ) )
		{
			nret = ShowNodesChanged( p_env ) ;
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "sessions" ) )
		{
			nret = ShowSessions( p_env ) ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "add_node" ) && p_env->cmd_param.__node && p_env->cmd_param.__invalid )
		{
			nret = AddNode( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "set_node" ) && p_env->cmd_param.__node )
		{
			nret = SetNode( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "remove_node" ) && p_env->cmd_param.__node )
		{
			nret = RemoveNode( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "add_node_host" ) && p_env->cmd_param.__node && p_env->cmd_param.__ip && p_env->cmd_param.__port )
		{
			nret = AddNodeHost( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "remove_node_host" ) && p_env->cmd_param.__node && p_env->cmd_param.__ip && p_env->cmd_param.__port )
		{
			nret = RemoveNodeHost( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "add_app" ) && p_env->cmd_param.__app && p_env->cmd_param.__desc && p_env->cmd_param.__bin && p_env->cmd_param.__timeout && p_env->cmd_param.__timeout2 && p_env->cmd_param.__invalid )
		{
			nret = AddApp( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "set_app" ) && p_env->cmd_param.__app && p_env->cmd_param.__desc && p_env->cmd_param.__bin && p_env->cmd_param.__timeout && p_env->cmd_param.__timeout2 && p_env->cmd_param.__invalid )
		{
			nret = SetApp( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "remove_app" ) && p_env->cmd_param.__app )
		{
			nret = RemoveApp( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "add_project" ) && p_env->cmd_param.__project && p_env->cmd_param.__nodes && p_env->cmd_param.__apps )
		{
			nret = AddProject( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "set_project" ) && p_env->cmd_param.__project && p_env->cmd_param.__nodes && p_env->cmd_param.__apps )
		{
			nret = SetProject( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "remove_project" ) && p_env->cmd_param.__project )
		{
			nret = RemoveProject( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "commit_changed" ) )
		{
			nret = CommitChanged( p_env ) ;
			if( nret == HTTP_OK )
				nret = 0 ;
		}
		else
		{
			usage();
			exit(7);
		}
		
		free( p_env );
	}
	else
	{
		usage();
		return 7;
	}
	
	IBPCleanLogEnv();
	
	return abs(nret);
}

