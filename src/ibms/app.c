#include "ibms_in.h"

int OnProcess( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	char			*uri = NULL ;
	int			uri_len ;
	
	int			nret = 0 ;
	
	uri = GetHttpHeaderPtr_URI( p_accepted_session->http , & uri_len ) ;
	INFOLOGSG( "uri[%.*s]" , uri_len , uri );
	
	/*
	if( uri_len == sizeof(IBP_URI_ALIVE_REPORT)-1 && MEMCMP( uri , == , IBP_URI_ALIVE_REPORT , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBAS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 1 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	else
	*/
	if( uri_len == sizeof(URI_SHOW_NODES)-1 && MEMCMP( uri , == , URI_SHOW_NODES , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessShowNodes( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessShowNodes failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessShowNodes ok" );
		}
	}
	else if( uri_len == sizeof(URI_SHOW_APPS)-1 && MEMCMP( uri , == , URI_SHOW_APPS , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessShowApps( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessShowApps failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessShowApps ok" );
		}
	}
	else if( uri_len == sizeof(URI_SHOW_PROJECTS)-1 && MEMCMP( uri , == , URI_SHOW_PROJECTS , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessShowProjects( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessShowProjects failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessShowProjects ok" );
		}
	}
	else if( uri_len == sizeof(URI_SHOW_NODES_CHANGED)-1 && MEMCMP( uri , == , URI_SHOW_NODES_CHANGED , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessShowNodesChanged( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessShowNodesChanged failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessShowNodesChanged ok" );
		}
	}
	else if( uri_len == sizeof(URI_SHOW_SESSIONS)-1 && MEMCMP( uri , == , URI_SHOW_SESSIONS , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessShowSessions( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessShowSessions failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessShowSessions ok" );
		}
	}
	else if( uri_len == sizeof(URI_ADD_NODE)-1 && MEMCMP( uri , == , URI_ADD_NODE , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessAddNode( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessAddNode failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessAddNode ok" );
		}
	}
	else if( uri_len == sizeof(URI_SET_NODE)-1 && MEMCMP( uri , == , URI_SET_NODE , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessSetNode( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessSetNode failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessSetNode ok" );
		}
	}
	else if( uri_len == sizeof(URI_REMOVE_NODE)-1 && MEMCMP( uri , == , URI_REMOVE_NODE , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessRemoveNode( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessRemoveNode failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessRemoveNode ok" );
		}
	}
	else if( uri_len == sizeof(URI_ADD_NODE_HOST)-1 && MEMCMP( uri , == , URI_ADD_NODE_HOST , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessAddNodeHost( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessAddNodeHost failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessAddNodeHost ok" );
		}
	}
	else if( uri_len == sizeof(URI_REMOVE_NODE_HOST)-1 && MEMCMP( uri , == , URI_REMOVE_NODE_HOST , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessRemoveNodeHost( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessRemoveNodeHost failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessRemoveNodeHost ok" );
		}
	}
	else if( uri_len == sizeof(URI_ADD_APP)-1 && MEMCMP( uri , == , URI_ADD_APP , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessAddApp( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessAddApp failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessAddApp ok" );
		}
	}
	else if( uri_len == sizeof(URI_SET_APP)-1 && MEMCMP( uri , == , URI_SET_APP , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessSetApp( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessSetApp failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessSetApp ok" );
		}
	}
	else if( uri_len == sizeof(URI_REMOVE_APP)-1 && MEMCMP( uri , == , URI_REMOVE_APP , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessRemoveApp( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessRemoveApp failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessRemoveApp ok" );
		}
	}
	else if( uri_len == sizeof(URI_ADD_PROJECT)-1 && MEMCMP( uri , == , URI_ADD_PROJECT , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessAddProject( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessAddProject failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessAddProject ok" );
		}
	}
	else if( uri_len == sizeof(URI_SET_PROJECT)-1 && MEMCMP( uri , == , URI_SET_PROJECT , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessSetProject( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessSetProject failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessSetProject ok" );
		}
	}
	else if( uri_len == sizeof(URI_REMOVE_PROJECT)-1 && MEMCMP( uri , == , URI_REMOVE_PROJECT , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMS )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessRemoveProject( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessRemoveProject failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessRemoveProject ok" );
		}
	}
	else if( uri_len == sizeof(URI_COMMIT_CHANGED)-1 && MEMCMP( uri , == , URI_COMMIT_CHANGED , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMA )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessCommitChanged( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessCommitChanged failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessCommitChanged ok" );
		}
	}
	else if( uri_len == sizeof(IBP_URI_FETCH_CONFIG)-1 && MEMCMP( uri , == , IBP_URI_FETCH_CONFIG , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMA )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBMA ;
		p_accepted_session->do_what = ACCEPTEDSESSION_DO_FETCH_CONFIG ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessFetchConfig( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessFetchConfig failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessFetchConfig ok , node[%s]" , p_accepted_session->node.node );
		}
	}
	else if( uri_len == sizeof(IBP_URI_FETCH_BIN)-1 && MEMCMP( uri , == , IBP_URI_FETCH_BIN , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMA )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessFetchBin( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessFetchBin failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessFetchBin ok , node[%s]" , p_accepted_session->node.node );
		}
	}
	else if( uri_len == sizeof(IBP_URI_DOWNLOAD_CONFIG)-1 && MEMCMP( uri , == , IBP_URI_DOWNLOAD_CONFIG , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY && p_accepted_session->role != ACCEPTEDSESSION_ROLE_IBMA )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessDownloadConfig( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessDownloadConfig failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessDownloadConfig ok" );
		}
	}
#if 0
	else if( uri_len == sizeof(IBP_URI_ALIVE_REPORT_FIRST)-1 && MEMCMP( uri , == , IBP_URI_ALIVE_REPORT_FIRST , uri_len ) )
	{
		if( p_accepted_session->role != ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY )
		{
			return HTTP_UNAUTHORIZED;
		}
		
		p_accepted_session->role = ACCEPTEDSESSION_ROLE_IBAS ;
		
		nret = FormatHttpResponseStartLine( HTTP_OK , p_accepted_session->http , 0 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = OnProcessAliveReportFirst( p_env , p_accepted_session ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessAliveReportFirst failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessAliveReportFirst ok" );
		}
	}
#endif
	else
	{
		return HTTP_NOT_FOUND;
	}
	
	return HTTP_OK;
}

#if 0
static int PrepareNodesInvalidMessage( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session , char *node )
{
	struct AcceptedSession	*p_changed_session = NULL ;
	struct epoll_event	event ;
	
	int			nret = 0 ;
	
	list_for_each_entry( p_changed_session , & (p_env->accepted_session_list.this_node) , struct AcceptedSession , this_node )
	{
		if( STRCMP( p_changed_session->node.node , == , node ) && p_changed_session->role == ACCEPTEDSESSION_ROLE_IBMA && p_changed_session->do_what == ACCEPTEDSESSION_DO_KEEP_SILENT )
		{
			ibp_alive_notice_reqmsg		ibp_alive_notice ;
			char				*reqmsg = NULL ;
			int				reqmsg_len ;
			struct HttpBuffer		*req_buf = NULL ;
			
			INFOLOGSG( "node[%s] p_changed_session[%p]" , node , p_changed_session );
			
			ResetHttpEnv( p_changed_session->http ) ;
			ResetHttpSecureEnv( p_changed_session->http_secure_env );
			
			memset( & ibp_alive_notice , 0x00 , sizeof(ibp_alive_notice_reqmsg) );
			strcpy( ibp_alive_notice.node , p_accepted_session->node.node );
			strcpy( ibp_alive_notice.ip , p_accepted_session->node_host.ip );
			ibp_alive_notice.port = p_accepted_session->node_host.port ;
			ibp_alive_notice.invalid = p_accepted_session->node_host.invalid ;
			nret = DSCSERIALIZE_JSON_DUP_ibp_alive_notice_reqmsg( & ibp_alive_notice , "GB18030" , & reqmsg , NULL , & reqmsg_len ) ;
			if( nret )
			{
				ERRORLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_alive_notice_reqmsg failed[%d] , errno[%d]" , nret , errno );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			
			req_buf = GetHttpRequestBuffer( p_changed_session->http ) ;
			nret = StrcpyfHttpBuffer( req_buf , "POST " IBP_URI_ALIVE_NOTICE " HTTP/1.0" HTTP_RETURN_NEWLINE
							"Connection: keep-alive" HTTP_RETURN_NEWLINE
							"Content-length : %d" HTTP_RETURN_NEWLINE
							HTTP_RETURN_NEWLINE
							"%s"
							, reqmsg_len 
							, reqmsg ) ;
			free( reqmsg );
			if( nret )
			{
				ERRORLOGSG( "StrcpyfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			else
			{
				DEBUGHEXLOGSG( GetHttpBufferBase(req_buf,NULL) , GetHttpBufferLength(req_buf) , "StrcpyfHttpBuffer ok" );
			}
			
			SetHttpTimeout( p_changed_session->http , 60 );
			
			p_changed_session->do_what = ACCEPTEDSESSION_DO_DOWNLOAD_CONFIG ;
			
			memset( & event , 0x00 , sizeof(struct epoll_event) );
			event.events = EPOLLOUT | EPOLLERR ;
			event.data.ptr = p_changed_session ;
			nret = epoll_ctl( p_env->epoll_fd , EPOLL_CTL_MOD , p_changed_session->netaddr.sock , & event ) ;
			if( nret == -1 )
			{
				ERRORLOGSG( "epoll_ctl[%d] modify[%d] failed , errno[%d]" , p_env->epoll_fd , p_changed_session->netaddr.sock , errno );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			else
			{
				DEBUGLOGSG( "epoll_ctl[%d] modify[%d] ok" , p_env->epoll_fd , p_changed_session->netaddr.sock );
			}
		}
	}
	
	return HTTP_OK;
}

int NoticeNodesInvalid( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct IbmsProject		*p_project = NULL ;
	char				*p = NULL ;
	
	int				nret = 0 ;
	
	p_project = NULL ;
	while( ( p_project = TravelIbmsProjectTreeNode( p_env , p_project ) ) )
	{
		if( strword( p_project->nodes , p_accepted_session->node.node ) )
		{
			p = gettok( p_project->nodes , " \t" ) ;
			while( p )
			{
				nret = PrepareNodesInvalidMessage( p_env , p_accepted_session , p ) ;
				if( nret != HTTP_OK )
					return nret;
				
				p = gettok( NULL , " \t" ) ;
			}
		}
	}
	
	return HTTP_OK;
}
#endif

static int PackFetchConfigMessage( struct IbmsEnv *p_env , char *node_changed , ibp_fetch_config_rspmsg *pst )
{
	struct IbmsProject		*p_project = NULL ;
	char				*p = NULL ;
	int				i ;
	
	int				nret = 0 ;
	
	p_project = NULL ;
	while( ( p_project = TravelIbmsProjectTreeNode( p_env , p_project ) ) )
	{
		if( strword( p_project->nodes , node_changed ) )
		{
			p = gettok( p_project->nodes , " \t" ) ;
			while( p )
			{
				for( i = 0 ; i < pst->_nodes_count ; i++ )
				{
					if( STRCMP( pst->nodes[i].node , == , p ) )
						break;
				}
				if( i >= pst->_nodes_count )
				{
					struct IbmsNode		node ;
					struct IbmsNode		*p_node = NULL ;
					struct IbmsNodeHost	*p_node_host = NULL ;
					
					DEBUGLOGSG( "add node[%s]" , p );
					
					memset( & node , 0x00 , sizeof(struct IbmsNode) );
					strcpy( node.node , p );
					p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
					if( p_node )
					{
						char		*path = NULL ;
						char		key_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
						char		*file_content = NULL ;
						int		file_len ;
						int		key_base64_buflen ;
						
						strcpy( pst->nodes[pst->_nodes_count].node , p_node->node );
						pst->nodes[pst->_nodes_count].invalid = p_node->invalid ;
						pst->nodes[pst->_nodes_count]._hosts_count = 0 ;
						
						path = getenv("IBAS_KEY_PATH") ;
						memset( key_pathfilename , 0x00 , sizeof(key_pathfilename) );
						if( path )
							snprintf( key_pathfilename , sizeof(key_pathfilename)-1 , "%s/%s.%s" , path , p_node->node , (STRCMP(node.node,==,node_changed)?"prikey":"pubkey") );
						else
							snprintf( key_pathfilename , sizeof(key_pathfilename)-1 , "%s/keys/%s.%s" , getenv("HOME") , p_node->node , (STRCMP(node.node,==,node_changed)?"prikey":"pubkey") );
						file_content = StrdupEntireFile( key_pathfilename , & file_len ) ;
						if( file_content == NULL )
						{
							ERRORLOGSG( "StrdupEntireFile[%s] failed , errno[%d]" , key_pathfilename , errno );
							return HTTP_INTERNAL_SERVER_ERROR;
						}
						key_base64_buflen = sizeof(pst->nodes[pst->_nodes_count].key_base64)-1 ;
						nret = Encoding_BASE64( file_content , file_len , pst->nodes[pst->_nodes_count].key_base64 , & key_base64_buflen ) ;
						if( nret )
						{
							ERRORLOGSG( "Encoding_BASE64 failed[%d]" , nret );
							return HTTP_INTERNAL_SERVER_ERROR;
						}
						free( file_content );
						
						p_node_host = NULL ;
						while( ( p_node_host = TravelIbmsNodeHostTreeNode( p_node , p_node_host ) ) )
						{
							strcpy( pst->nodes[pst->_nodes_count].hosts[pst->nodes[pst->_nodes_count]._hosts_count].ip , p_node_host->ip );
							pst->nodes[pst->_nodes_count].hosts[pst->nodes[pst->_nodes_count]._hosts_count].port = p_node_host->port ;
#if 0
							pst->nodes[pst->_nodes_count].hosts[pst->nodes[pst->_nodes_count]._hosts_count].invalid = p_node_host->invalid ;
#endif
							
							pst->nodes[pst->_nodes_count]._hosts_count++;
						}
						
						pst->_nodes_count++;
					}
				}
				
				p = gettok( NULL , " \t" ) ;
			}
			
			p = gettok( p_project->apps , " \t" ) ;
			while( p )
			{
				for( i = 0 ; i < pst->_apps_count ; i++ )
				{
					if( STRCMP( pst->apps[i].app , == , p ) )
						break;
				}
				if( i >= pst->_apps_count )
				{
					struct IbmsApp		app ;
					struct IbmsApp		*p_app = NULL ;
					
					DEBUGLOGSG( "add app[%s]" , p );
					
					memset( & app , 0x00 , sizeof(struct IbmsApp) );
					strcpy( app.app , p );
					p_app = QueryIbmsAppTreeNode( p_env , & app ) ;
					if( p_app )
					{
						char		*path = NULL ;
						char		so_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
						
						strcpy( pst->apps[pst->_apps_count].app , p_app->app );
						strcpy( pst->apps[pst->_apps_count].desc , p_app->desc );
						strcpy( pst->apps[pst->_apps_count].bin , p_app->bin );
						pst->apps[pst->_apps_count].timeout = p_app->timeout ;
						pst->apps[pst->_apps_count].timeout2 = p_app->timeout2 ;
						pst->apps[pst->_apps_count].invalid = p_app->invalid ;
						
						path = getenv("IBAS_SO_PATH") ;
						memset( so_pathfilename , 0x00 , sizeof(so_pathfilename) );
						if( path )
							snprintf( so_pathfilename , sizeof(so_pathfilename)-1 , "%s/%s" , path , p_app->bin );
						else
							snprintf( so_pathfilename , sizeof(so_pathfilename)-1 , "%s/so/%s" , getenv("HOME") , p_app->bin );
						nret = Md5File( so_pathfilename , pst->apps[pst->_apps_count].md5 ) ;
						if( nret )
						{
							ERRORLOGSG( "Md5File[%s] failed[%d]" , so_pathfilename , nret );
							return HTTP_INTERNAL_SERVER_ERROR;
						}
						
						pst->_apps_count++;
					}
				}
				
				p = gettok( NULL , " \t" ) ;
			}
		}
	}
	
	return HTTP_OK;
}

static int AddNodeToChangedList( struct IbmsEnv *p_env , char *node )
{
	struct NoticeNodeChanged	*p_notice_node_changed = NULL ;
	struct IbmsProject		*p_project = NULL ;
	
	p_project = NULL ;
	while( ( p_project = TravelIbmsProjectTreeNode( p_env , p_project ) ) )
	{
		if( strword( p_project->nodes , node ) )
		{
			list_for_each_entry( p_notice_node_changed , & (p_env->notice_nodes_changed_list.this_node) , struct NoticeNodeChanged , this_node )
			{
				if( STRCMP( p_notice_node_changed->node , == , node ) )
				{
					break;
				}
			}
			if( p_notice_node_changed == & (p_env->notice_nodes_changed_list) )
			{
				p_notice_node_changed = (struct NoticeNodeChanged *)malloc( sizeof(struct NoticeNodeChanged) ) ;
				if( p_notice_node_changed == NULL )
				{
					ERRORLOGSG( "malloc failed , errno[%d]" , errno );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				memset( p_notice_node_changed , 0x00 , sizeof(struct NoticeNodeChanged) );
				strcpy( p_notice_node_changed->node , node );
				list_add_tail( & (p_notice_node_changed->this_node) , & (p_env->notice_nodes_changed_list.this_node) );
			}
		}
	}
	
	return HTTP_OK;
}

static int AddNodesToChangedList( struct IbmsEnv *p_env , char *nodes )
{
	char			*node = NULL ;
	
	int			nret = 0 ;
	
	node = gettok( nodes , " \t" ) ;
	while( node )
	{
		nret = AddNodeToChangedList( p_env , node ) ;
		if( nret != HTTP_OK )
			return nret;
		
		node = gettok( NULL , " \t" ) ;
	}
	
	return HTTP_OK;
}

static int AddAppToChangedList( struct IbmsEnv *p_env , char *app )
{
	struct IbmsProject		*p_project = NULL ;
	char				*node = NULL ;
	struct NoticeNodeChanged	*p_notice_node_changed = NULL ;
	
	p_project = NULL ;
	while( ( p_project = TravelIbmsProjectTreeNode( p_env , p_project ) ) )
	{
		if( strword( p_project->apps , app ) )
		{
			node = gettok( p_project->nodes , " \t" ) ;
			while( node )
			{
				list_for_each_entry( p_notice_node_changed , & (p_env->notice_nodes_changed_list.this_node) , struct NoticeNodeChanged , this_node )
				{
					if( STRCMP( p_notice_node_changed->node , == , node ) )
					{
						break;
					}
				}
				if( p_notice_node_changed == & (p_env->notice_nodes_changed_list) )
				{
					p_notice_node_changed = (struct NoticeNodeChanged *)malloc( sizeof(struct NoticeNodeChanged) ) ;
					if( p_notice_node_changed == NULL )
					{
						ERRORLOGSG( "malloc failed , errno[%d]" , errno );
						return HTTP_INTERNAL_SERVER_ERROR;
					}
					memset( p_notice_node_changed , 0x00 , sizeof(struct NoticeNodeChanged) );
					strcpy( p_notice_node_changed->node , node );
					list_add_tail( & (p_notice_node_changed->this_node) , & (p_env->notice_nodes_changed_list.this_node) );
				}
				
				node = gettok( NULL , " \t" ) ;
			}
		}
	}
	
	return HTTP_OK;
}

int OnProcessShowNodes( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*buf = NULL ;
	struct IbmsNode		*p_node = NULL ;
	struct IbmsNodeHost	*p_node_host = NULL ;
	
	struct HttpBuffer	*b = NULL ;
	
	int			nret = 0 ;
	
	buf = AllocHttpBuffer( 4096 ) ;
	if( buf == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	p_node = NULL ;
	while( ( p_node = TravelIbmsNodeTreeNode( p_env , p_node ) ) )
	{
		nret = StrcatfHttpBuffer( buf , "%s %d" , p_node->node , p_node->invalid );
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			FreeHttpBuffer( buf );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		p_node_host = NULL ;
		while( ( p_node_host = TravelIbmsNodeHostTreeNode( p_node , p_node_host ) ) )
		{
			nret = StrcatfHttpBuffer( buf , " %s:%d" , p_node_host->ip , p_node_host->port );
			if( nret )
			{
				ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
				FreeHttpBuffer( buf );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
		}
		
		nret = StrcatfHttpBuffer( buf , "\n" );
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			FreeHttpBuffer( buf );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	
	b = GetHttpResponseBuffer( p_accepted_session->http ) ;
	nret = StrcatfHttpBuffer( b , "Content-length : %d" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "%.*s" , GetHttpBufferLength(buf) , GetHttpBufferLength(buf) , GetHttpBufferBase(buf,NULL) ) ;
	FreeHttpBuffer( buf );
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessShowApps( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*buf = NULL ;
	struct IbmsApp		*p_app = NULL ;
	
	struct HttpBuffer	*b = NULL ;
	
	int			nret = 0 ;
	
	buf = AllocHttpBuffer( 4096 ) ;
	if( buf == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	p_app = NULL ;
	while( ( p_app = TravelIbmsAppTreeNode( p_env , p_app ) ) )
	{
		nret = StrcatfHttpBuffer( buf , "%s %s %s %d %d %d\n" , p_app->app , p_app->desc , p_app->bin , p_app->timeout , p_app->timeout2 , p_app->invalid );
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			FreeHttpBuffer( buf );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	
	b = GetHttpResponseBuffer( p_accepted_session->http ) ;
	nret = StrcatfHttpBuffer( b , "Content-length : %d" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "%.*s" , GetHttpBufferLength(buf) , GetHttpBufferLength(buf) , GetHttpBufferBase(buf,NULL) ) ;
	FreeHttpBuffer( buf );
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessShowProjects( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*buf = NULL ;
	struct IbmsProject	*p_project = NULL ;
	
	struct HttpBuffer	*b = NULL ;
	
	int			nret = 0 ;
	
	buf = AllocHttpBuffer( 4096 ) ;
	if( buf == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	p_project = NULL ;
	while( ( p_project = TravelIbmsProjectTreeNode( p_env , p_project ) ) )
	{
		nret = StrcatfHttpBuffer( buf , "%s ||| %s ||| %s\n" , p_project->project , p_project->nodes , p_project->apps );
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			FreeHttpBuffer( buf );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	
	b = GetHttpResponseBuffer( p_accepted_session->http ) ;
	nret = StrcatfHttpBuffer( b , "Content-length : %d" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "%.*s" , GetHttpBufferLength(buf) , GetHttpBufferLength(buf) , GetHttpBufferBase(buf,NULL) ) ;
	FreeHttpBuffer( buf );
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessShowNodesChanged( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer		*buf = NULL ;
	struct NoticeNodeChanged	*p_notice_node_changed = NULL ;
	
	struct HttpBuffer		*b = NULL ;
	
	int				nret = 0 ;
	
	buf = AllocHttpBuffer( 4096 ) ;
	if( buf == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	list_for_each_entry( p_notice_node_changed , & (p_env->notice_nodes_changed_list.this_node) , struct NoticeNodeChanged , this_node )
	{
		nret = StrcatfHttpBuffer( buf , "%s\n" , p_notice_node_changed->node );
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			FreeHttpBuffer( buf );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	
	b = GetHttpResponseBuffer( p_accepted_session->http ) ;
	nret = StrcatfHttpBuffer( b , "Content-length : %d" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "%.*s" , GetHttpBufferLength(buf) , GetHttpBufferLength(buf) , GetHttpBufferBase(buf,NULL) ) ;
	FreeHttpBuffer( buf );
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessShowSessions( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*buf = NULL ;
	struct AcceptedSession	*p_session = NULL ;
	
	struct HttpBuffer	*b = NULL ;
	
	int			nret = 0 ;
	
	buf = AllocHttpBuffer( 4096 ) ;
	if( buf == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	list_for_each_entry( p_session , & (p_env->accepted_session_list.this_node) , struct AcceptedSession , this_node )
	{
		switch( p_session->role )
		{
			case ACCEPTEDSESSION_ROLE_UNKNOWN_IDENTITY:
				nret = StrcatfHttpBuffer( buf , "%s:%d UNKNOWN_IDENTITY\n" , p_session->netaddr.ip , p_session->netaddr.port );
				break;
			case ACCEPTEDSESSION_ROLE_IBMS:
				nret = StrcatfHttpBuffer( buf , "%s:%d ibms\n" , p_session->netaddr.ip , p_session->netaddr.port );
				break;
			case ACCEPTEDSESSION_ROLE_IBMA:
				nret = StrcatfHttpBuffer( buf , "%s:%d ibma %s\n" , p_session->netaddr.ip , p_session->netaddr.port , p_session->node.node );
				break;
			case ACCEPTEDSESSION_ROLE_IBAS:
				nret = StrcatfHttpBuffer( buf , "%s:%d ibas %s %s:%d\n" , p_session->netaddr.ip , p_session->netaddr.port , p_session->node.node , p_session->node_host.ip , p_session->node_host.port );
				break;
		}
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			FreeHttpBuffer( buf );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	
	b = GetHttpResponseBuffer( p_accepted_session->http ) ;
	nret = StrcatfHttpBuffer( b , "Content-length : %d" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "%.*s" , GetHttpBufferLength(buf) , GetHttpBufferLength(buf) , GetHttpBufferBase(buf,NULL) ) ;
	FreeHttpBuffer( buf );
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessAddNode( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_reqmsg	ibms_node ;
	struct IbmsNode		node ;
	struct IbmsNode		*p_node = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node.node , sizeof(node.node)-1 );
	node.invalid = ibms_node.invalid ;
	
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node )
	{
		ERRORLOGSG( "node[%s] found" , node.node );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 5" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	p_node = (struct IbmsNode *)malloc( sizeof(struct IbmsNode) ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "strndup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	memset( p_node , 0x00 , sizeof(struct IbmsNode) );
	
	strcpy( p_node->node , node.node );
	p_node->invalid = node.invalid ;
	
	nret = LinkIbmsNodeTreeNode( p_env , p_node ) ;
	if( nret )
	{
		ERRORLOGSG( "LinkIbmsNodeTreeNode[%s] failed[%d]" , p_node->node , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	nret = AddNodeToChangedList( p_env , node.node ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodeToChangedList[%s] failed[%d]" , node.node , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodeToChangedList[%s] ok" , node.node );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessSetNode( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_reqmsg	ibms_node ;
	struct IbmsNode		node ;
	struct IbmsNode		*p_node = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node.node , sizeof(node.node)-1 );
	node.invalid = ibms_node.invalid ;
	
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	if( node.invalid != -1 )
		p_node->invalid = node.invalid ;
	
	nret = AddNodeToChangedList( p_env , node.node ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodeToChangedList[%s] failed[%d]" , node.node , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodeToChangedList[%s] ok" , node.node );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessRemoveNode( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_reqmsg	ibms_node ;
	struct IbmsNode		node ;
	struct IbmsNode		*p_node = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node.node , sizeof(node.node)-1 );
	
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	UnlinkIbmsNodeTreeNode( p_env , p_node );
	free( p_node );
	
	nret = AddNodeToChangedList( p_env , node.node ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodeToChangedList[%s] failed[%d]" , node.node , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodeToChangedList[%s] ok" , node.node );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessAddNodeHost( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_host_reqmsg	ibms_node_host ;
	struct IbmsNode		node ;
	struct IbmsNodeHost	node_host ;
	struct IbmsNode		*p_node = NULL ;
	struct IbmsNodeHost	*p_node_host = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node_host , 0x00 , sizeof(ibms_node_host_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_host_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node_host ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_host_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node_host.node , sizeof(node.node)-1 );
	
	memset( & node_host , 0x00 , sizeof(struct IbmsNodeHost) );
	strncpy( node_host.ip , ibms_node_host.ip , sizeof(node_host.ip)-1 );
	node_host.port = ibms_node_host.port ;
	
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	p_node_host = QueryIbmsNodeHostTreeNode( p_node , & node_host ) ;
	if( p_node_host )
	{
		ERRORLOGSG( "node[%s] host[%s:%d] found" , node.node , node_host.ip , node_host.port );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 5" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	p_node_host = (struct IbmsNodeHost *)malloc( sizeof(struct IbmsNodeHost) ) ;
	if( p_node_host == NULL )
	{
		ERRORLOGSG( "strndup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	memset( p_node_host , 0x00 , sizeof(struct IbmsNodeHost) );
	
	strcpy( p_node_host->ip , node_host.ip );
	p_node_host->port = node_host.port ;
#if 0
	p_node_host->invalid = 1 ;
#endif
	
	nret = LinkIbmsNodeHostTreeNode( p_node , p_node_host ) ;
	if( nret )
	{
		ERRORLOGSG( "LinkIbmsNodeHostTreeNode[%s][%s:%d] failed[%d]" , p_node->node , p_node_host->ip , p_node_host->port , nret );
		return HTTP_BAD_REQUEST;
	}
	
	nret = AddNodeToChangedList( p_env , node.node ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodeToChangedList[%s] failed[%d]" , node.node , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodeToChangedList[%s] ok" , node.node );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessRemoveNodeHost( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_host_reqmsg	ibms_node_host ;
	struct IbmsNode		node ;
	struct IbmsNodeHost	node_host ;
	struct IbmsNode		*p_node = NULL ;
	struct IbmsNodeHost	*p_node_host = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node_host , 0x00 , sizeof(ibms_node_host_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_host_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node_host ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_host_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node_host.node , sizeof(node.node)-1 );
	
	memset( & node_host , 0x00 , sizeof(struct IbmsNodeHost) );
	strncpy( node_host.ip , ibms_node_host.ip , sizeof(node_host.ip)-1 );
	node_host.port = ibms_node_host.port ;
	
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	p_node_host = QueryIbmsNodeHostTreeNode( p_node , & node_host ) ;
	if( p_node_host == NULL )
	{
		ERRORLOGSG( "node[%s] host[%s:%d] not found" , node.node , node_host.ip , node_host.port );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	UnlinkIbmsNodeHostTreeNode( p_node , p_node_host );
	free( p_node_host );
	
	nret = AddNodeToChangedList( p_env , node.node ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodeToChangedList[%s] failed[%d]" , node.node , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodeToChangedList[%s] ok" , node.node );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessAddApp( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_app_reqmsg		ibms_app ;
	struct IbmsApp		app ;
	struct IbmsApp		*p_app = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_app_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_app ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_app_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & app , 0x00 , sizeof(struct IbmsApp) );
	strncpy( app.app , ibms_app.app , sizeof(app.app)-1 );
	strncpy( app.desc , ibms_app.desc , sizeof(app.desc)-1 );
	strncpy( app.bin , ibms_app.bin , sizeof(app.bin)-1 );
	app.timeout = ibms_app.timeout ;
	app.timeout2 = ibms_app.timeout2 ;
	app.invalid = ibms_app.invalid ;
	
	p_app = QueryIbmsAppTreeNode( p_env , & app ) ;
	if( p_app )
	{
		ERRORLOGSG( "app[%s] found" , app.app );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 5" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	p_app = (struct IbmsApp *)malloc( sizeof(struct IbmsApp) ) ;
	if( p_app == NULL )
	{
		ERRORLOGSG( "strndup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	memset( p_app , 0x00 , sizeof(struct IbmsApp) );
	
	strcpy( p_app->app , app.app );
	strcpy( p_app->desc , app.desc );
	strcpy( p_app->bin , app.bin );
	p_app->timeout = app.timeout ;
	p_app->timeout2 = app.timeout2 ;
	p_app->invalid = app.invalid ;
	
	nret = LinkIbmsAppTreeNode( p_env , p_app ) ;
	if( nret )
	{
		ERRORLOGSG( "LinkIbmsAppTreeNode[%s] failed[%d]" , p_app->app , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	nret = AddAppToChangedList( p_env , app.app ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddAppToChangedList[%s] failed[%d]" , app.app , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddAppToChangedList[%s] ok" , app.app );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessSetApp( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_app_reqmsg		ibms_app ;
	struct IbmsApp		app ;
	struct IbmsApp		*p_app = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_app_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_app ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_app_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & app , 0x00 , sizeof(struct IbmsApp) );
	strncpy( app.app , ibms_app.app , sizeof(app.app)-1 );
	strncpy( app.desc , ibms_app.desc , sizeof(app.desc)-1 );
	strncpy( app.bin , ibms_app.bin , sizeof(app.bin)-1 );
	app.timeout = ibms_app.timeout ;
	app.timeout2 = ibms_app.timeout2 ;
	app.invalid = ibms_app.invalid ;
	
	p_app = QueryIbmsAppTreeNode( p_env , & app ) ;
	if( p_app == NULL )
	{
		ERRORLOGSG( "app[%s] not found" , app.app );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	strcpy( p_app->desc , app.desc );
	strcpy( p_app->bin , app.bin );
	p_app->timeout = app.timeout ;
	p_app->timeout2 = app.timeout2 ;
	p_app->invalid = app.invalid ;
	
	nret = AddAppToChangedList( p_env , app.app ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddAppToChangedList[%s] failed[%d]" , app.app , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddAppToChangedList[%s] ok" , app.app );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessRemoveApp( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_app_reqmsg		ibms_app ;
	struct IbmsApp		app ;
	struct IbmsApp		*p_app = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_app_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_app ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_app_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & app , 0x00 , sizeof(struct IbmsApp) );
	strncpy( app.app , ibms_app.app , sizeof(app.app)-1 );
	
	p_app = QueryIbmsAppTreeNode( p_env , & app ) ;
	if( p_app == NULL )
	{
		ERRORLOGSG( "app[%s] not found" , app.app );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	UnlinkIbmsAppTreeNode( p_env , p_app );
	free( p_app );
	
	nret = AddAppToChangedList( p_env , app.app ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddAppToChangedList[%s] failed[%d]" , app.app , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddAppToChangedList[%s] ok" , app.app );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessAddProject( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_project_reqmsg	ibms_project ;
	struct IbmsProject	project ;
	struct IbmsProject	*p_project = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_project , 0x00 , sizeof(ibms_project_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_project_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_project ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_project_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & project , 0x00 , sizeof(struct IbmsProject) );
	strncpy( project.project , ibms_project.project , sizeof(project.project)-1 );
	project.nodes = strdup(ibms_project.nodes) ;
	if( project.nodes == NULL )
	{
		ERRORLOGSG( "strdup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	project.apps = strdup(ibms_project.apps) ;
	if( project.apps == NULL )
	{
		ERRORLOGSG( "strdup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	p_project = QueryIbmsProjectTreeNode( p_env , & project ) ;
	if( p_project )
	{
		ERRORLOGSG( "project[%s] found" , project.project );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 5" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			free( project.nodes );
			free( project.apps );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	p_project = (struct IbmsProject *)malloc( sizeof(struct IbmsProject) ) ;
	if( p_project == NULL )
	{
		ERRORLOGSG( "strndup failed , errno[%d]" , errno );
		free( project.nodes );
		free( project.apps );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	memset( p_project , 0x00 , sizeof(struct IbmsProject) );
	
	strcpy( p_project->project , project.project );
	p_project->nodes = project.nodes ;
	p_project->apps = project.apps ;
	
	nret = LinkIbmsProjectTreeNode( p_env , p_project ) ;
	if( nret )
	{
		ERRORLOGSG( "LinkIbmsProjectTreeNode[%s] failed[%d]" , p_project->project , nret );
		free( project.nodes );
		free( project.apps );
		free( p_project );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	nret = AddNodesToChangedList( p_env , project.nodes ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodesToChangedList[%s] failed[%d]" , project.nodes , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodesToChangedList[%s] ok" , project.nodes );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessSetProject( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_project_reqmsg	ibms_project ;
	struct IbmsProject	project ;
	struct IbmsProject	*p_project = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_project , 0x00 , sizeof(ibms_project_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_project_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_project ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_project_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & project , 0x00 , sizeof(struct IbmsProject) );
	strncpy( project.project , ibms_project.project , sizeof(project.project)-1 );
	project.nodes = strdup(ibms_project.nodes) ;
	if( project.nodes == NULL )
	{
		ERRORLOGSG( "strdup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	project.apps = strdup(ibms_project.apps) ;
	if( project.apps == NULL )
	{
		ERRORLOGSG( "strdup failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	p_project = QueryIbmsProjectTreeNode( p_env , & project ) ;
	if( p_project == NULL )
	{
		ERRORLOGSG( "project[%s] not found" , project.project );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			free( project.nodes );
			free( project.apps );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	free( p_project->nodes ); p_project->nodes = project.nodes ;
	free( p_project->apps ); p_project->apps = project.apps ;
	
	nret = AddNodesToChangedList( p_env , project.nodes ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodesToChangedList[%s] failed[%d]" , project.project , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodesToChangedList[%s] ok" , project.project );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessRemoveProject( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_project_reqmsg	ibms_project ;
	struct IbmsProject	project ;
	struct IbmsProject	*p_project = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_project , 0x00 , sizeof(ibms_project_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_project_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_project ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_project_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & project , 0x00 , sizeof(struct IbmsProject) );
	strncpy( project.project , ibms_project.project , sizeof(project.project)-1 );
	
	p_project = QueryIbmsProjectTreeNode( p_env , & project ) ;
	if( p_project == NULL )
	{
		ERRORLOGSG( "project[%s] not found" , project.project );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	UnlinkIbmsProjectTreeNode( p_env , p_project );
	free( p_project );
	
	nret = AddNodesToChangedList( p_env , project.nodes ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "AddNodesToChangedList[%s] failed[%d]" , project.nodes , nret );
		free( p_project );
		return nret;
	}
	else
	{
		INFOLOGSG( "AddNodesToChangedList[%s] ok" , project.nodes );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessCommitChanged( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer		*rsp_buf = NULL ;
	
	struct IbmsNode			node ;
	struct IbmsNode			*p_node = NULL ;
	struct IbmsNodeHost		*p_node_host = NULL ;
	struct IbmsApp			*p_app = NULL ;
	struct IbmsProject		*p_project = NULL ;
	
	FILE				*fp = NULL ;
	
	struct list_head		*node1 = NULL , *next1 = NULL ;
	struct NoticeNodeChanged	*p_notice_node_changed = NULL ;
	struct AcceptedSession		*p_close_session = NULL , *p_next_close_session = NULL ;
	struct AcceptedSession		*p_change_session = NULL , *p_next_change_session = NULL ;
	
	struct epoll_event		event ;
	
	int				nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	INFOLOGSG( "rewrite config files ..." );
	
	fp = fopen( p_env->nodes_conf_pathfilename , "w" ) ;
	if( fp == NULL )
	{
		ERRORLOGSG( "fopen[%s] failed , errno[%d]" , p_env->nodes_conf_pathfilename , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	fprintf( fp , "{\n" );
	fprintf( fp , "	\"nodes\" :\n" );
	fprintf( fp , "	[\n" );
	p_node = NULL ;
	while( ( p_node = TravelIbmsNodeTreeNode( p_env , p_node ) ) )
	{
	fprintf( fp , "	{\n" );
	fprintf( fp , "		\"node\" : \"%s\" ,\n" , p_node->node );
	fprintf( fp , "		\"invalid\" : \"%d\" ,\n" , p_node->invalid );
	fprintf( fp , "		\"hosts\" :\n" );
	fprintf( fp , "		[\n" );
	p_node_host = NULL ;
	while( ( p_node_host = TravelIbmsNodeHostTreeNode( p_node , p_node_host ) ) )
	{
	fprintf( fp , "		{ \"ip\" : \"%s\" , \"port\" : %d }%s\n" , p_node_host->ip , p_node_host->port , ((rb_next(&(p_node_host->node_host_rbnode)))?" ,":"") );
	}
	fprintf( fp , "		]\n" );
	fprintf( fp , "	}%s\n" , ((rb_next(&(p_node->node_rbnode)))?" ,":"") );
	}
	fprintf( fp , "	]\n" );
	fprintf( fp , "}\n" );
	
	fclose( fp );
	
	fp = fopen( p_env->apps_conf_pathfilename , "w" ) ;
	if( fp == NULL )
	{
		ERRORLOGSG( "fopen[%s] failed , errno[%d]" , p_env->apps_conf_pathfilename , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	fprintf( fp , "{\n" );
	fprintf( fp , "	\"apps\" :\n" );
	fprintf( fp , "	[\n" );
	p_app = NULL ;
	while( ( p_app = TravelIbmsAppTreeNode( p_env , p_app ) ) )
	{
	fprintf( fp , "	{\n" );
	fprintf( fp , "		\"app\" : \"%s\" ,\n" , p_app->app );
	fprintf( fp , "		\"desc\" : \"%s\" ,\n" , p_app->desc );
	fprintf( fp , "		\"bin\" : \"%s\" ,\n" , p_app->bin );
	fprintf( fp , "		\"timeout\" : \"%d\" ,\n" , p_app->timeout );
	fprintf( fp , "		\"timeout2\" : \"%d\" ,\n" , p_app->timeout2 );
	fprintf( fp , "		\"invalid\" : \"%d\"\n" , p_app->invalid );
	fprintf( fp , "	}%s\n" , ((rb_next(&(p_app->app_rbnode)))?" ,":"") );
	}
	fprintf( fp , "	]\n" );
	fprintf( fp , "}\n" );
	
	fclose( fp );
	
	fp = fopen( p_env->projects_conf_pathfilename , "w" ) ;
	if( fp == NULL )
	{
		ERRORLOGSG( "fopen[%s] failed , errno[%d]" , p_env->projects_conf_pathfilename , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	fprintf( fp , "{\n" );
	fprintf( fp , "	\"projects\" :\n" );
	fprintf( fp , "	[\n" );
	p_project = NULL ;
	while( ( p_project = TravelIbmsProjectTreeNode( p_env , p_project ) ) )
	{
	fprintf( fp , "	{\n" );
	fprintf( fp , "		\"project\" : \"%s\" ,\n" , p_project->project );
	fprintf( fp , "		\"nodes\" : \"%s\" ,\n" , p_project->nodes );
	fprintf( fp , "		\"apps\" : \"%s\"\n" , p_project->apps );
	fprintf( fp , "	}%s\n" , ((rb_next(&(p_project->project_rbnode)))?" ,":"") );
	}
	fprintf( fp , "	]\n" );
	fprintf( fp , "}\n" );
	
	fclose( fp );
	
	INFOLOGSG( "rewrite config files ... finish" );
	
	INFOLOGSG( "close session from nodes removed ..." );
	
	list_for_each_entry_safe( p_close_session , p_next_close_session , & (p_env->accepted_session_list.this_node) , struct AcceptedSession , this_node )
	{
		if( STRCMP( p_close_session->node.node , != , "" ) && p_close_session->role == ACCEPTEDSESSION_ROLE_IBMA && p_close_session->do_what == ACCEPTEDSESSION_DO_KEEP_SILENT )
		{
			memset( & node , 0x00 , sizeof(struct IbmsNode) );
			strcpy( node.node , p_close_session->node.node );
			p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
			if( p_node == NULL )
			{
				OnClosingSocket( p_env , p_close_session );
			}
		}
	}
	
	INFOLOGSG( "close session from nodes removed ... finish" );
	
	INFOLOGSG( "notice nodes changed ..." );
	
	list_for_each_safe( node1 , next1 , & (p_env->notice_nodes_changed_list.this_node) )
	{
		p_notice_node_changed = list_entry( node1 , struct NoticeNodeChanged , this_node ) ;
		
		list_for_each_entry_safe( p_change_session , p_next_change_session , & (p_env->accepted_session_list.this_node) , struct AcceptedSession , this_node )
		{
			if( STRCMP( p_change_session->node.node , == , p_notice_node_changed->node ) && p_change_session->role == ACCEPTEDSESSION_ROLE_IBMA && p_change_session->do_what == ACCEPTEDSESSION_DO_KEEP_SILENT )
			{
				ibp_fetch_config_rspmsg		*ibp_fetch_config = NULL ;
				char				*reqmsg = NULL ;
				int				reqmsg_len ;
				
				struct HttpBuffer		*req_buf = NULL ;
				
				int				nret = 0 ;
				
				INFOLOGSG( "notice session[%s]" , p_notice_node_changed->node );
				
				ibp_fetch_config = (ibp_fetch_config_rspmsg *)malloc( sizeof(ibp_fetch_config_rspmsg) ) ;
				if( ibp_fetch_config == NULL )
				{
					ERRORLOGSG( "malloc failed , errno[%d]" , errno );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				memset( ibp_fetch_config , 0x00 , sizeof(ibp_fetch_config_rspmsg) );
				
				nret = PackFetchConfigMessage( p_env , p_change_session->node.node , ibp_fetch_config ) ;
				if( nret != HTTP_OK )
				{
					ERRORLOGSG( "PackFetchConfigMessage failed[%d]" , nret );
					free( ibp_fetch_config );
					return nret;
				}
				
				reqmsg_len = 0 ;
				nret = DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg( ibp_fetch_config , "GB18030" , & reqmsg , NULL , & reqmsg_len ) ;
				if( nret )
				{
					ERRORLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg failed[%d] , errno[%d]" , nret , errno );
					free( ibp_fetch_config );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				else
				{
					DEBUGLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg ok , msg[%d][%.*s]" , reqmsg_len , reqmsg_len , reqmsg );
				}
				
				req_buf = GetHttpRequestBuffer( p_change_session->http ) ;
				nret = StrcatfHttpBuffer( req_buf , "POST " IBP_URI_DOWNLOAD_CONFIG " HTTP/1.0" HTTP_RETURN_NEWLINE
								"Connection: keep-alive" HTTP_RETURN_NEWLINE
								"Content-length : %d" HTTP_RETURN_NEWLINE
								HTTP_RETURN_NEWLINE
								"%.*s"
								, reqmsg_len
								, reqmsg_len , reqmsg ) ;
				if( nret )
				{
					ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
					free( reqmsg );
					free( ibp_fetch_config );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				else
				{
					DEBUGHEXLOGSG( GetHttpBufferBase(req_buf,NULL) , GetHttpBufferLength(req_buf) , "StrcatfHttpBuffer ok" );
				}
				
				free( reqmsg );
				free( ibp_fetch_config );
				
				SetHttpTimeout( p_change_session->http , 60 );
				
				p_change_session->do_what = ACCEPTEDSESSION_DO_DOWNLOAD_CONFIG ;
				
				memset( & event , 0x00 , sizeof(struct epoll_event) );
				event.events = EPOLLOUT | EPOLLERR ;
				event.data.ptr = p_change_session ;
				nret = epoll_ctl( p_env->epoll_fd , EPOLL_CTL_MOD , p_change_session->netaddr.sock , & event ) ;
				if( nret == -1 )
				{
					ERRORLOGSG( "epoll_ctl[%d] modify[%d] failed , errno[%d]" , p_env->epoll_fd , p_change_session->netaddr.sock , errno );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				else
				{
					DEBUGLOGSG( "epoll_ctl[%d] modify[%d] ok" , p_env->epoll_fd , p_change_session->netaddr.sock );
				}
				
				INFOLOGSG( "p_notice_node_changed->node[%s] p_change_session[%p]" , p_notice_node_changed->node , p_change_session );
			}
		}
		
		list_del( node1 );
		free( p_notice_node_changed );
	}
	
	INFOLOGSG( "notice nodes changed ... finish" );
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessFetchConfig( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	char				*req_msg = NULL ;
	int				req_msg_len ;
	ibms_node_reqmsg		ibms_node ;
	struct IbmsNode			node ;
	struct IbmsNode			*p_node = NULL ;
	
	ibp_fetch_config_rspmsg		*pst = NULL ;
	char				*rsp_msg = NULL ;
	int				rsp_msg_len ;
	
	int				nret = 0 ;
	
	nret = ExtractMessageFromHttpBuffer( p_accepted_session->http , GetHttpRequestBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , & req_msg , & req_msg_len  ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "post node not found" );
		
		nret = FormatHttpResponseStartLine( HTTP_BAD_REQUEST , p_accepted_session->http , 1 , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "FormatHttpResponseStartLine failed[%d]" , nret );
			return 1;
		}
		
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node.node , sizeof(node.node)-1 );
	INFOLOGSG( "session[%s]" , node.node );
	
	pst = (ibp_fetch_config_rspmsg *)malloc( sizeof(ibp_fetch_config_rspmsg) ) ;
	if( pst == NULL )
	{
		ERRORLOGSG( "malloc failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	memset( pst , 0x00 , sizeof(ibp_fetch_config_rspmsg) );
	
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		pst->response_code = 1 ;
		snprintf( pst->response_desc , sizeof(pst->response_desc)-1 , "node[%s] not found in config" , node.node );
	}
	else
	{
		memcpy( & (p_accepted_session->node) , p_node , sizeof(struct IbmsNode) );
		
		nret = PackFetchConfigMessage( p_env , node.node , pst ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "PackFetchConfigMessage failed[%d]" , nret );
			free( pst );
			return nret;
		}
		
		pst->response_code = 0 ;
	}
	
	rsp_msg_len = 0 ;
	nret = DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg( pst , "GB18030" , & rsp_msg , NULL , & rsp_msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg failed[%d] , errno[%d]" , nret , errno );
		free( pst );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		DEBUGLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg ok , msg[%d][%.*s]" , rsp_msg_len , rsp_msg_len , rsp_msg );
	}
	
	nret = PutMessageToHttpBuffer( p_accepted_session->http , GetHttpResponseBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , rsp_msg , rsp_msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "PutMessageToHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		free( rsp_msg );
		free( pst );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	free( rsp_msg );
	free( pst );
	
	return HTTP_OK;
}

int OnProcessFetchKey( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_reqmsg	ibms_node ;
	char			*path = NULL ;
	char			key_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	
	int			nret = 0 ;
	
	nret = ExtractMessageFromHttpBuffer( p_accepted_session->http , GetHttpRequestBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , & req_msg , & req_msg_len  ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	path = getenv("IBAS_KEY_PATH") ;
	memset( key_pathfilename , 0x00 , sizeof(key_pathfilename) );
	if( path )
		snprintf( key_pathfilename , sizeof(key_pathfilename)-1 , "%s/%s.%s" , path , ibms_node.node , (ibms_node.flag?"prikey":"pubkey") );
	else
		snprintf( key_pathfilename , sizeof(key_pathfilename)-1 , "%s/keys/%s.%s" , getenv("HOME") , ibms_node.node , (ibms_node.flag?"prikey":"pubkey") );
	nret = PutFileToHttpBuffer( p_accepted_session->http , GetHttpResponseBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , key_pathfilename , NULL ) ;
	if( nret )
	{
		ERRORLOGSG( "PutFileToHttpBuffer[%s] failed[%d]" , key_pathfilename , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessFetchBin( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_app_reqmsg		ibms_app ;
	char			*p = NULL ;
	char			so_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	
	int			nret = 0 ;
	
	nret = ExtractMessageFromHttpBuffer( p_accepted_session->http , GetHttpRequestBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , & req_msg , & req_msg_len  ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "req_msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_app_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_app ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_app_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	p = getenv("IBAS_SO_PATH") ;
	memset( so_pathfilename , 0x00 , sizeof(so_pathfilename) );
	if( p )
		snprintf( so_pathfilename , sizeof(so_pathfilename)-1 , "%s/%s" , p , ibms_app.bin );
	else
		snprintf( so_pathfilename , sizeof(so_pathfilename)-1 , "%s/so/%s" , getenv("HOME") , ibms_app.bin );
	nret = PutFileToHttpBuffer( p_accepted_session->http , GetHttpResponseBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , so_pathfilename , NULL ) ;
	if( nret )
	{
		ERRORLOGSG( "PutFileToHttpBuffer[%s] failed[%d]" , so_pathfilename , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

int OnProcessDownloadConfig( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	char			*req_msg = NULL ;
	int			req_msg_len ;
	ibms_node_reqmsg	ibms_node ;
	struct IbmsNode		node ;
	struct IbmsNode		*p_node = NULL ;
	struct AcceptedSession	*p_change_session = NULL ;
	struct epoll_event	event ;
	
	int			nret = 0 ;
	
	req_msg = GetHttpBodyPtr( p_accepted_session->http , & req_msg_len ) ;
	if( req_msg_len <= 0 )
	{
		ERRORLOGSG( "msg_len[%d]invalid" , req_msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibms_node_reqmsg( "GB18030" , req_msg , & req_msg_len , & ibms_node ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_node_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strncpy( node.node , ibms_node.node , sizeof(node.node)-1 );

	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		
		nret = PutMessageToHttpBuffer( p_accepted_session->http , GetHttpResponseBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , HTTP_BODY_NOT_FOUND , sizeof(HTTP_BODY_NOT_FOUND)-1 ) ;
		if( nret )
		{
			ERRORLOGSG( "PutMessageToHttpBuffer failed[%d]" , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	list_for_each_entry( p_change_session , & (p_env->accepted_session_list.this_node) , struct AcceptedSession , this_node )
	{
		if( STRCMP( p_change_session->node.node , == , node.node ) && p_change_session->role == ACCEPTEDSESSION_ROLE_IBMA && p_change_session->do_what == ACCEPTEDSESSION_DO_KEEP_SILENT )
		{
			ibp_fetch_config_rspmsg		*fetch_config = NULL ;
			char				*req_msg = NULL ;
			int				req_msg_len ;
			
			struct HttpBuffer		*req_buf = NULL ;
			
			int				nret = 0 ;
			
			INFOLOGSG( "node[%s] p_change_session[%p]" , node.node , p_change_session );
			
			fetch_config = (ibp_fetch_config_rspmsg *)malloc( sizeof(ibp_fetch_config_rspmsg) ) ;
			if( fetch_config == NULL )
			{
				ERRORLOGSG( "malloc failed , errno[%d]" , errno );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			memset( fetch_config , 0x00 , sizeof(ibp_fetch_config_rspmsg) );
			
			nret = PackFetchConfigMessage( p_env , node.node , fetch_config ) ;
			if( nret != HTTP_OK )
			{
				ERRORLOGSG( "PackFetchConfigMessage failed[%d]" , nret );
				free( fetch_config );
				return nret;
			}
			
			req_msg_len = 0 ;
			nret = DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg( fetch_config , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
			if( nret )
			{
				ERRORLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg failed[%d] , errno[%d]" , nret , errno );
				free( fetch_config );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			else
			{
				DEBUGLOGSG( "DSCSERIALIZE_JSON_DUP_ibp_fetch_config_rspmsg ok , msg[%d][%.*s]" , req_msg_len , req_msg_len , req_msg );
			}
			
			req_buf = GetHttpRequestBuffer( p_change_session->http ) ;
			nret = StrcatfHttpBuffer( req_buf , "POST " IBP_URI_DOWNLOAD_CONFIG " HTTP/1.0" HTTP_RETURN_NEWLINE
							"Connection: keep-alive" HTTP_RETURN_NEWLINE ) ;
			if( nret )
			{
				ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
				free( req_msg );
				free( fetch_config );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			
			nret = PutMessageToHttpBuffer( p_accepted_session->http , GetHttpResponseBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , req_msg , req_msg_len ) ;
			if( nret )
			{
				ERRORLOGSG( "PutMessageToHttpBuffer failed[%d] , errno[%d]" , nret , errno );
				free( req_msg );
				free( fetch_config );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			
			free( req_msg );
			free( fetch_config );
			
			SetHttpTimeout( p_change_session->http , 60 );
			
			p_change_session->do_what = ACCEPTEDSESSION_DO_DOWNLOAD_CONFIG ;
			
			memset( & event , 0x00 , sizeof(struct epoll_event) );
			event.events = EPOLLOUT | EPOLLERR ;
			event.data.ptr = p_change_session ;
			nret = epoll_ctl( p_env->epoll_fd , EPOLL_CTL_MOD , p_change_session->netaddr.sock , & event ) ;
			if( nret == -1 )
			{
				ERRORLOGSG( "epoll_ctl[%d] modify[%d] failed , errno[%d]" , p_env->epoll_fd , p_change_session->netaddr.sock , errno );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			else
			{
				DEBUGLOGSG( "epoll_ctl[%d] modify[%d] ok" , p_env->epoll_fd , p_change_session->netaddr.sock );
			}
		}
	}
	
	nret = PutMessageToHttpBuffer( p_accepted_session->http , GetHttpResponseBuffer(p_accepted_session->http) , p_accepted_session->http_secure_env , HTTP_BODY_FOUND , sizeof(HTTP_BODY_FOUND)-1 ) ;
	if( nret )
	{
		ERRORLOGSG( "PutMessageToHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}

#if 0
int OnProcessAliveReportFirst( struct IbmsEnv *p_env , struct AcceptedSession *p_accepted_session )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	char			*msg = NULL ;
	int			msg_len ;
	ibp_alive_report_reqmsg	ibp_alive_report ;
	
	struct IbmsNode		node ;
	struct IbmsNodeHost	node_host ;
	struct IbmsNode		*p_node = NULL ;
	struct IbmsNodeHost	*p_node_host = NULL ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_accepted_session->http ) ;
	
	msg = GetHttpBodyPtr( p_accepted_session->http , & msg_len ) ;
	if( msg_len <= 0 )
	{
		ERRORLOGSG( "msg_len[%d]invalid" , msg_len );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & ibp_alive_report , 0x00 , sizeof(ibp_alive_report_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibp_alive_report_reqmsg( "GB18030" , msg , & msg_len , & ibp_alive_report ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibp_alive_report_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	
	memset( & node , 0x00 , sizeof(struct IbmsNode) );
	strcpy( node.node , ibp_alive_report.node );
	p_node = QueryIbmsNodeTreeNode( p_env , & node ) ;
	if( p_node == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , node.node );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	memset( & node_host , 0x00 , sizeof(struct IbmsNodeHost) );
	strcpy( node_host.ip , ibp_alive_report.ip );
	node_host.port = ibp_alive_report.port ;
	p_node_host = QueryIbmsNodeHostTreeNode( p_node , & node_host ) ;
	if( p_node_host == NULL )
	{
		ERRORLOGSG( "node[%s] host[%s:%d] not found" , node.node , node_host.ip , node_host.port );
		
		nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 9" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "NOT FOUND" ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		return HTTP_OK;
	}
	
	memcpy( & (p_accepted_session->node) , p_node , sizeof(struct IbmsNode) );
	memcpy( & (p_accepted_session->node_host) , p_node_host , sizeof(struct IbmsNodeHost) );
	
	p_node_host->alive_connections++;
	p_node_host->invalid = 0 ;
	
	memcpy( & (p_accepted_session->node) , p_node , sizeof(struct IbmsNode) );
	memcpy( & (p_accepted_session->node_host) , p_node_host , sizeof(struct IbmsNodeHost) );
	
	nret = NoticeNodesInvalid( p_env , p_accepted_session ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "NoticeNodesInvalid failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "NoticeNodesInvalid ok" );
	}
	
	nret = StrcatfHttpBuffer( rsp_buf , "Content-length : 2" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE "OK" ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	return HTTP_OK;
}
#endif

