STRUCT					ibas_conf
{
	STRUCT				ibas
	{
		STRUCT			server
		{
			STRING 20	ip
			INT 4		port
		}
		
		STRUCT			mpm
		{
			STRING 20	mode
			INT 4		max_count
			STRUCT		DYNAMIC_WORKERPOOL
			{
				INT 4	min_idle_count
				INT 4	max_idle_count
				INT 4	create_interval
				INT 4	destroy_interval
				UINT 4	max_process_count
			}
		}
		
		STRUCT			app
		{
			INT 4		max_so_cache_count
		}
		
		STRUCT	log
		{
			STRING 256	iblog_server
			STRING 256	event_output
			STRING 256	main_output
			STRING 6	main_loglevel
			STRING 256	monitor_output
			STRING 6	monitor_loglevel
			STRING 256	worker_output
			STRING 6	worker_loglevel
			STRING 256	app_output
			STRING 6	app_loglevel
		}
	}
	
	STRUCT				ibma
	{
		STRING 256		config_filename
	}
	
	STRUCT				ibms
	{
		STRUCT			server
		{
			STRING 20	ip
			INT 4		port
		}
	}
}

