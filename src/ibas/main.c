#include "ibas_in.h"

char		*g_ibp_ibas_kernel_version = IBP_VERSION ;

static void version()
{
	printf( "ibas v%s build %s %s\n" , g_ibp_ibas_kernel_version , __DATE__ , __TIME__ );
	printf( "Copyright by calvin 2017\n" );
	printf( "Email : calvinwilliams@163.com\n" );
	return;
}

static void usage()
{
	printf( "USAGE : ibas -f ibas.conf -a start\n" );
	printf( "                          -s status\n" );
	printf( "                          -s bbstat\n" );
	printf( "                          -s bbdetail\n" );
	
	return;
}

int main( int argc , char *argv[] )
{
	struct IbasEnv		*p_env = NULL ;
	int			i ;
	
	int			nret = 0 ;
	
	if( argc > 1 )
	{
		p_env = (struct IbasEnv *)malloc( sizeof(struct IbasEnv) ) ;
		if( p_env == NULL )
		{
			printf( "malloc failed , errno[%d]\n" , errno );
			return 1;
		}
		memset( p_env , 0x00 , sizeof(struct IbasEnv) );
		
		g_p_ibas_env = p_env ;
		
		for( i = 1 ; i < argc ; i++ )
		{
			if( strcmp( argv[i] , "-v" ) == 0 )
			{
				version();
				exit(0);
			}
			else if( strcmp( argv[i] , "-f" ) == 0 && i + 1 < argc )
			{
				p_env->ibas_conf_filename = argv[++i] ;
			}
			else if( strcmp( argv[i] , "-a" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param._action = argv[++i] ;
			}
			else if( strcmp( argv[i] , "-s" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param._show = argv[++i] ;
			}
			else
			{
				printf( "Invalid opt[%s]\r\n" , argv[i] );
				usage();
				exit(7);
			}
		}
		p_env->argv = argv ;
		
		if( p_env->ibas_conf_filename == NULL )
			p_env->ibas_conf_filename = "ibas.conf" ;
		
		memset( p_env->ibas_conf_pathfilename , 0x00 , sizeof(p_env->ibas_conf_pathfilename) );
		snprintf( p_env->ibas_conf_pathfilename , sizeof(p_env->ibas_conf_pathfilename)-1 , "%s/etc/%s" , getenv("HOME") , p_env->ibas_conf_filename );
		
		nret = LoadConfig( p_env->ibas_conf_pathfilename , p_env ) ;
		if( nret )
		{
			printf( "LoadConfig[%s] failed[%d]\n" , p_env->ibas_conf_pathfilename , nret );
			return 1;
		}
		
		IBPCleanLogEnv();
		IBPInitLogEnv( p_env->ibas_conf.ibas.log.iblog_server , "ibas.main" , p_env->ibas_conf.ibas.log.event_output , IBPConvertLogLevel(p_env->ibas_conf.ibas.log.main_loglevel) , "%s" , p_env->ibas_conf.ibas.log.main_output );
		
		if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "start" ) )
		{
			nret = InitEnvironment( p_env ) ;
			if( nret )
			{
				printf( "InitEnvironment failed[%d]\n" , nret );
				return 1;
			}
			
#if 0
			nret = OnProcessAliveReport( p_env ) ;
			if( nret != HTTP_OK )
			{
				printf( "OnProcessAliveReport failed[%d]" , nret );
				return 1;
			}
#endif
			
			nret = BindDaemonServer( & _monitor , p_env ) ;
			if( nret < 0 )
			{
				printf( "BindDaemonServer failed[%d]\n" , nret );
			}
			
			CleanEnvironment( p_env , 0 );
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "status" ) )
		{
			nret = ShowStatus( p_env ) ; 
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "bbstat" ) )
		{
			nret = ShowBbStat( p_env ) ; 
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "bbdetail" ) )
		{
			nret = ShowBbDetail( p_env ) ; 
		}
		else
		{
			usage();
			exit(7);
		}
		
		free( p_env );
	}
	else
	{
		usage();
		return 7;
	}
	
	return abs(nret);
}

