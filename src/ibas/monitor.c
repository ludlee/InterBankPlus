#include "ibas_in.h"

sig_atomic_t		g_EXIT_flag = 0 ;
sig_atomic_t		g_USR1_flag = 0 ;
sig_atomic_t		g_USR2_flag = 0 ;

static void sig_set_flag( int sig_no )
{
	if( sig_no == SIGTERM )
	{
		g_EXIT_flag = 1 ;
	}
	else if( sig_no == SIGUSR1 )
	{
		g_USR1_flag = 1 ;
	}
	else if( sig_no == SIGUSR2 )
	{
		g_USR2_flag = 1 ;
	}
	
	return;
}

static void DumpWorkerStack( pid_t pid )
{
	char		exe_filename[ IBP_MAXLEN_FILENAME + 1 ] ;
	char		pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	char		command[ 256 + 1 ] ;
	FILE		*fp = NULL ;
	char		buf[ 256 + 1 ] ;
	int		len ;
	
	int		nret = 0 ;
	
	memset( exe_filename , 0x00 , sizeof(exe_filename) );
	nret = readlink( "/proc/self/exe" , exe_filename , sizeof(exe_filename) ) ;
	if( nret == -1 )
	{
		WARNLOGSG( "readlink self exe failed[%d] , errno[%d]" , nret , errno )
		return;
	}
	
	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	snprintf( pathfilename , sizeof(pathfilename)-1 , "/tmp/core.%d" , pid );
	nret = access( pathfilename , R_OK ) ;
	if( nret )
	{
		WARNLOGSG( "corefile[%s] not found" , pathfilename )
		return;
	}
	
	memset( command , 0x00 , sizeof(command) );
	snprintf( command , sizeof(command)-1 , "gdb %s /tmp/core.%d -q -batch -ex \"where\" 2>/dev/null" , exe_filename , pid );
	fp = popen( command , "r" ) ;
	if( fp == NULL )
	{
		WARNLOGSG( "popen[%s] failed , errno[%d]" , command , errno )
		return;
	}
	
	while(1)
	{
		memset( buf , 0x00 , sizeof(buf) );
		if( fgets( buf , sizeof(buf)-1 , fp ) == NULL )
			break;
		len = strlen( buf ) ;
		if( buf[len-1] == '\n' )
			buf[len-1] = '\0' ;
		INFOLOGSG( "===%d=== %s" , pid , buf )
	}
	
	pclose( fp );
	
	return;
}

static void ReloadLog( struct IbasEnv *p_env , char *service_name )
{
	char		*file_content = NULL ;
	int		file_len ;
	ibas_conf	ibas_conf ;
	
	int		nret = 0 ;
	
	file_content = StrdupEntireFile( p_env->ibas_conf_pathfilename , & file_len ) ;
	if( file_content == NULL )
	{
		ERRORLOGSG( "StrdupEntireFile[%s] failed" , p_env->ibas_conf_pathfilename );
		return;
	}
	
	memset( & ibas_conf , 0x00 , sizeof(ibas_conf) );
	nret = DSCDESERIALIZE_JSON_ibas_conf( "GB18030" , file_content , & file_len , & ibas_conf ) ;
	free( file_content );
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibas_conf[%s] failed[%d]" , p_env->ibas_conf_pathfilename , nret );
		return;
	}
	
	strcpy( p_env->ibas_conf.ibas.log.iblog_server , ibas_conf.ibas.log.iblog_server );
	strcpy( p_env->ibas_conf.ibas.log.event_output , ibas_conf.ibas.log.event_output );
	strcpy( p_env->ibas_conf.ibas.log.monitor_loglevel , ibas_conf.ibas.log.monitor_loglevel );
	strcpy( p_env->ibas_conf.ibas.log.monitor_output , ibas_conf.ibas.log.monitor_output );
	
	INFOLOGSG( "InitLogEnv iblog_server[%s] event_output[%s] monitor_output[%s][%s]" , p_env->ibas_conf.ibas.log.iblog_server , p_env->ibas_conf.ibas.log.event_output , p_env->ibas_conf.ibas.log.monitor_loglevel , p_env->ibas_conf.ibas.log.monitor_output );
	IBPCleanLogEnv();
	IBPInitLogEnv( p_env->ibas_conf.ibas.log.iblog_server , service_name , p_env->ibas_conf.ibas.log.event_output , IBPConvertLogLevel(ibas_conf.ibas.log.monitor_loglevel) , "%s" , p_env->ibas_conf.ibas.log.monitor_output );
	INFOLOGSG( "InitLogEnv iblog_server[%s] event_output[%s] monitor_output[%s][%s]" , p_env->ibas_conf.ibas.log.iblog_server , p_env->ibas_conf.ibas.log.event_output , p_env->ibas_conf.ibas.log.monitor_loglevel , p_env->ibas_conf.ibas.log.monitor_output );
	
	return;
}

static struct IbasWorkerDetail *GetWorkerDetail( struct IbasEnv *p_env , pid_t pid )
{
	int			i ;
	
	for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
	{
		if( p_env->workers_space.worker_detail_array[i].status && p_env->workers_space.worker_detail_array[i].pid == pid )
			return p_env->workers_space.worker_detail_array+i;
	}
	
	return NULL;
}

static int CreateWorker( struct IbasEnv *p_env , struct IbasWorkerDetail *p_worker_detail )
{
	pid_t		pid ;
	
	int		nret = 0 ;
	
	p_worker_detail->index = p_worker_detail - p_env->workers_space.worker_detail_array + 1 ;
	
_RETRY_FORK :
	pid = fork() ;
	if( pid == -1 )
	{
		if( errno == EINTR )
			goto _RETRY_FORK;
		ERRORLOGSG( "fork failed , errno[%d]" , errno );
		return -1;
	}
	else if( pid == 0 )
	{
		INFOLOGSG( "child : [%ld] fork [%ld]" , getppid() , getpid() );
		
		p_worker_detail->status = IBAS_WORKERSTATUS_INITING ;
		p_env->p_worker_detail = p_worker_detail ;
		nret = ManagerWorker( p_env ) ;
		CleanEnvironment( p_env , 0 );
		exit(abs(nret));
	}
	else
	{
		p_worker_detail->pid = pid ;
		
		INFOLOGSG( "parent : [%ld] fork [%ld]" , getpid() , p_worker_detail->pid );
		
		NOTICELOGSG( "工作进程[%d][%d]开始" , p_worker_detail->index , p_worker_detail->pid );
	}
	
	p_env->worker_count++;
	
	return 0;
}

static void StatWorkerPool( struct IbasEnv *p_env )
{
	int		i ;
	
	p_env->workers_space.workers_stat->initing_count = 0 ;
	p_env->workers_space.workers_stat->idle_count = 0 ;
	p_env->workers_space.workers_stat->listening_count = 0 ;
	p_env->workers_space.workers_stat->working_count = 0 ;
	p_env->workers_space.workers_stat->keepalive_count = 0 ;
	p_env->workers_space.workers_stat->restarting_count = 0 ;
	p_env->workers_space.workers_stat->exiting_count = 0 ;
	
	for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
	{
		if( p_env->workers_space.worker_detail_array[i].status )
		{
			switch( p_env->workers_space.worker_detail_array[i].status )
			{
				case IBAS_WORKERSTATUS_INITING:
					p_env->workers_space.workers_stat->initing_count++;
					break;
				case IBAS_WORKERSTATUS_IDLE:
					p_env->workers_space.workers_stat->idle_count++;
					break;
				case IBAS_WORKERSTATUS_LISTENING:
					p_env->workers_space.workers_stat->listening_count++;
					break;
				case IBAS_WORKERSTATUS_WORKING:
					p_env->workers_space.workers_stat->working_count++;
					break;
				case IBAS_WORKERSTATUS_KEEPALIVE:
					p_env->workers_space.workers_stat->keepalive_count++;
					break;
				case IBAS_WORKERSTATUS_RESTARTING:
					p_env->workers_space.workers_stat->restarting_count++;
					break;
				case IBAS_WORKERSTATUS_EXITING:
					p_env->workers_space.workers_stat->exiting_count++;
					break;
				default :
					FATALLOGSG( "worker status[%s] invalid" , p_env->workers_space.worker_detail_array[i].status );
					return;
			}
			
			INFOLOGSG( "STATDETAIL[%d] - worker[%d][%d] cmd[%c] status[%c] process_count[%d] - node[%s] ip[%s] app[%s] begin_time[%s] timeout[%d]"
				, i
				, p_env->workers_space.worker_detail_array[i].index , p_env->workers_space.worker_detail_array[i].pid
				, p_env->workers_space.worker_detail_array[i].cmd , p_env->workers_space.worker_detail_array[i].status
				, p_env->workers_space.worker_detail_array[i].process_count
				, p_env->workers_space.worker_detail_array[i].processing_info.node
				, p_env->workers_space.worker_detail_array[i].processing_info.ip
				, p_env->workers_space.worker_detail_array[i].processing_info.app
				, p_env->workers_space.worker_detail_array[i].processing_info.timeval_stat.timestamp_str
				, p_env->workers_space.worker_detail_array[i].processing_info.timeout );
		}
	}
	if( p_env->worker_count > 0 )
		p_env->workers_space.workers_stat->load = p_env->workers_space.workers_stat->working_count * 100 / p_env->worker_count ;
	else
		p_env->workers_space.workers_stat->load = 100 ;
	
	INFOLOGSG( "STATTOTAL ------ total[%d] - initing[%d] idle[%d] listening[%d] working[%d] keepalive[%d] restarting[%d] exiting[%d] ------"
		, p_env->worker_count
		, p_env->workers_space.workers_stat->initing_count
		, p_env->workers_space.workers_stat->idle_count
		, p_env->workers_space.workers_stat->listening_count
		, p_env->workers_space.workers_stat->working_count
		, p_env->workers_space.workers_stat->keepalive_count
		, p_env->workers_space.workers_stat->restarting_count
		, p_env->workers_space.workers_stat->exiting_count );
	
	return;
}

static void PokeListeningWorker( struct IbasEnv *p_env )
{
	int		i ;
	int		poke_flag ;
	
	int		nret = 0 ;
	
	poke_flag = 0 ;
	for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
	{
		if( p_env->workers_space.worker_detail_array[i].status == IBAS_WORKERSTATUS_LISTENING )
		{
			poke_flag = 1 ;
			break;
		}
	}
	
	if( poke_flag == 1 )
	{
		DEBUGLOGSG( "poke listening worker[%d][%d]" , p_env->workers_space.worker_detail_array[i].index , p_env->workers_space.worker_detail_array[i].pid );
		nret = HttpRequest( & (p_env->accepted_session.netaddr) , p_env->accepted_session.http , 60 , "GET %s HTTP/1.0" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE , IBP_URI_NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "HttpRequest failed[%d]" , nret );
		}
	}
	
	return;
}

static int AdjustWorkerPool( struct IbasEnv *p_env )
{
	int		i ;
	
	int		nret = 0 ;
	
	if( p_env->mpm_mode == IBAS_MPMMODE_DYNAMIC_WORKERPOOL )
	{
		if( p_env->workers_space.workers_stat->idle_count < p_env->ibas_conf.ibas.mpm.DYNAMIC_WORKERPOOL.min_idle_count )
		{
			p_env->hope_to_create++;
			if( p_env->hope_to_create >= p_env->ibas_conf.ibas.mpm.DYNAMIC_WORKERPOOL.create_interval )
			{
				for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
				{
					if( p_env->workers_space.worker_detail_array[i].status == IBAS_WORKERSTATUS_NULL )
					{
						nret = CreateWorker( p_env , p_env->workers_space.worker_detail_array+i ) ;
						if( nret )
						{
							FATALLOGSG( "CreateWorker failed[%d]" , nret );
						}
						else
						{
							INFOLOGSG( "CreateWorker ok , index[%d]pid[%d]" , i , p_env->workers_space.worker_detail_array[i].pid );
						}
						
						break;
					}
				}
				
				p_env->hope_to_create = 0 ;
			}
		}
		else
		{
			p_env->hope_to_create = 0 ;
		}
		
		if( p_env->workers_space.workers_stat->idle_count > p_env->ibas_conf.ibas.mpm.DYNAMIC_WORKERPOOL.max_idle_count )
		{
			p_env->hope_to_destroy++;
			if( p_env->hope_to_destroy >= p_env->ibas_conf.ibas.mpm.DYNAMIC_WORKERPOOL.destroy_interval )
			{
				for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
				{
					if( p_env->workers_space.worker_detail_array[i].status == IBAS_WORKERSTATUS_LISTENING )
					{
						INFOLOGSG( "set worker[%d][%d] cmd [%c] -> [%c]" , i+1 , p_env->workers_space.worker_detail_array[i].pid , p_env->workers_space.worker_detail_array[i].status , IBAS_WORKERSTATUS_EXITING );
						p_env->workers_space.worker_detail_array[i].cmd = IBAS_WORKERCMD_EXITING ;
						
						PokeListeningWorker( p_env );
						
						break;
					}
				}
				
				p_env->hope_to_destroy = 0 ;
			}
		}
		else
		{
			p_env->hope_to_destroy = 0 ;
		}
	}
	else if( p_env->mpm_mode == IBAS_MPMMODE_STATIC_WORKERPOOL )
	{
		if( p_env->worker_count < p_env->ibas_conf.ibas.mpm.max_count )
		{
			for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
			{
				if( p_env->workers_space.worker_detail_array[i].status == IBAS_WORKERSTATUS_NULL )
				{
					nret = CreateWorker( p_env , p_env->workers_space.worker_detail_array+i ) ;
					if( nret )
					{
						FATALLOGSG( "CreateWorker failed[%d]" , nret );
					}
					else
					{
						INFOLOGSG( "CreateWorker ok , index[%d]pid[%d]" , i , p_env->workers_space.worker_detail_array[i].pid );
					}
					
					break;
				}
			}
		}
	}
	
	return 0;
}

static int MonitorWorkerTimeout( struct IbasEnv *p_env )
{
	time_t		tt ;
	time_t		dtt ;
	int		index ;
	
	int		nret = 0 ;
	
	tt = time(NULL) ;
	
	for( index = 0 ; index < p_env->ibas_conf.ibas.mpm.max_count ; index++ )
	{
		if( p_env->workers_space.worker_detail_array[index].status == IBAS_WORKERSTATUS_WORKING )
		{
			dtt = tt - p_env->workers_space.worker_detail_array[index].processing_info.timeval_stat.receiving_request.tv_sec ;
			if( dtt > p_env->workers_space.worker_detail_array[index].processing_info.timeout * 2 + p_env->workers_space.worker_detail_array[index].processing_info.timeout / 2 )
			{
				ERRORLOGSG( "worker[%d][%d] timeout([%d]-[%d]=[%d]) seriously , kill -KILL it" , index , p_env->workers_space.worker_detail_array[index].pid , tt , p_env->workers_space.worker_detail_array[index].processing_info.timeval_stat.receiving_request.tv_sec , dtt );
_RETRY_KILL1 :
				nret = kill( p_env->workers_space.worker_detail_array[index].pid , SIGKILL ) ;
				if( nret == -1 && errno == EINTR )
					goto _RETRY_KILL1;
			}
			else if( dtt > p_env->workers_space.worker_detail_array[index].processing_info.timeout * 2 )
			{
				ERRORLOGSG( "worker[%d][%d] timeout([%d]-[%d]=[%d]) , kill -SIGTERM it" , index , p_env->workers_space.worker_detail_array[index].pid , tt , p_env->workers_space.worker_detail_array[index].processing_info.timeval_stat.receiving_request.tv_sec , dtt );
_RETRY_KILL2 :
				nret = kill( p_env->workers_space.worker_detail_array[index].pid , SIGTERM ) ;
				if( nret == -1 && errno == EINTR )
					goto _RETRY_KILL2;
			}
		}
	}
	
	return 0;
}

static void SetAllWorkersCommand( struct IbasEnv *p_env , unsigned char cmd )
{
	int		i ;
	
	for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
	{
		if( p_env->workers_space.worker_detail_array[i].status )
		{
			p_env->workers_space.worker_detail_array[i].cmd = cmd ;
			INFOLOGSG( "set worker[%d][%d] exiting" , p_env->workers_space.worker_detail_array[i].index , p_env->workers_space.worker_detail_array[i].pid , IBAS_WORKERSTATUS_EXITING );
		}
	}
	
	PokeListeningWorker( p_env );
	
	return;
}

static int AlonerMonitor( struct IbasEnv *p_env )
{
	fd_set			readfds ;
	struct timeval		select_timeout ;
	socklen_t		addrlen ;
	
	pid_t			pid ;
	int			status ;
	struct IbasWorkerDetail	*p_worker_detail = NULL ;
	int			i ;
	struct timeval		waiting_for_accepting ;
	
	int			nret = 0 ;
	
	IBPCleanLogEnv();
	IBPInitLogEnv( p_env->ibas_conf.ibas.log.iblog_server , "ibas.aloner" , p_env->ibas_conf.ibas.log.event_output , IBPConvertLogLevel(p_env->ibas_conf.ibas.log.monitor_loglevel) , "%s" , p_env->ibas_conf.ibas.log.monitor_output );
	
	while(1)
	{
		nret = IBMACheckConfigSpaceAttaching( p_env->ibma_config_space ) ;
		if( nret < 0 )
		{
			FATALLOGSG( "IBMACheckConfigSpaceAttaching failed[%d]" , nret );
			return nret;
		}
		
		StatWorkerPool( p_env );
		
		if( g_EXIT_flag == 1 )
		{
			if( p_env->listen_session.netaddr.sock != -1 )
			{
				close( p_env->listen_session.netaddr.sock ); p_env->listen_session.netaddr.sock = -1 ;
			}
#if 0
			if( p_env->report_session.netaddr.sock != -1 )
			{
				close( p_env->report_session.netaddr.sock ); p_env->report_session.netaddr.sock = -1 ;
			}
#endif
			SetAllWorkersCommand( p_env , IBAS_WORKERCMD_EXITING );
		}
		
		if( g_USR1_flag == 1 )
		{
			g_USR1_flag = 0 ;
			
			SetAllWorkersCommand( p_env , IBAS_WORKERCMD_RELOAD_LOG );
			
			ReloadLog( p_env , "ibas.aloner" );
		}
		
		if( g_USR2_flag == 1 )
		{
			char	env_val[ 64 + 1 ] ;
			
			pid_t	pid ;
			
			memset( env_val , 0x00 , sizeof(env_val) );
			snprintf( env_val , sizeof(env_val)-1 , "%d" , p_env->listen_session.netaddr.sock );
			setenv( IBAS_LISTEN_SOCKFD , env_val , 1 );
			
			/* 产生下一辈进程 */
_RETRY_FORK2 :
			pid = fork() ;
			if( pid == -1 )
			{
				if( errno == EINTR )
					goto _RETRY_FORK2;
				;
			}
			else if( pid == 0 )
			{
				INFOLOGSG( "execvp ..." );
				execvp( "ibas" , p_env->argv );
				FATALLOGSG( "execvp failed , errno[%d]" , ERRNO );
				
				exit(9);
			}
			
			g_USR2_flag = 0 ;
		}
		
		if( g_EXIT_flag == 0 )
		{
			nret = MonitorWorkerTimeout( p_env ) ;
			if( nret )
			{
				FATALLOGSG( "MonitorWorkerTimeout failed[%d]" , nret );
				return -1;
			}
			
#if 0
			p_env->report_session.hope_to_report--;
			if( p_env->report_session.hope_to_report < 0 )
			{
				nret = OnProcessAliveReport( p_env ) ;
				if( nret != HTTP_OK )
				{
					ERRORLOGSG( "OnProcessAliveReport failed[%d]" , nret );
				}
				
				p_env->report_session.hope_to_report = 30 ;
			}
#endif
		}
		
		while(1)
		{
_RETRY_WAITPID :
			pid = waitpid( -1 , & status , WNOHANG );
			if( pid == -1 )
			{
				if( errno == EINTR )
					goto _RETRY_WAITPID;
				if( errno == ECHILD )
					break;
				ERRORLOGSG( "waitpid failed , errno[%d]" , errno );
				return -1;
			}
			else if( pid > 0 )
			{
				int		worker_detail_index ;
				int		worker_detail_status ;
				
				if( WIFSIGNALED(status) || WTERMSIG(status) )
				{
					ERRORLOGSG( "waitpid[%d] WEXITSTATUS[%d] WIFSIGNALED[%d] WTERMSIG[%d]" , pid , WEXITSTATUS(status) , WIFSIGNALED(status) , WTERMSIG(status) );
					DumpWorkerStack( pid );
				}
				else if( WEXITSTATUS(status) )
				{
					WARNLOGSG( "waitpid[%d] WEXITSTATUS[%d] WIFSIGNALED[%d] WTERMSIG[%d]" , pid , WEXITSTATUS(status) , WIFSIGNALED(status) , WTERMSIG(status) );
				}
				else
				{
					INFOLOGSG( "waitpid[%d] WEXITSTATUS[%d] WIFSIGNALED[%d] WTERMSIG[%d]" , pid , WEXITSTATUS(status) , WIFSIGNALED(status) , WTERMSIG(status) );
				}
				
				p_worker_detail = GetWorkerDetail( p_env , pid ) ;
				if( p_worker_detail == NULL )
				{
					FATALLOGSG( "未知工作进程[%d]结束" , pid );
					continue;
				}
				if( WIFSIGNALED(status) || WTERMSIG(status) )
				{
					ERRORLOGSG( "工作进程[%d][%d]异常结束 , app[%s] timestamp_str[%s]" , p_worker_detail->index , pid , p_worker_detail->processing_info.app , p_worker_detail->processing_info.timeval_stat.timestamp_str );
				}
				
				worker_detail_index = p_worker_detail->index ;
				worker_detail_status = p_worker_detail->status ;
				memset( p_worker_detail , 0x00 , sizeof(struct IbasWorkerDetail) );
				p_env->worker_count--;
				
				INFOLOGSG( "RECYCLING worker[%d]" , worker_detail_index );
				continue;
			}
			else
			{
				break;
			}
		}
		
		select_timeout.tv_sec = 1 ;
		select_timeout.tv_usec = 0 ;
		while(1)
		{
_RETRY_SELECT :
			FD_ZERO( & readfds );
			FD_SET( p_env->listen_session.netaddr.sock , & readfds );
			nret = select( p_env->listen_session.netaddr.sock+1 , & readfds , NULL , NULL , & select_timeout ) ;
			if( nret == -1 )
			{
				if( errno == EINTR )
					goto _RETRY_SELECT;
				FATALLOGSG( "select failed , errno[%d]" , errno );
				return -1;
			}
			else if( nret == 0 )
			{
				break;
			}
			
_RETRY_ACCEPT :
			gettimeofday( & waiting_for_accepting , NULL );
			
			addrlen = sizeof(struct sockaddr) ;
			p_env->accepted_session.netaddr.sock = accept( p_env->listen_session.netaddr.sock , (struct sockaddr *) & (p_env->accepted_session.netaddr.addr) , & addrlen ) ;
			if( p_env->accepted_session.netaddr.sock == -1 )
			{
				if( errno == EINTR )
					goto _RETRY_ACCEPT;
				FATALLOGSG( "accept failed , errno[%d]" , errno );
				DEBUGLOGSG( "worker status [%c]->[%c]" , p_env->p_worker_detail->status , IBAS_WORKERSTATUS_EXITING );
				return -1;
			}
			else
			{
				DEBUGLOGSG( "accept ok , sock[%d]" , p_env->accepted_session.netaddr.sock );
				GETNETADDRESS( p_env->accepted_session.netaddr );
			}
			
			SetHttpNodelay( p_env->accepted_session.netaddr.sock , 1 );
			
			if( p_env->worker_count+1 > p_env->ibas_conf.ibas.mpm.max_count )
			{
				ERRORLOGSG( "too many workers , max_count[%d]" , p_env->ibas_conf.ibas.mpm.max_count );
				close( p_env->accepted_session.netaddr.sock );
				continue;
			}
			else
			{
				INFOLOGSG( "worker_count[%d] max_count[%d]" , p_env->worker_count , p_env->ibas_conf.ibas.mpm.max_count );
			}
			
			for( i = 0 ; i < p_env->ibas_conf.ibas.mpm.max_count ; i++ )
			{
				if( p_env->workers_space.worker_detail_array[i].status == IBAS_WORKERSTATUS_NULL )
				{
					p_worker_detail = p_env->workers_space.worker_detail_array + i ; ;
					
					p_worker_detail->index = i + 1 ;
					
_RETRY_FORK :
					memcpy( & (p_worker_detail->processing_info.timeval_stat.waiting_for_accepting) , & waiting_for_accepting , sizeof(struct timeval) );
					p_worker_detail->pid = fork() ;
					if( p_worker_detail->pid == -1 )
					{
						if( errno == EINTR )
							goto _RETRY_FORK;
						ERRORLOGSG( "fork failed , errno[%d]" , errno );
						break;
					}
					else if( p_worker_detail->pid == 0 )
					{
						INFOLOGSG( "child : [%ld] fork [%ld]" , getppid() , getpid() );
						
						p_worker_detail->status = IBAS_WORKERSTATUS_INITING ;
						p_env->p_worker_detail = p_worker_detail ;
						nret = AlonerWorker( p_env ) ;
						CleanEnvironment( p_env , 0 );
						exit(abs(nret));
					}
					else
					{
						INFOLOGSG( "parent : [%ld] fork [%ld]" , getpid() , p_worker_detail->pid );
					}
					
					p_env->worker_count++;
					break;
				}
			}
		}
	}
	
	return 0;
}

static int ManagerMonitor( struct IbasEnv *p_env )
{
	pid_t			pid ;
	int			status ;
	struct IbasWorkerDetail	*p_worker_detail = NULL ;
	
	int			nret = 0 ;
	
	IBPCleanLogEnv();
	IBPInitLogEnv( p_env->ibas_conf.ibas.log.iblog_server , "ibas.manger" , p_env->ibas_conf.ibas.log.event_output , IBPConvertLogLevel(p_env->ibas_conf.ibas.log.monitor_loglevel) , "%s" , p_env->ibas_conf.ibas.log.monitor_output );
	
	nret = CreateWorker( p_env , p_env->workers_space.worker_detail_array ) ;
	if( nret )
	{
		FATALLOGSG( "CreateWorker failed[%d]" , nret );
		close( p_env->listen_session.netaddr.sock );
		return -1;
	}
	else
	{
		INFOLOGSG( "CreateWorker ok , index[%d]pid[%d]" , 0 , p_env->workers_space.worker_detail_array[0].pid );
	}
	
#if 0
	p_env->report_session.hope_to_report = 30 ;
#endif
	while( g_EXIT_flag == 0 || p_env->worker_count > 0 )
	{
		sleep( 1 );
		
		nret = IBMACheckConfigSpaceAttaching( p_env->ibma_config_space ) ;
		if( nret < 0 )
		{
			FATALLOGSG( "IBMACheckConfigSpaceAttaching failed[%d]" , nret );
			return nret;
		}
		
		StatWorkerPool( p_env );
		
		if( g_EXIT_flag == 1 )
		{
			if( p_env->listen_session.netaddr.sock != -1 )
			{
				close( p_env->listen_session.netaddr.sock ); p_env->listen_session.netaddr.sock = -1 ;
			}
#if 0
			if( p_env->report_session.netaddr.sock != -1 )
			{
				close( p_env->report_session.netaddr.sock ); p_env->report_session.netaddr.sock = -1 ;
			}
#endif
			SetAllWorkersCommand( p_env , IBAS_WORKERCMD_EXITING );
		}
		
		if( g_USR1_flag == 1 )
		{
			g_USR1_flag = 0 ;
			
			SetAllWorkersCommand( p_env , IBAS_WORKERCMD_RELOAD_LOG );
			
			ReloadLog( p_env , "ibas.aloner" );
		}
		
		if( g_USR2_flag == 1 )
		{
			char	env_val[ 64 + 1 ] ;
			
			pid_t	pid ;
			
			memset( env_val , 0x00 , sizeof(env_val) );
			snprintf( env_val , sizeof(env_val)-1 , "%d" , p_env->listen_session.netaddr.sock );
			setenv( IBAS_LISTEN_SOCKFD , env_val , 1 );
			
			/* 产生下一辈进程 */
_RETRY_FORK :
			pid = fork() ;
			if( pid == -1 )
			{
				if( errno == EINTR )
					goto _RETRY_FORK;
				;
			}
			else if( pid == 0 )
			{
				INFOLOGSG( "execvp ..." );
				execvp( "ibas" , p_env->argv );
				FATALLOGSG( "execvp failed , errno[%d]" , ERRNO );
				
				exit(9);
			}
			
			g_USR2_flag = 0 ;
		}
		
		if( g_EXIT_flag == 0 )
		{
			nret = AdjustWorkerPool( p_env ) ;
			if( nret )
			{
				FATALLOGSG( "AdjustWorkerPool failed[%d]" , nret );
				return -1;
			}
			
			nret = MonitorWorkerTimeout( p_env ) ;
			if( nret )
			{
				FATALLOGSG( "MonitorWorkerTimeout failed[%d]" , nret );
				return -1;
			}
			
#if 0
			p_env->report_session.hope_to_report--;
			if( p_env->report_session.hope_to_report < 0 )
			{
				nret = OnProcessAliveReport( p_env ) ;
				if( nret != HTTP_OK )
				{
					ERRORLOGSG( "OnProcessAliveReport failed[%d]" , nret );
				}
				
				p_env->report_session.hope_to_report = 30 ;
			}
#endif
		}
		
		while(1)
		{
_RETRY_WAITPID :
			pid = waitpid( -1 , & status , WNOHANG );
			if( pid == -1 )
			{
				if( errno == EINTR )
					goto _RETRY_WAITPID;
				if( errno == ECHILD )
					break;
				ERRORLOGSG( "waitpid failed , errno[%d]" , errno );
				return -1;
			}
			else if( pid > 0 )
			{
				int		worker_detail_index ;
				int		worker_detail_status ;
				
				if( WIFSIGNALED(status) || WTERMSIG(status) )
				{
					ERRORLOGSG( "waitpid[%d] WEXITSTATUS[%d] WIFSIGNALED[%d] WTERMSIG[%d]" , pid , WEXITSTATUS(status) , WIFSIGNALED(status) , WTERMSIG(status) );
					DumpWorkerStack( pid );
				}
				else if( WEXITSTATUS(status) )
				{
					WARNLOGSG( "waitpid[%d] WEXITSTATUS[%d] WIFSIGNALED[%d] WTERMSIG[%d]" , pid , WEXITSTATUS(status) , WIFSIGNALED(status) , WTERMSIG(status) );
				}
				else
				{
					INFOLOGSG( "waitpid[%d] WEXITSTATUS[%d] WIFSIGNALED[%d] WTERMSIG[%d]" , pid , WEXITSTATUS(status) , WIFSIGNALED(status) , WTERMSIG(status) );
				}
				
				p_worker_detail = GetWorkerDetail( p_env , pid ) ;
				if( p_worker_detail == NULL )
				{
					FATALLOGSG( "未知工作进程[%d]结束" , pid );
					continue;
				}
				if( WIFSIGNALED(status) || WTERMSIG(status) )
				{
					ERRORLOGSG( "工作进程[%d][%d]异常结束 , app[%s] timestamp_str[%s]" , p_worker_detail->index , pid , p_worker_detail->processing_info.app , p_worker_detail->processing_info.timeval_stat.timestamp_str );
				}
				else
				{
					NOTICELOGSG( "工作进程[%d][%d]结束" , p_worker_detail->index , pid );
				}
				
				worker_detail_index = p_worker_detail->index ;
				worker_detail_status = p_worker_detail->status ;
				memset( p_worker_detail , 0x00 , sizeof(struct IbasWorkerDetail) );
				p_env->worker_count--;
				
				if( g_EXIT_flag )
				{
					INFOLOGSG( "RECYCLING worker[%d]" , worker_detail_index );
					continue;
				}
				
				if( worker_detail_status != IBAS_WORKERSTATUS_RESTARTING )
				{
					if( p_env->mpm_mode == IBAS_MPMMODE_DYNAMIC_WORKERPOOL || p_env->mpm_mode == IBAS_MPMMODE_STATIC_WORKERPOOL )
					{
						nret = AdjustWorkerPool( p_env ) ;
						if( nret )
						{
							FATALLOGSG( "AdjustWorkerPool failed[%d]" , nret );
							return -1;
						}
						
						continue;
					}
					else
					{
						INFOLOGSG( "RECYCLING worker[%d]" , worker_detail_index );
						continue;
					}
				}
				
				INFOLOGSG( "RESTARTING worker[%d]" , worker_detail_index );
				
				nret = CreateWorker( p_env , p_worker_detail ) ;
				if( nret )
				{
					FATALLOGSG( "ReCreateWorker failed[%d]" , nret );
					return -1;
				}
				else
				{
					INFOLOGSG( "ReCreateWorker ok , index[%d]pid[%d]" , p_worker_detail->index , p_worker_detail->pid );
				}
				
				continue;
			}
			else
			{
				break;
			}
		}
	}
	
	INFOLOGSG( "all worker exited" );
	
	INFOLOGSG( "CleanEnvironment" );
	CleanEnvironment( p_env , 1 );
	
	return 0;
}

int _monitor( void *pv )
{
	struct IbasEnv		*p_env = (struct IbasEnv *)pv ;
	
	char			*env_val = NULL ;
	
	struct sigaction	act ;
	
	int			nret = 0 ;
	
	
	env_val = getenv( IBAS_LISTEN_SOCKFD ) ;
	if( env_val == NULL )
	{
		p_env->listen_session.netaddr.sock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ;
		if( p_env->listen_session.netaddr.sock == -1 )
		{
			ERRORLOGSG( "socket failed , errno[%d]" , errno );
			return -1;
		}
		else
		{
			INFOLOGSG( "socket ok[%d]" , p_env->listen_session.netaddr.sock );
		}
		
		SetHttpReuseAddr( p_env->listen_session.netaddr.sock );
		SetHttpNodelay( p_env->listen_session.netaddr.sock , 1 );
		
		strncpy( p_env->listen_session.netaddr.ip , p_env->ibas_conf.ibas.server.ip , sizeof(p_env->listen_session.netaddr.ip)-1 );
		p_env->listen_session.netaddr.port = p_env->ibas_conf.ibas.server.port ;
		SETNETADDRESS( p_env->listen_session.netaddr )
		nret = bind( p_env->listen_session.netaddr.sock , (struct sockaddr *) & (p_env->listen_session.netaddr.addr) , sizeof(struct sockaddr) ) ;
		if( nret == -1 )
		{
			ERRORLOGSG( "bind[%s:%d][%d] failed , errno[%d]" , p_env->listen_session.netaddr.ip , p_env->listen_session.netaddr.port , p_env->listen_session.netaddr.sock , errno );
			close( p_env->listen_session.netaddr.sock );
			return -1;
		}
		else
		{
			INFOLOGSG( "bind[%s:%d][%d] ok" , p_env->listen_session.netaddr.ip , p_env->listen_session.netaddr.port , p_env->listen_session.netaddr.sock );
		}
		
		nret = listen( p_env->listen_session.netaddr.sock , 10240 ) ;
		if( nret == -1 )
		{
			ERRORLOGSG( "listen[%s:%d][%d] failed , errno[%d]" , p_env->listen_session.netaddr.ip , p_env->listen_session.netaddr.port , p_env->listen_session.netaddr.sock , errno );
			close( p_env->listen_session.netaddr.sock );
			return -1;
		}
		else
		{
			INFOLOGSG( "listen[%s:%d][%d] ok" , p_env->listen_session.netaddr.ip , p_env->listen_session.netaddr.port , p_env->listen_session.netaddr.sock );
		}
	}
	else
	{
		p_env->listen_session.netaddr.sock = atoi(env_val) ;
		INFOLOGSG( "reuse[%d]" , p_env->listen_session.netaddr.sock );
	}
	
	signal( SIGCLD , SIG_DFL );
	signal( SIGCHLD , SIG_DFL );
	signal( SIGPIPE , SIG_IGN );
	act.sa_handler = & sig_set_flag ;
	sigemptyset( & (act.sa_mask) );
	act.sa_flags = 0 ;
	sigaction( SIGTERM , & act , NULL );
	sigaction( SIGUSR2 , & act , NULL );
	act.sa_flags = SA_RESTART ;
	sigaction( SIGUSR1 , & act , NULL );
	
	if( p_env->mpm_mode == IBAS_MPMMODE_FORK )
	{
		INFOLOGSG( "--- ibas.AlonerMonitor begin --- " );
		nret = AlonerMonitor( p_env ) ;
		INFOLOGSG( "--- ibas.AlonerMonitor end --- " );
	}
	else if( p_env->mpm_mode == IBAS_MPMMODE_STATIC_WORKERPOOL || p_env->mpm_mode == IBAS_MPMMODE_DYNAMIC_WORKERPOOL )
	{
		INFOLOGSG( "--- ibas.ManagerMonitor begin --- " );
		nret = ManagerMonitor( p_env ) ;
		INFOLOGSG( "--- ibas.ManagerMonitor end --- " );
	}
	
	exit(0);
}

