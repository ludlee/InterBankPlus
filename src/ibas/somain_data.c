#include "ibas_in.h"
#include "ibas_api.h"

int BeforeCallSomain( struct IbasEnv *p_env )
{
	char			*msg = NULL ;
	int			msg_len ;
	
	char			HTTPHEADER_name[ sizeof(IBP_HTTPHEADER_IBP_FILE_NAME)+2 ] ;
	int			i ;
	char			*HTTPHEADER_Ibp_File_Name = NULL ;
	int			HTTPHEADERLEN_Ibp_File_Name ;
	struct IbasAddonFiles	*p_addon_files = NULL ;
	
	int			nret = 0 ;
	
	nret = ExtractMessageFromHttpBuffer( p_env->accepted_session.http , GetHttpRequestBuffer(p_env->accepted_session.http) , p_env->accepted_session.http_secure_env , & msg , & msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed , errno[%d]" , errno );
		return HTTP_BAD_REQUEST;
	}
	ResetHttpBuffer( p_env->accepted_session.req_body );
	nret = MemcatHttpBuffer( p_env->accepted_session.req_body , msg , msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "MemcatHttpBuffer failed , errno[%d]" , errno );
		return IBP_IBAS_ERROR_A_S(nret);
	}
	DEBUGHEXLOGSG( msg , msg_len , "RequestBuffer(decrypt) [%d]bytes" , msg_len );
	
	ResetHttpBuffer( p_env->accepted_session.rsp_body );
	
	p_addon_files = & (p_env->p_worker_detail->processing_info.addon_files) ;
	for( i = 0 ; i < IBP_MAXCNT_REQUEST_FILES ; i++ )
	{
		sprintf( HTTPHEADER_name , "%s-%d" , IBP_HTTPHEADER_IBP_FILE_NAME , i+1 );
		HTTPHEADER_Ibp_File_Name = QueryHttpHeaderPtr( p_env->accepted_session.http , HTTPHEADER_name , & HTTPHEADERLEN_Ibp_File_Name ) ;
		if( HTTPHEADER_Ibp_File_Name == NULL )
			break;
		snprintf( p_addon_files->req_filename[i] , sizeof(p_addon_files->req_filename[i])-1 , "%.*s" , HTTPHEADERLEN_Ibp_File_Name , HTTPHEADER_Ibp_File_Name );
		INFOLOGSG( "REQFILE[%s]" , p_addon_files->req_filename[i] );
	}
	p_addon_files->req_filecount = i ;
	
	return 0;
}

int AfterCallSomain( struct IbasEnv *p_env )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	
	struct IbasAddonFiles	*p_addon_files = NULL ;
	int			i ;
	
	char			*msg = NULL ;
	int			msg_len ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_env->accepted_session.http ) ;
	
	p_addon_files = & (p_env->p_worker_detail->processing_info.addon_files) ;
	for( i = 0 ; i < IBP_MAXCNT_RESPONSE_FILES ; i++ )
	{
		if( p_addon_files->rsp_filename[i][0] == '\0' )
			break;
		
		nret = StrcatfHttpBuffer( rsp_buf , "%s-%d: %s" HTTP_RETURN_NEWLINE
						, IBP_HTTPHEADER_IBP_FILE_NAME , i+1 , p_addon_files->rsp_filename[i] );
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed , errno[%d]" , errno );
			return IBP_IBAS_ERROR_A_S(nret);
		}
		INFOLOGSG( "RSPFILE[%s]" , p_addon_files->rsp_filename[i] );
	}
	
	msg = GetHttpBufferBase( p_env->accepted_session.rsp_body , & msg_len ) ;
	if( msg_len > 0 )
	{
		nret = PutMessageToHttpBuffer( p_env->accepted_session.http , rsp_buf , p_env->accepted_session.http_secure_env , msg , msg_len ) ;
		if( nret )
		{
			ERRORLOGSG( "PutMessageToHttpBuffer failed , errno[%d]" , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	else
	{
		nret = StrcatfHttpBuffer( rsp_buf , HTTP_RETURN_NEWLINE ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed , errno[%d]" , errno );
			return IBP_IBAS_ERROR_A_S(nret);
		}
	}
	
	return 0;
}

