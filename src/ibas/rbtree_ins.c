#include "ibas_in.h"

#include "rbtree_tpl.h"

LINK_RBTREENODE_STRING( LinkSoBinTreeNode , struct IbasEnv , so_bin_rbtree , struct SoFile , bin_this_rbnode , bin )
QUERY_RBTREENODE_STRING( QuerySoBinTreeNode , struct IbasEnv , so_bin_rbtree , struct SoFile , bin_this_rbnode , bin )
UNLINK_RBTREENODE( UnlinkSoBinTreeNode , struct IbasEnv , so_bin_rbtree , struct SoFile , bin_this_rbnode )

LINK_RBTREENODE_INT( LinkSoCallCountTreeNode , struct IbasEnv , so_call_count_rbtree , struct SoFile , call_count_this_rbnode , call_count )
UPDATE_RBTREENODE( UpdateSoCallCountTreeNode , UnlinkSoCallCountTreeNode , LinkSoCallCountTreeNode , struct IbasEnv , struct SoFile )
UNLINK_RBTREENODE( UnlinkSoCallCountTreeNode , struct IbasEnv , so_call_count_rbtree , struct SoFile , call_count_this_rbnode )
void UnlinkMinSoCallCountTreeNode( struct IbasEnv *p_env )
{
	struct rb_node		*p_curr = NULL ;
	struct SoFile		*p = NULL ;
	
	p_curr = rb_first( & (p_env->so_call_count_rbtree) ); 
	if (p_curr == NULL)
		return;
	
	p = container_of( p_curr , struct SoFile , call_count_this_rbnode ) ;
	
	UnlinkSoCallCountTreeNode( p_env , p );
	
	return;
}

LINK_RBTREENODE_INT( LinkSoFileWdTreeNode , struct IbasEnv , so_file_wd_rbtree , struct SoFile , file_wd_this_rbnode , file_wd )
QUERY_RBTREENODE_INT( QuerySoFileWdTreeNode , struct IbasEnv , so_file_wd_rbtree , struct SoFile , file_wd_this_rbnode , file_wd )
UNLINK_RBTREENODE( UnlinkSoFileWdTreeNode , struct IbasEnv , so_file_wd_rbtree , struct SoFile , file_wd_this_rbnode )

DESTROY_RBTREE( DestroySoFileTree , struct IbasEnv , so_bin_rbtree , struct SoFile , bin_this_rbnode , FREE_RBTREENODEENTRY_DIRECTLY )

