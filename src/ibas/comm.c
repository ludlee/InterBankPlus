#include "ibas_in.h"

int OnReceiveRequest( struct IbasEnv *p_env )
{
	struct HttpBuffer	*req_buf = NULL ;
	char			*msg = NULL ;
	int			msg_len ;
	
	int			nret = 0 ;
	
	nret = ReceiveHttpRequest( p_env->accepted_session.netaddr.sock , NULL , p_env->accepted_session.http ) ;
	if( nret > 0 )
	{
		INFOLOGSG( "ReceiveHttpRequest return[%d]" , nret );
		return nret;
	}
	else if( nret < 0 )
	{
		ERRORLOGSG( "ReceiveHttpRequest failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "ReceiveHttpRequest ok" );
	}
	
	req_buf = GetHttpRequestBuffer( p_env->accepted_session.http ) ;
	msg = GetHttpBufferBase( req_buf , & msg_len ) ;
	DEBUGHEXLOGSG( msg , msg_len , "RequestBuffer [%d]bytes" , msg_len );
	
	return 0;
}

int OnSendResponse( struct IbasEnv *p_env )
{
	struct HttpBuffer	*rsp_buf = NULL ;
	char			*msg = NULL ;
	int			msg_len ;
	
	int			nret = 0 ;
	
	rsp_buf = GetHttpResponseBuffer( p_env->accepted_session.http ) ;
	msg = GetHttpBufferBase( rsp_buf , & msg_len ) ;
	DEBUGHEXLOGSG( msg , msg_len , "ResponseBuffer [%d]bytes" , msg_len );
	
	nret = SendHttpResponse( p_env->accepted_session.netaddr.sock , NULL , p_env->accepted_session.http ) ;
	if( nret > 0 )
	{
		INFOLOGSG( "SendHttpResponse return[%d]" , nret );
		return nret;
	}
	else if( nret < 0 )
	{
		ERRORLOGSG( "SendHttpResponse failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "SendHttpResponse ok" );
	}
	
	p_env->accepted_session.response_flag = 1 ;
	
	return 0;
}

