#include "ibas_in.h"

int InitEnvironment( struct IbasEnv *p_env )
{
	struct HostSpaceUnit	*p_host_unit = NULL ;
	
	union semun		semopts ;
	
	int			nret = 0 ;
	
	SSL_load_error_strings();
	
	p_env->ibma_config_space = IBMAOpenConfigSpace( p_env->ibas_conf.ibma.config_filename ) ;
	if( p_env->ibma_config_space == NULL )
	{
		ERRORLOGSG( "IBMAOpenConfigSpace[%s] failed , errno[%d]" , p_env->ibas_conf.ibma.config_filename , errno );
		return -1;
	}
	
	strcpy( p_env->node , IBMAGetThisNodePtr(p_env->ibma_config_space) );
	INFOLOGSG( "IBMAGetThisNodePtr [%s]" , p_env->node );
	
	p_host_unit = IBMAQueryNodeHost( p_env->ibma_config_space , IBMAGetThisNode(p_env->ibma_config_space) , p_env->ibas_conf.ibas.server.ip , p_env->ibas_conf.ibas.server.port ) ;
	if( p_host_unit == NULL )
	{
		ERRORLOGSG( "IBMAQueryNodeHost[%s:%d] failed , errno[%d]" , p_env->ibas_conf.ibas.server.ip , p_env->ibas_conf.ibas.server.port , errno );
		return -1;
	}
	
#if 0
	memset( & (p_env->report_session) , 0x00 , sizeof(struct ReportSession) );
	strcpy( p_env->report_session.netaddr.ip , p_env->ibas_conf.ibms.server.ip );
	p_env->report_session.netaddr.port = p_env->ibas_conf.ibms.server.port ;
	p_env->report_session.netaddr.sock = -1 ;
	
	p_env->report_session.http = CreateHttpEnv() ;
	if( p_env->report_session.http == NULL )
	{
		ERRORLOGSG( "CreateHttpEnv failed , errno[%d]" , errno );
		return -1;
	}
#endif
	
	memset( & (p_env->accepted_session) , 0x00 , sizeof(struct AcceptedSession) );
	strcpy( p_env->accepted_session.netaddr.ip , p_env->ibas_conf.ibas.server.ip );
	p_env->accepted_session.netaddr.port = p_env->ibas_conf.ibas.server.port ;
	p_env->accepted_session.netaddr.sock = -1 ;
	
	p_env->accepted_session.http = CreateHttpEnv() ;
	if( p_env->accepted_session.http == NULL )
	{
		ERRORLOGSG( "CreateHttpEnv failed , errno[%d]" , errno );
		return -1;
	}
	
	p_env->accepted_session.http_secure_env = CreateHttpSecureEnv( IBP_HTTPSECURE_SIGN_FLAG_MD5 , IBP_HTTPSECURE_COMPRESS_FLAG_GZIP , IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA ) ;
	if( p_env->accepted_session.http_secure_env == NULL )
	{
		ERRORLOGSG( "CreateHttpSecureEnv failed , errno[%d]" , errno );
		return -1;
	}
	
	p_env->accepted_session.req_body = AllocHttpBuffer( FASTERHTTP_REQUEST_BUFSIZE_DEFAULT ) ;
	if( p_env->accepted_session.req_body == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed , errno[%d]" , errno );
		return -1;
	}
	
	p_env->accepted_session.rsp_body = AllocHttpBuffer( FASTERHTTP_RESPONSE_BUFSIZE_DEFAULT ) ;
	if( p_env->accepted_session.rsp_body == NULL )
	{
		ERRORLOGSG( "AllocHttpBuffer failed , errno[%d]" , errno );
		return -1;
	}
	
	p_env->workers_space.shm.proj_id = 0 ;
	p_env->workers_space.shm.shmkey = IPC_PRIVATE ;
	
	p_env->workers_space.shm.size = sizeof(struct IbasWorkerStat) + sizeof(struct IbasWorkerDetail) * p_env->ibas_conf.ibas.mpm.max_count ;
	p_env->workers_space.shm.shmid = shmget( p_env->workers_space.shm.shmkey , p_env->workers_space.shm.size , IPC_CREAT|0644 ) ;
	if( p_env->workers_space.shm.shmid == -1 )
	{
		ERRORLOGSG( "shmget[0x%X] failed , errno[%d]" , p_env->workers_space.shm.shmkey , errno );
		return -1;
	}
	else
	{
		INFOLOGSG( "shmget[0x%X] ok , shmid[%d]" , p_env->workers_space.shm.shmkey , p_env->workers_space.shm.shmid );
	}
	
	p_env->workers_space.shm.base = shmat( p_env->workers_space.shm.shmid , NULL , 0 ) ;
	if( p_env->workers_space.shm.base == NULL )
	{
		ERRORLOGSG( "shmat[%d] failed , errno[%d]" , p_env->workers_space.shm.shmid , errno );
		return -1;
	}
	else
	{
		INFOLOGSG( "shmat[%d] ok" , p_env->workers_space.shm.shmid );
	}
	
	p_env->workers_space.workers_stat = (struct IbasWorkerStat *)(p_env->workers_space.shm.base) ;
	p_env->workers_space.worker_detail_array = (struct IbasWorkerDetail *)((char*)(p_env->workers_space.workers_stat)+sizeof(struct IbasWorkerStat)) ;
	
	p_env->accept_mutex.sem.proj_id = 0 ;
	p_env->accept_mutex.sem.semkey = IPC_PRIVATE ;
	
	p_env->accept_mutex.sem.semid = semget( p_env->accept_mutex.sem.semkey , 1 , IPC_CREAT|0644 ) ;
	if( p_env->accept_mutex.sem.semid == -1 )
	{
		ERRORLOGSG( "semget[0x%X] failed , errno[%d]" , p_env->accept_mutex.sem.semkey , errno );
		return -1;
	}
	else
	{
		INFOLOGSG( "semget[0x%X] ok , semid[%d]" , p_env->accept_mutex.sem.semkey , p_env->accept_mutex.sem.semid );
	}
	
	memset( & semopts , 0x00 , sizeof(union semun) );
	semopts.val = 1 ;
	nret = semctl( p_env->accept_mutex.sem.semid , 0 , SETVAL , semopts ) ;
	if( nret == -1 )
	{
		ERRORLOGSG( "semctl[0x%X][%ld] SETVAL failed , errno[%d]" , p_env->accept_mutex.sem.semkey , p_env->accept_mutex.sem.semid , errno );
		return -1;
	}
	else
	{
		INFOLOGSG( "semctl[0x%X][%ld] SETVAL ok" , p_env->accept_mutex.sem.semkey , p_env->accept_mutex.sem.semid );
	}
	
	return 0;
}

void CleanEnvironment( struct IbasEnv *p_env , int rm_ipc_flag )
{
	IBMACloseConfigSpace( p_env->ibma_config_space );
	
#if 0
	DestroyHttpEnv( p_env->report_session.http );
#endif
	
	DestroyHttpSecureEnv( p_env->accepted_session.http_secure_env );
	DestroyHttpEnv( p_env->accepted_session.http );
	FreeHttpBuffer( p_env->accepted_session.req_body );
	FreeHttpBuffer( p_env->accepted_session.rsp_body );
	
	INFOLOGSG( "shmdt[0x%X][%ld] ok" , p_env->workers_space.shm.shmkey , p_env->workers_space.shm.shmid );
	shmdt( p_env->workers_space.shm.base );
	
	if( rm_ipc_flag == 1 )
	{
		INFOLOGSG( "shmctl[0x%X][%ld] IPC_RMID ok" , p_env->workers_space.shm.shmkey , p_env->workers_space.shm.shmid );
		shmctl( p_env->workers_space.shm.shmid , IPC_RMID , NULL );
	}
	
	if( rm_ipc_flag == 1 )
	{
		INFOLOGSG( "semctl[0x%X][%ld] IPC_RMID ok" , p_env->accept_mutex.sem.semkey , p_env->accept_mutex.sem.semid );
		semctl( p_env->accept_mutex.sem.semid , 0 , IPC_RMID , NULL );
	}
	
	return;
}

