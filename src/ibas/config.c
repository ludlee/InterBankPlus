#include "ibas_in.h"

#define PREFIX_DSCLOG_ibas_conf			INFOLOGSG(
#define NEWLINE_DSCLOG_ibas_conf	
#include "IDL_ibas_conf.dsc.LOG.c"

int LoadConfig( char *config_pathfilename , struct IbasEnv *p_env )
{
	char		*file_content = NULL ;
	int		file_len ;
	
	int		nret = 0 ;
	
	file_content = StrdupEntireFile( config_pathfilename , & file_len ) ;
	if( file_content == NULL )
	{
		ERRORLOGSG( "StrdupEntireFile[%s] failed" , config_pathfilename );
		return -1;
	}
	
	nret = DSCDESERIALIZE_JSON_ibas_conf( "GB18030" , file_content , & file_len , & (p_env->ibas_conf) ) ;
	free( file_content );
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibas_conf[%s] failed[%d]" , config_pathfilename , nret );
		return -1;
	}
	
	DSCLOG_ibas_conf( & (p_env->ibas_conf) );
	
	if( STRCMP( p_env->ibas_conf.ibas.mpm.mode , == , "FORK" ) )
	{
		p_env->mpm_mode = IBAS_MPMMODE_FORK ;
	}
	else if( STRCMP( p_env->ibas_conf.ibas.mpm.mode , == , "STATIC_WORKERPOOL" ) )
	{
		p_env->mpm_mode = IBAS_MPMMODE_STATIC_WORKERPOOL ;
	}
	else if( STRCMP( p_env->ibas_conf.ibas.mpm.mode , == , "DYNAMIC_WORKERPOOL" ) )
	{
		p_env->mpm_mode = IBAS_MPMMODE_DYNAMIC_WORKERPOOL ;
	}
	else
	{
		ERRORLOGSG( "ibas_conf.ibas.mpm.mode[%s] invalid" , p_env->ibas_conf.ibas.mpm.mode );
		return -1;
	}
	
	p_env->ibma_config_space = IBMAOpenConfigSpace( p_env->ibas_conf.ibma.config_filename ) ;
	if( p_env->ibma_config_space == NULL )
	{
		ERRORLOGSG( "IBMAOpenConfigSpace[%s] failed" , p_env->ibas_conf.ibma.config_filename );
		return -1;
	}
	IBMACloseConfigSpace( p_env->ibma_config_space );
	
	return 0;
}

