#include "ibas_in.h"

char *GetWorkerStatus( unsigned char status )
{
	switch( status )
	{
		case IBAS_WORKERSTATUS_INITING:
			return "INITING";
		case IBAS_WORKERSTATUS_IDLE:
			return "IDLE";
		case IBAS_WORKERSTATUS_LISTENING:
			return "LISTENING";
		case IBAS_WORKERSTATUS_WORKING:
			return "WORKING";
		case IBAS_WORKERSTATUS_RESTARTING:
			return "RESTARTING";
		case IBAS_WORKERSTATUS_EXITING:
			return "EXITING";
	}
	
	return "";
}

