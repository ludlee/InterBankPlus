#ifndef _H_IBAS_API_
#define _H_IBAS_API_

#include "fasterhttp.h"
#include "ibma_api.h"

extern char		*g_ibp_ibas_api_kernel_version ;

struct IbasEnv ;
struct IbasAddonFiles ;
typedef int funcIbasSomain( struct HttpBuffer *req_body , struct HttpBuffer *rsp_body , struct IbasAddonFiles *addon_files );
#define IBAS_SOMAIN		"somain"

const char *IBASGetThisNode();
const char *IBASGetThisApp();
struct HttpBuffer *IBASGetResponseBody();

int IBASSendResponseAhead();

int IBASGetResponseFlag();

int IBASGetRequestFileCount( struct IbasAddonFiles *addon_files );
char *IBASGetRequestFile( struct IbasAddonFiles *addon_files , int index );
int IBASGetResponseFileCount( struct IbasAddonFiles *addon_files );
int IBASPutResponseFile( struct IbasAddonFiles *addon_files , char *filename );
void IBASGetPathFilename( char *filename , char *pathfilename , int sizeof_pathfilename );

struct IbasEnv *IBASGetEnv();

#endif

