#include "ibmsgv_api.h"

int IBMSGVSendRequestAndReceiveResponse_JSON_V( struct IbacEnv *p_env , int keep_alive_flag , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, va_list valist )
{
	char		*msg = NULL ;
	char		*msg_copy = NULL ;
	int		msg_len ;
	
	int		nret = 0 ;
	
	INFOLOGSG( "ibmsgv_api v%s build %s %s ( ibidl v%s, ibutil v%s )" , g_ibp_ibmsgv_api_kernel_version , __DATE__ , __TIME__ , g_ibp_idl_kernel_version , g_ibp_util_kernel_version );
	
	IBACResetCommStatus( p_env );
	
	nret = p_pack_func( pv_req_st , "GB18030" , & msg , NULL , & msg_len ) ;
	if( nret )
	{
		ERRORLOGG( "p_pack_func failed[%d]" , nret );
		IBACDisconnect( p_env );
		return IBP_IBMSGV_ERROR_PACK_MESSAGE;
	}
	else
	{
		INFOLOGG( "p_pack_func ok" );
		INFOLOGG( "msg[%.*s]" , msg_len , msg );
	}
	
	msg_copy = msg ;
	nret = IBACSendRequestAndReceiveResponseV( p_env , keep_alive_flag , app , & msg , & msg_len , valist ) ;
	free( msg_copy );
	if( nret )
	{
		ERRORLOGG( "IBACRequester failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGG( "IBACRequester ok" );
	}
	
	INFOLOGG( "msg[%.*s]" , msg_len , msg );
	msg_len = 0 ;
	nret = p_unpack_func( "GB18030" , msg , & msg_len , pv_rsp_st ) ;
	if( nret )
	{
		ERRORLOGG( "p_unpack_func failed[%d]" , nret );
		IBACDisconnect( p_env );
		return IBP_IBMSGV_ERROR_UNPACK_MESSAGE;
	}
	else
	{
		INFOLOGG( "p_unpack_func ok" );
	}
	
	return 0;
}

int IBMSGVSendRequestAndReceiveResponse_JSON( struct IbacEnv *p_env , int keep_alive_flag , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , p_unpack_func );
	nret = IBMSGVSendRequestAndReceiveResponse_JSON_V( p_env , keep_alive_flag , app , pv_req_st , p_pack_func , pv_rsp_st , p_unpack_func , valist ) ;
	va_end( valist );
	return nret;
}

int IBMSGVRequester_JSON_V( struct IbacEnv *p_env , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, va_list valist )
{
	int		nret = 0 ;
	
	nret = IBACConnect( p_env , node ) ;
	if( nret )
	{
		ERRORLOGG( "IBACConnect failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGG( "IBACConnect ok" );
	}
	
	nret = IBMSGVSendRequestAndReceiveResponse_JSON_V( p_env , 0 , app , pv_req_st , p_pack_func , pv_rsp_st , p_unpack_func , valist ) ;
	if( nret )
	{
		ERRORLOGG( "IBMSGVSendRequestAndReceiveResponse_JSON failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGG( "IBMSGVSendRequestAndReceiveResponse_JSON ok" );
	}
	
	IBACDisconnect( p_env );
	
	return 0;
}

int IBMSGVRequester_JSON( struct IbacEnv *p_env , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , p_unpack_func );
	nret = IBMSGVRequester_JSON_V( p_env , node , app , pv_req_st , p_pack_func , pv_rsp_st , p_unpack_func , valist ) ;
	va_end( valist );
	return nret;
}

int IBMSGVCall_JSON_V( char *ibma_conf_filename , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, va_list valist )
{
	struct IbacEnv		*p_env = NULL ;
	
	int			nret = 0 ;
	
	p_env = IBACCreateEnvirment( ibma_conf_filename ) ;
	if( p_env == NULL )
	{
		ERRORLOGG( "IBACCreateEnvirment failed" );
		return IBP_IBAC_ERROR_ALLOC;
	}
	else
	{
		INFOLOGG( "IBACCreateEnvirment ok" );
	}
	
	nret = IBMSGVRequester_JSON_V( p_env , node , app , pv_req_st , p_pack_func , pv_rsp_st , p_unpack_func , valist ) ;
	if( nret )
	{
		ERRORLOGG( "IBMSGVRequester_JSON failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGG( "IBMSGVRequester_JSON ok" );
	}
	
	IBACDestroyEnvirment( p_env );
	
	return 0;
}

int IBMSGVCall_JSON( char *ibma_conf_filename , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , p_unpack_func );
	nret = IBMSGVCall_JSON_V( ibma_conf_filename , node , app , pv_req_st , p_pack_func , pv_rsp_st , p_unpack_func , valist ) ;
	va_end( valist );
	return nret;
}

