#ifndef _H_IBMSGV_API_
#define _H_IBMSGV_API_

#include "ibac_api.h"
#include "ibas_api.h"
#include "ibtm_api.h"

extern char     *g_ibp_ibmsgv_api_kernel_version ;

typedef int DSCSERIALIZE_JSON_DUP_xxx_V( void *pv , char *encoding , char **pp_base , int *p_buf_size , int *p_len );
typedef int DSCDESERIALIZE_JSON_xxx_V( char *encoding , char *buf , int *p_len , void *pv );

int IBMSGVSendRequestAndReceiveResponse_JSON_V( struct IbacEnv *p_env , int keep_alive_flag , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, va_list valist );
int IBMSGVSendRequestAndReceiveResponse_JSON( struct IbacEnv *p_env , int keep_alive_flag , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, ... );

int IBMSGVRequester_JSON_V( struct IbacEnv *p_env , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, va_list valist );
int IBMSGVRequester_JSON( struct IbacEnv *p_env , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, ... );

int IBMSGVCall_JSON_V( char *ibma_conf_filename , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, va_list valist );
int IBMSGVCall_JSON( char *ibma_conf_filename , char *node , char *app
						, void *pv_req_st , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func
						, void *pv_rsp_st , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func
						, ... );

typedef int funcIbmsgvTransmain( void *pv_req_st , void *pv_rsp_st , struct IbasAddonFiles *addon_files );

int IBMSGVReceiver_JSON( struct HttpBuffer *req_buf , DSCDESERIALIZE_JSON_xxx_V *p_unpack_func , void *pv_req_st
			, struct HttpBuffer *rsp_buf , DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func , void *pv_rsp_st
			, funcIbmsgvTransmain *p_transmain_func , struct IbasAddonFiles *addon_files );

int IBMSGVReceiver_JSON_SendResponseAhead( DSCSERIALIZE_JSON_DUP_xxx_V *p_pack_func , void *pv_rsp_st );

#endif

