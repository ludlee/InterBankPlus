STRUCT					ibp_fetch_config_rspmsg
{
	INT 4				response_code
	STRING 256			response_desc
	
	STRUCT				nodes	ARRAY	1000
	{
		STRING 32		node
		INT 4			invalid
		STRUCT			hosts	ARRAY	100
		{
			STRING 20	ip
			INT 4		port
			INT 4		invalid
		}
		STRING 1024		key_base64
	}
	
	STRUCT				apps	ARRAY	50000
	{
		STRING 32		app
		STRING 256		desc
		STRING 256		bin
		INT 4			timeout
		INT 4			timeout2
		INT 4			invalid
		STRING 32		md5
	}
}

