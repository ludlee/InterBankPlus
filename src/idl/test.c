#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "IDL_ibp_fetch_config_rspmsg.dsc.h"

void test_DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg( char *pathfilename )
{
	FILE			*fp = NULL ;
	char			*file_content = NULL ;
	int			file_len ;
	ibp_fetch_config_rspmsg	*pst = NULL ;
	
	int			nret = 0 ;
	
	fp = fopen( pathfilename , "r" ) ;
	if( fp == NULL )
	{
		printf( "can't open filename[%s]\n" , pathfilename );
		return;
	}
	
	fseek( fp , 0 , SEEK_END );
	file_len = ftell( fp ) ;
	fseek( fp , 0 , SEEK_SET ) ;
	
	file_content = (char*)malloc( file_len + 1 ) ;
	if( file_content == NULL )
	{
		printf( "malloc failed , errno[%d]\n" , errno );
		return;
	}
	memset( file_content , 0x00 , file_len + 1 );
	fread( file_content , file_len , 1 , fp );
	fclose( fp );
	
	pst = (ibp_fetch_config_rspmsg *)malloc( sizeof(ibp_fetch_config_rspmsg) ) ;
	if( pst == NULL )
	{
		printf( "malloc failed , errno[%d]\n" , errno );
		return;
	}
	memset( pst , 0x00 , sizeof(ibp_fetch_config_rspmsg) );
	nret = DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg( "GB18030" , file_content , & file_len , pst ) ;
	printf( "DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg return[%d]\n" , nret );
	
	free( file_content );
	free( pst );
	
	return;
}

int main( int argc , char *argv[] )
{
	if( argc == 1 + 1 )
	{
		test_DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg( argv[1] );
	}
	else
	{
		printf( "usage invalid\n" );
	}
	
	return 0;
}

