/* It had generated by DirectStruct v1.7.0 */
#include "IDL_ibp_alive_report_reqmsg.dsc.h"

#ifndef FUNCNAME_DSCLOG_ibp_alive_report_reqmsg
#define FUNCNAME_DSCLOG_ibp_alive_report_reqmsg	DSCLOG_ibp_alive_report_reqmsg
#endif

#ifndef PREFIX_DSCLOG_ibp_alive_report_reqmsg
#define PREFIX_DSCLOG_ibp_alive_report_reqmsg	printf( 
#endif

#ifndef NEWLINE_DSCLOG_ibp_alive_report_reqmsg
#define NEWLINE_DSCLOG_ibp_alive_report_reqmsg	"\n"
#endif

int FUNCNAME_DSCLOG_ibp_alive_report_reqmsg( ibp_alive_report_reqmsg *pst )
{
	int	index[10] = { 0 } ; index[0] = 0 ;
	PREFIX_DSCLOG_ibp_alive_report_reqmsg "ibp_alive_report_reqmsg.node[%s]" NEWLINE_DSCLOG_ibp_alive_report_reqmsg , pst->node );
	PREFIX_DSCLOG_ibp_alive_report_reqmsg "ibp_alive_report_reqmsg.ip[%s]" NEWLINE_DSCLOG_ibp_alive_report_reqmsg , pst->ip );
	PREFIX_DSCLOG_ibp_alive_report_reqmsg "ibp_alive_report_reqmsg.port[%d]" NEWLINE_DSCLOG_ibp_alive_report_reqmsg , pst->port );
	return 0;
}
