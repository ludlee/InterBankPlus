#ifndef _H_IBP_IDL_
#define _H_IBP_IDL_

#include "ibp_ver.h"

#include "IDL_ibms_node_reqmsg.dsc.h"
#include "IDL_ibms_node_host_reqmsg.dsc.h"
#include "IDL_ibms_app_reqmsg.dsc.h"
#include "IDL_ibms_project_reqmsg.dsc.h"
#include "IDL_ibp_fetch_config_rspmsg.dsc.h"
#include "IDL_ibp_alive_report_reqmsg.dsc.h"
#include "IDL_ibp_alive_notice_reqmsg.dsc.h"

#define IBP_MAXLEN_NODE		sizeof(((ibms_node_reqmsg*)0)->node)
#define IBP_MAXLEN_NODE_IP	sizeof(((ibms_node_host_reqmsg*)0)->ip)
#define IBP_MAXLEN_APP		sizeof(((ibms_app_reqmsg*)0)->app)
#define IBP_MAXLEN_APP_DESC	sizeof(((ibms_app_reqmsg*)0)->desc)
#define IBP_MAXLEN_APP_BIN	sizeof(((ibms_app_reqmsg*)0)->bin)
#define IBP_MAXLEN_PROJECT	sizeof(((ibms_project_reqmsg*)0)->project)

extern char	*g_ibp_idl_kernel_version ;

#endif

