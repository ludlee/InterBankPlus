#ifndef _H_IBTM_API_
#define _H_IBTM_API_

#include "ibma_api.h"
#include "ibas_api.h"

/********* 以下给transmain使用 *********/

/* 交易处理环境 */
struct TransProcessEnv ;

struct TransProcessEnv *IBTMCreateTransProcessEnv( char *req_msg_name , int sizeof_req_msg , void *p_req_st
						, char *rsp_msg_name , int sizeof_rsp_msg , void *p_rsp_st
						, struct IbasAddonFiles *addon_files
						, char *trans_code );
void IBTMDestroyTransProcessEnv( struct TransProcessEnv *p_tpe );

/* 错误码 */
void IBTMSetResponseCode( struct TransProcessEnv *p_tpe , int nret );
void IBTMFormatResponseCode( struct TransProcessEnv *p_tpe , char *response_code , int sizeof_response_code );

/********* 以下给IBTPS使用 *********/

/* 交易函数数组单元类型 */
#define TFATYPE_FUNCARRAY				11 /* 交易函数数组，执行某个单元返回<0时中断流程并抛出该返回值 */
#define TFATYPE_FUNC					12 /* 交易函数，执行某个单元返回<0时中断流程并抛出该返回值 */
#define TFATYPE_IF_IN_TRANSCODES_THEN_FUNCARRAY		21 /* 如果当前交易码在配置交易码列表中，则执行交易函数数组 */
#define TFATYPE_IF_IN_TRANSCODES_THEN_FUNC		22 /* 如果当前交易码在配置交易码列表中，则执行交易函数 */
#define TFATYPE_IF_NOTIN_TRANSCODES_THEN_FUNCARRAY	25 /* 如果当前交易码不在配置交易码列表中，则执行交易函数数组 */
#define TFATYPE_IF_NOTIN_TRANSCODES_THEN_FUNC		26 /* 如果当前交易码不在配置交易码列表中，则执行交易函数 */
#define TFATYPE_IF_FUNCARRAY_THEN			31 /* 如果交易函数数组执行返回>0时，执行本数组中下一层级内容 */
#define TFATYPE_IF_FUNC_THEN				32 /* 如果交易函数执行返回>0时，执行本数组中下一层级内容 */
#define TFATYPE_ELSE					37 /* 与IF_..._THEN配合使用 */
#define TFATYPE_ENDIF					39 /* 与IF_..._THEN配合使用 */
#define TFATYPE_RETURN					99 /* 交易函数数组最后一个单元（必须） */

/* 交易函数数组单元配置展开宏 */
#define FUNCARRAY(_desc_,_funcarray_)						{ TFATYPE_FUNCARRAY , _desc_ , _funcarray_ , NULL , NULL } ,
#define FUNC(_desc_,_func_)							{ TFATYPE_FUNC , _desc_ , NULL , _func_ , NULL } ,
#define IF_IN_TRANSCODES_THEN_FUNCARRAY(_desc_,_transcode_,_funcarray_)		{ TFATYPE_IF_IN_TRANSCODES_THEN_FUNCARRAY , _desc_ , _funcarray_ , NULL , _transcode_ } ,
#define IF_IN_TRANSCODES_THEN_FUNC(_desc_,_transcode_,_func_)			{ TFATYPE_IF_IN_TRANSCODES_THEN_FUNC , _desc_ , NULL , _func_ , _transcode_ } ,
#define IF_NOTIN_TRANSCODES_THEN_FUNCARRAY(_desc_,_transcode_,_funcarray_)	{ TFATYPE_IF_NOTIN_TRANSCODES_THEN_FUNCARRAY , _desc_ , _funcarray_ , NULL , _transcode_ } ,
#define IF_NOTIN_TRANSCODES_THEN_FUNC(_desc_,_transcode_,_func_)		{ TFATYPE_IF_NOTIN_TRANSCODES_THEN_FUNC , _desc_ , NULL , _func_ , _transcode_ } ,
#define IF_FUNCARRAY_THEN(_desc_,_funcarray_)					{ TFATYPE_IF_FUNCARRAY_THEN , _desc_ , _funcarray_ , NULL , NULL } ,
#define IF_FUNC_THEN(_desc_,_func_)						{ TFATYPE_IF_FUNC_THEN , _desc_ , NULL , _func_ , NULL } ,
#define ELSE									{ TFATYPE_ELSE , NULL , NULL , NULL , NULL } ,
#define ENDIF									{ TFATYPE_ENDIF , NULL , NULL , NULL , NULL } ,
#define RETURN									{ TFATYPE_RETURN , NULL , NULL , NULL , NULL }

/* 交易函数原型定义 */
typedef int TransFunc( struct TransProcessEnv *p_tpe );

/* 函数数组c结构定义 */
struct TransFuncArray
{
	int			type ; /* 本单元下挂的类型 */
	
	char			*desc ; /* 中文描述，便于流程描述 */
	struct TransFuncArray	*func_array ; /* 下挂的是函数数组 */
	TransFunc		*func ; /* 下挂的是函数 */
	
	char			*trans_codes ; /* 交易码列表，用于交易码分支 */
} ;

/* 执行函数数组 */
int IBTMExecuteFuncArray( struct TransProcessEnv *p_tpe , char *desc , struct TransFuncArray *p_tfa );

/********* 以下给应用函数使用 *********/

/* 结构数据块操作函数 */
#define IBP_MAXLEN_STRUCTBLOCK_NAME		64

int IBTMAppendStructBlock( struct TransProcessEnv *tpe , char *name , long struct_size , void **ppmem );
int IBTMGetStructBlock( struct TransProcessEnv *tpe , char *name , long struct_size , void **ppmem );
int IBTMAppendGetStructBlock( struct TransProcessEnv *tpe , char *name , long struct_size , void **ppmem );

#endif

