#include "ibtm_in.h"
#include "ibtm_api.h"

static char *FindSubInString( char *str , char *sub )
{
	long	sublen ;
	char	*p1 = NULL , *p2 = NULL ;
	
	sublen = strlen(sub) ;
	
	p1 = str ;
	do
	{
		while( (*p1) && strchr( " \t\r\n\f" , (*p1) ) )
			p1++;
		if( (*p1) == '\0' )
			return NULL;
		
		p2 = p1 ;
		while( (*p2) && !strchr( " \t\r\n\f" , (*p2) ) )
			p2++;
		if( p2 - p1 == sublen && memcmp( p1 , sub , sublen ) == 0 )
			return p1;
		if( (*p2) == '\0' )
			return NULL;
		
		p1 = p2 + 1 ;
	}
	while( (*p1) );
	
	return NULL;
}

static int ExecuteFuncArray( struct TransProcessEnv *p_tpe , struct TransFuncArray *p_tfa )
{
	int			i ;
	char			tabbuf[ 64 + 1 ] = "" ;
	struct TransFuncArray	*p = NULL ;
	
	int			nret = 0 ;
	
	if( p_tfa == NULL )
		return 0;
	
	if( p_tpe->depth )
		for( i = 0 ; i < p_tpe->depth ; i++ )
			strcat( tabbuf , "+ - " );
	
	for( p = p_tfa ; p->type != TFATYPE_RETURN ; p++ )
	{
		if( p->type == TFATYPE_RETURN )
		{
			break;
		}
		else if( p->type == TFATYPE_FUNCARRAY )
		{
			INFOLOGG( "\t%s进入函数数组[%s]" , tabbuf , p->desc ); p_tpe->depth++;
			nret = ExecuteFuncArray( p_tpe , p->func_array ) ;
			WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数数组[%s] 返回值[%d]" , tabbuf , p->desc , nret ); p_tpe->depth--;
			if( nret )
			{
				return nret;
			}
		}
		else if( p->type == TFATYPE_FUNC )
		{
			INFOLOGG( "\t%s进入函数[%s]" , tabbuf , p->desc );
			nret = p->func( p_tpe ) ;
			WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数[%s] 返回值[%d]" , tabbuf , p->desc , nret );
			if( nret )
			{
				return nret;
			}
		}
		else if( p->type == TFATYPE_IF_IN_TRANSCODES_THEN_FUNCARRAY )
		{
			if( FindSubInString( p_tpe->trans_code , p->trans_codes ) )
			{
				INFOLOGG( "\t%s进入函数数组[%s]" , tabbuf , p->desc ); p_tpe->depth++;
				nret = ExecuteFuncArray( p_tpe , p->func_array ) ;
				WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数数组[%s] 返回值[%d]" , tabbuf , p->desc , nret ); p_tpe->depth--;
				if( nret )
				{
					return nret;
				}
			}
		}
		else if( p->type == TFATYPE_IF_IN_TRANSCODES_THEN_FUNC )
		{
			if( FindSubInString( p_tpe->trans_code , p->trans_codes ) )
			{
				INFOLOGG( "\t%s进入函数[%s]" , tabbuf , p->desc );
				nret = p->func( p_tpe ) ;
				WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数[%s] 返回值[%d]" , tabbuf , p->desc , nret );
				if( nret )
				{
					return nret;
				}
			}
		}
		else if( p->type == TFATYPE_IF_NOTIN_TRANSCODES_THEN_FUNCARRAY )
		{
			if( ! FindSubInString( p_tpe->trans_code , p->trans_codes ) )
			{
				INFOLOGG( "\t%s进入函数数组[%s]" , tabbuf , p->desc ); p_tpe->depth++;
				nret = ExecuteFuncArray( p_tpe , p->func_array ) ;
				WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数数组[%s] 返回值[%d]" , tabbuf , p->desc , nret ); p_tpe->depth--;
				if( nret )
				{
					return nret;
				}
			}
		}
		else if( p->type == TFATYPE_IF_NOTIN_TRANSCODES_THEN_FUNC )
		{
			if( ! FindSubInString( p_tpe->trans_code , p->trans_codes ) )
			{
				INFOLOGG( "\t%s进入函数[%s]" , tabbuf , p->desc );
				nret = p->func( p_tpe ) ;
				WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数[%s] 返回值[%d]" , tabbuf , p->desc , nret );
				if( nret )
				{
					return nret;
				}
			}
		}
		else if( p->type == TFATYPE_IF_FUNCARRAY_THEN )
		{
			INFOLOGG( "\t%s进入函数数组[%s]" , tabbuf , p->desc ); p_tpe->depth++;
			nret = ExecuteFuncArray( p_tpe , p->func_array ) ;
			WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数数组[%s] 返回值[%d]" , tabbuf , p->desc , nret ); p_tpe->depth--;
			if( nret < 0 )
			{
				return nret;
			}
			else if( nret == 0 )
			{
				p_tpe->depth++;
				nret = ExecuteFuncArray( p_tpe , p+1 ) ;
				p_tpe->depth--;
				if( nret )
					return nret;
				
				for( p++ ; p->type != TFATYPE_RETURN ; p++ )
				{
					if( p->type == TFATYPE_ENDIF )
						break;
				}
				if( p->type == TFATYPE_RETURN )
					return IBP_IBTM_ERROR_EXPECT_ENDIF;
			}
			else
			{
				for( p++ ; p->type != TFATYPE_RETURN ; p++ )
				{
					if( p->type == TFATYPE_ELSE || p->type == TFATYPE_ENDIF )
						break;
				}
				if( p->type == TFATYPE_RETURN )
					return IBP_IBTM_ERROR_EXPECT_ENDIF;
				
				if( p->type == TFATYPE_ELSE )
				{
					p_tpe->depth++;
					nret = ExecuteFuncArray( p_tpe , p+1 ) ;
					p_tpe->depth--;
					if( nret )
						return nret;
					
					for( p++ ; p->type != TFATYPE_RETURN ; p++ )
					{
						if( p->type == TFATYPE_ENDIF )
							break;
					}
					if( p->type == TFATYPE_RETURN )
						return IBP_IBTM_ERROR_EXPECT_ENDIF;
				}	
			}
		}
		else if( p->type == TFATYPE_IF_FUNC_THEN )
		{
			INFOLOGG( "\t%s进入函数[%s]" , tabbuf , p->desc );
			nret = p->func( p_tpe ) ;
			WRITELOGG( nret<0?LOG_LEVEL_ERROR:LOG_LEVEL_INFO , "\t%s离开函数[%s] 返回值[%d]" , tabbuf , p->desc , nret );
			if( nret < 0 )
			{
				return nret;
			}
			else if( nret == 0 )
			{
				p_tpe->depth++;
				nret = ExecuteFuncArray( p_tpe , p+1 ) ;
				p_tpe->depth--;
				if( nret )
					return nret;
				
				for( p++ ; p->type != TFATYPE_RETURN ; p++ )
				{
					if( p->type == TFATYPE_ENDIF )
						break;
				}
				if( p->type == TFATYPE_RETURN )
					return IBP_IBTM_ERROR_EXPECT_ENDIF;
			}
			else
			{
				for( p++ ; p->type != TFATYPE_RETURN ; p++ )
				{
					if( p->type == TFATYPE_ELSE || p->type == TFATYPE_ENDIF )
						break;
				}
				if( p->type == TFATYPE_RETURN )
					return IBP_IBTM_ERROR_EXPECT_ENDIF;
				
				if( p->type == TFATYPE_ELSE )
				{
					p_tpe->depth++;
					nret = ExecuteFuncArray( p_tpe , p+1 ) ;
					p_tpe->depth--;
					if( nret )
						return nret;
					
					for( p++ ; p->type != TFATYPE_RETURN ; p++ )
					{
						if( p->type == TFATYPE_ENDIF )
							break;
					}
					if( p->type == TFATYPE_RETURN )
						return IBP_IBTM_ERROR_EXPECT_ENDIF;
				}	
			}
		}
		else if( p->type == TFATYPE_ELSE )
		{
			return 0;
		}
		else if( p->type == TFATYPE_ENDIF )
		{
			return 0;
		}
		else
		{
			ERRORLOGG( "\t%s函数数组类型[%d]无效" , tabbuf , p->type );
			return -1;
		}
	}
	
	return 0;
}

int IBTMExecuteFuncArray( struct TransProcessEnv *p_tpe , char *desc , struct TransFuncArray *p_tfa )
{
	int		nret = 0 ;
	
	INFOLOGG( "进入处理阶段[%s]" , desc ); p_tpe->depth++;
	nret = ExecuteFuncArray( p_tpe , p_tfa ) ;
	INFOLOGG( "离开处理阶段[%s]" , desc ); p_tpe->depth--;
	
	return nret;
}

