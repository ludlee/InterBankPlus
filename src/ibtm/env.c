#include "ibtm_in.h"
#include "ibtm_api.h"

struct TransProcessEnv *IBTMCreateTransProcessEnv( char *req_msg_name , int sizeof_req_msg , void *pv_req_st
						, char *rsp_msg_name , int sizeof_rsp_msg , void *pv_rsp_st
						, struct IbasAddonFiles *addon_files
						, char *trans_code )
{
	struct TransProcessEnv	*p_tpe = NULL ;
	
	int			nret = 0 ;
	
	p_tpe = (struct TransProcessEnv *)malloc( sizeof(struct TransProcessEnv) ) ;
	if( p_tpe == NULL )
	{
		ERRORLOGG( "malloc failed , errno[%d]" , errno );
		return NULL;
	}
	memset( p_tpe , 0x00 , sizeof(struct TransProcessEnv) );
	
	nret = IBTMAppendStructBlock( p_tpe , req_msg_name , sizeof_req_msg , (void**) & pv_req_st ) ;
	if( nret )
	{
		ERRORLOGSG( "IBTMAppendStructBlock failed[%d]" , nret );
		return NULL;
	}
	
	memset( (char*)pv_rsp_st , 0x00 , sizeof_rsp_msg );
	nret = IBTMAppendStructBlock( p_tpe , rsp_msg_name , sizeof_rsp_msg , (void**) & pv_rsp_st ) ;
	if( nret )
	{
		ERRORLOGSG( "IBTMAppendStructBlock failed[%d]" , nret );
		return NULL;
	}
	
	p_tpe->addon_files = addon_files ;
	
	strncpy( p_tpe->trans_code , trans_code , MIN(sizeof(p_tpe->trans_code)-1,strlen(trans_code)) );
	
	return p_tpe;
}

void IBTMDestroyTransProcessEnv( struct TransProcessEnv *p_tpe )
{
	if( p_tpe )
	{
		DestroyStructBlockTree( p_tpe );
		
		free( p_tpe );
	}
	
	return;
}

void IBTMSetResponseCode( struct TransProcessEnv *p_tpe , int nret )
{
	if( p_tpe->response_code == 0 )
		p_tpe->response_code = nret ;
	
	return;
}

void IBTMFormatResponseCode( struct TransProcessEnv *p_tpe , char *response_code , int sizeof_response_code )
{
	memset( response_code , 0x00 , sizeof_response_code );
	snprintf( response_code , sizeof_response_code , "%06d" , p_tpe->response_code );
	
	return;
}
