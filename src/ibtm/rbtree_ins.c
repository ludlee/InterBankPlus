#include "ibtm_api.h"
#include "ibtm_in.h"

#include "rbtree_tpl.h"

extern void FreeeStructBlock( void *pv );

LINK_RBTREENODE_STRING( LinkStructBlockTreeNode , struct TransProcessEnv , structs_rbtree , struct StructBlock , name_rbnode , name )
QUERY_RBTREENODE_STRING( QueryStructBlockTreeNode , struct TransProcessEnv , structs_rbtree , struct StructBlock , name_rbnode , name )
UNLINK_RBTREENODE( UnlinkStructBlockTreeNode , struct TransProcessEnv , structs_rbtree , struct StructBlock , name_rbnode )
DESTROY_RBTREE( DestroyStructBlockTree , struct TransProcessEnv , structs_rbtree , struct StructBlock , name_rbnode , & FreeeStructBlock )
