#include "ibtm_in.h"
#include "ibtm_api.h"

int IBTMDoTrans( struct IbasAddonFiles *addon_files , char *trans_code
		, char *req_msg_name , int sizeof_req_msg , void *pv_req_msg
		, char *rsp_msg_name , int sizeof_rsp_msg , void *pv_rsp_msg
		, funcExecuteSchedule *pfuncExecuteSchedule , void *p_tps , int sizeof_tps
		, char *response_code , int sizeof_response_code )
{
	struct TransProcessEnv	*p_tpe = NULL ;
	
	int			nret = 0 ;
	
	p_tpe = CreateTransProcessEnv( addon_files , trans_code ) ;
	if( p_tpe == NULL )
	{
		ERRORLOGG( "CreateTransProcessEnv failed , errno[%d]" , errno );
		sprintf( response_code , "%d" , IBP_IBTM_ERROR_ALLOC );
		return IBP_IBTM_ERROR_ALLOC;
	}
	
	nret = pfuncExecuteSchedule( p_tpe , p_tps , sizeof_tps ) ;
	if( nret )
	{
		ERRORLOGSG( "pfuncExecuteSchedule failed[%d]" , nret );
		memset( response_code , 0x00 , sizeof_response_code );
		snprintf( response_code , sizeof_response_code , "%d" , nret );
	}
	else
	{
		INFOLOGSG( "pfuncExecuteSchedule ok" );
		memset( response_code , 0x00 , sizeof_response_code );
		snprintf( response_code , sizeof_response_code-1 , "000000" );
	}
	
	return nret;
}

