#include "ibtm_api.h"

/* 结构数据块 */
struct StructBlock
{
	char		name[ IBP_MAXLEN_STRUCTBLOCK_NAME + 1 ] ; /* 结构名称 */
	
	long		struct_size ; /* 结构大小 */
	void		*pmem ; /* 结构指针 */
	
	char		alloc_flag ;
	
	struct rb_node	name_rbnode ; /* 结构树节点 */
} ;

int LinkStructBlockTreeNode( struct TransProcessEnv *p_tpe , struct StructBlock *p_struct_block );
struct StructBlock *QueryStructBlockTreeNode( struct TransProcessEnv *p_tpe , struct StructBlock *p_struct_block );
void UnlinkStructBlockTreeNode( struct TransProcessEnv *p_tpe , struct StructBlock *p_struct_block );
void DestroyStructBlockTree( struct TransProcessEnv *p_tpe );

/* 交易处理环境 */
struct TransProcessEnv
{
	struct rb_root		structs_rbtree ; /* 结构数据块树 */
	struct IbasAddonFiles	*addon_files ; /* ibas请求文件和响应文件信息 */
	char			trans_code[ IBP_MAXLEN_APP + 1 ] ; /* 业务交易码 */
	
	int			depth ; /* 流程深度 */
	
	int			response_code;
} ;

