#ifndef _H_IBAC_API_
#define _H_IBAC_API_

#include "ibma_api.h"

extern char     *g_ibp_ibac_api_kernel_version ;

struct IbacEnv *IBACCreateEnvirment( char *config_filename );
void IBACDestroyEnvirment( struct IbacEnv *p_env );

#define IBAC_COMM_STATUS_BEFORE_SENDING		0
#define IBAC_COMM_STATUS_ON_SENDING		1
#define IBAC_COMM_STATUS_AFTER_SENDING		2

int IBACConnect( struct IbacEnv *p_env , char *node );
int IBACSendRequestAndReceiveResponseV( struct IbacEnv *p_env , int keep_alive_flag , char *app , char **pp_msg , int *p_msg_len , va_list valist );
int IBACSendRequestAndReceiveResponse( struct IbacEnv *p_env , int keep_alive_flag , char *app , char **pp_msg , int *p_msg_len , ... );
int IBACDisconnect( struct IbacEnv *p_env );

int IBACRequesterV( struct IbacEnv *p_env , char *node , char *app , char **pp_msg , int *p_msg_len , va_list valist );
int IBACRequester( struct IbacEnv *p_env , char *node , char *app , char **pp_msg , int *p_msg_len , ... );

int IBACCallV( char *ibma_config_filename , char *node , char *app , char **pp_msg , int *p_msg_len , va_list valist );
int IBACCall( char *ibma_config_filename , char *node , char *app , char **pp_msg , int *p_msg_len , ... );

void IBACResetCommStatus( struct IbacEnv *p_env );
int IBACGetCommStatus( struct IbacEnv *p_env );

#endif

