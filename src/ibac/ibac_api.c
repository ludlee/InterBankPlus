#include "ibp_util.h"
#include "ibac_api.h"

char		*g_ibp_ibac_api_kernel_version = IBP_VERSION ;

struct IbacEnv
{
	struct IbmaConfigSpace	*ibma_conf_space ;
	char			node[ IBP_MAXLEN_NODE + 1 ] ;
	
	struct NetAddress	netaddr ;
	struct HttpEnv		*pre_http ;
	struct HttpEnv		*msg_http ;
	struct HttpEnv		*file_http ;
	struct HttpSecureEnv	*http_secure_env ;
	
	struct NodeSpaceUnit	*p_this_node_unit ;
	struct NodeSpaceUnit	*p_server_node_unit ;
	struct HostSpaceUnit	*p_server_host_unit ;
	struct AppSpaceUnit	*p_this_app_unit ;
	
	int			comm_status ;
} ;

struct IbacEnv *IBACCreateEnvirment( char *ibma_conf_filename )
{
	struct IbacEnv	*p_env = NULL ;
	
	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();
	
	p_env = (struct IbacEnv *)malloc( sizeof(struct IbacEnv) ) ;
	if( p_env == NULL )
	{
		ERRORLOGSG( "malloc failed , errno[%d]" , errno );
		return NULL;
	}
	memset( p_env , 0x00 , sizeof(struct IbacEnv) );
	
	p_env->ibma_conf_space = IBMAOpenConfigSpace( ibma_conf_filename ) ;
	if( p_env->ibma_conf_space == NULL )
	{
		ERRORLOGSG( "IBMAOpenConfigSpace failed" );
		free( p_env );
		return NULL;
	}
	
	p_env->netaddr.sock = -1 ;
	
	p_env->pre_http = CreateHttpEnv() ;
	if( p_env->pre_http == NULL )
	{
		ERRORLOGSG( "CreateHttpEnv failed" );
		IBMACloseConfigSpace( p_env->ibma_conf_space );
		free( p_env );
		return NULL;
	}
	
	p_env->msg_http = CreateHttpEnv() ;
	if( p_env->msg_http == NULL )
	{
		ERRORLOGSG( "CreateHttpEnv failed" );
		IBMACloseConfigSpace( p_env->ibma_conf_space );
		DestroyHttpEnv( p_env->pre_http );
		free( p_env );
		return NULL;
	}
	
	p_env->file_http = CreateHttpEnv() ;
	if( p_env->file_http == NULL )
	{
		ERRORLOGSG( "CreateHttpEnv failed" );
		IBMACloseConfigSpace( p_env->ibma_conf_space );
		DestroyHttpEnv( p_env->pre_http );
		DestroyHttpEnv( p_env->msg_http );
		free( p_env );
		return NULL;
	}
	
	p_env->http_secure_env = CreateHttpSecureEnv( IBP_HTTPSECURE_SIGN_FLAG_MD5 , IBP_HTTPSECURE_COMPRESS_FLAG_GZIP , IBP_HTTPSECURE_ENCRYPT_FLAG_3DES_FOR_RSA ) ;
	if( p_env->http_secure_env == NULL )
	{
		ERRORLOGSG( "CreateHttpSecureEnv failed" );
		IBMACloseConfigSpace( p_env->ibma_conf_space );
		DestroyHttpEnv( p_env->pre_http );
		DestroyHttpEnv( p_env->msg_http );
		DestroyHttpEnv( p_env->file_http );
		free( p_env );
		return NULL;
	}
	else
	{
		INFOLOGSG( "CreateHttpSecureEnv ok" );
	}
	
	return p_env;
}

void IBACDestroyEnvirment( struct IbacEnv *p_env )
{
	if( p_env )
	{
		IBMACloseConfigSpace( p_env->ibma_conf_space );
		DestroyHttpEnv( p_env->pre_http );
		DestroyHttpEnv( p_env->msg_http );
		DestroyHttpEnv( p_env->file_http );
		DestroyHttpSecureEnv( p_env->http_secure_env );
		free( p_env );
	}
	
	return;
}

#define _BEFORE_RETRY_SELECT_HOST \
	p_env->p_server_host_unit->cannot_connect_timestamp = time( NULL ) ; \
	IBACDisconnect( p_env ); \
	goto _RETRY_SELECT_HOST; \

int IBACConnect( struct IbacEnv *p_env , char *node )
{
	struct HostSpaceUnit	*p_server_host_unit = NULL ;
	time_t			timestamp_now ;
	int			retry_connect_timeval ;
	
	int			nret = 0 ;
	
	if( p_env->netaddr.sock != -1 )
	{
		ERRORLOGSG( "connection already established" );
		return IBP_IBAC_ERROR_NO_CLIENT_SOCKET;
	}
	
	nret = IBMACheckConfigSpaceAttaching( p_env->ibma_conf_space ) ;
	if( nret < 0 )
	{
		FATALLOGSG( "IBMACheckConfigSpaceAttaching failed[%d]" , nret );
		return nret;
	}
	
	p_env->p_this_node_unit = IBMAQueryNode( p_env->ibma_conf_space , IBMAGetThisNodePtr( p_env->ibma_conf_space ) ) ;
	if( p_env->p_this_node_unit == NULL )
	{
		ERRORLOGSG( "this node[%s] not exist" , IBMAGetThisNodePtr( p_env->ibma_conf_space ) );
		return IBP_IBMA_ERROR_THIS_NODE_NOT_EXIST;
	}
	else
	{
		DEBUGLOGSG( "node[%s] found" , IBMAGetThisNodePtr( p_env->ibma_conf_space ) );
	}
	
	strncpy( p_env->node , node , sizeof(p_env->node)-1 );
	p_env->p_server_node_unit = IBMAQueryNode( p_env->ibma_conf_space , p_env->node ) ;
	if( p_env->p_server_node_unit == NULL )
	{
		ERRORLOGSG( "server node[%s] not exist" , p_env->node );
		return IBP_IBMA_ERROR_SERVER_NODE_NOT_EXIST;
	}
	else
	{
		DEBUGLOGSG( "node[%s] found" , p_env->node );
	}
	
_RETRY_SELECT_HOST :
	
	timestamp_now = time( NULL ) ;
	retry_connect_timeval = IBMAGetRetryConnectTimeval(p_env->ibma_conf_space) ;
	p_server_host_unit = IBMATravelNodeHosts( p_env->ibma_conf_space , p_env->p_server_node_unit , NULL ) ;
	p_env->p_server_host_unit = NULL ;
	while( p_server_host_unit )
	{
		if( p_server_host_unit->cannot_connect_timestamp )
		{
			if( timestamp_now - p_server_host_unit->cannot_connect_timestamp > retry_connect_timeval )
			{
				p_server_host_unit->cannot_connect_timestamp = 0 ;
			}
		}
		
#if 0
		if( p_server_host_unit->invalid == 0 && p_server_host_unit->cannot_connect_timestamp == 0 )
#endif
		if( p_server_host_unit->cannot_connect_timestamp == 0 )
		{
			if( p_env->p_server_host_unit == NULL || p_server_host_unit->load < p_env->p_server_host_unit->load )
			{
				p_env->p_server_host_unit = p_server_host_unit ;
			}
		}
		
		p_server_host_unit = IBMATravelNodeHosts( p_env->ibma_conf_space , p_env->p_server_node_unit , p_server_host_unit ) ;
	}
	if( p_env->p_server_host_unit == NULL )
	{
		ERRORLOGSG( "valid server host not found" );
		return IBP_IBMA_ERROR_NO_VALID_SERVER_HOST;
	}
	
	nret = ReloadRsaKey( p_env->http_secure_env , p_env->p_this_node_unit , p_env->p_server_node_unit ) ;
	if( nret )
	{
		ERRORLOGSG( "ReloadRsaKeyFiles failed[%d]" , nret );
		return IBP_IBAC_ERROR_S(nret);
	}
	else
	{
		DEBUGLOGSG( "ReloadRsaKeyFiles ok" );
	}
	
	p_env->netaddr.sock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ;
	if( p_env->netaddr.sock == -1 )
	{
		ERRORLOGSG( "socket failed , errno[%d]" , errno );
		return IBP_IBAC_ERROR_SOCKET;
	}
	
	SetHttpReuseAddr( p_env->netaddr.sock );
	SetHttpNodelay( p_env->netaddr.sock , 1 );
	
	strcpy( p_env->netaddr.ip , p_env->p_server_host_unit->ip );
	p_env->netaddr.port = p_env->p_server_host_unit->port ;
	SETNETADDRESS( p_env->netaddr )
	nret = connect( p_env->netaddr.sock , (struct sockaddr *) & (p_env->netaddr.addr) , sizeof(struct sockaddr) ) ;
	if( nret == -1 )
	{
		ERRORLOGSG( "connect[%s:%d] failed , errno[%d]" , p_env->netaddr.ip , p_env->netaddr.port , errno );
		_BEFORE_RETRY_SELECT_HOST
	}
	else
	{
		INFOLOGSG( "connect[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	
	return 0;
}

#define _BEFORE_RETRY_PRE_TRANSACTION \
	if( first_send_flag == 1 ) \
	{ \
		first_send_flag = 0 ; \
		p_env->p_server_host_unit->cannot_connect_timestamp = time( NULL ) ; \
		IBACDisconnect( p_env ); \
		nret = IBACConnect( p_env , p_env->node ) ; \
		if( nret ) \
		{ \
			ERRORLOGSG( "IBACConnect failed[%d]" , nret ); \
			return nret; \
		} \
		goto _RETRY_PRE_TRANSACTION; \
	} \
	else \
	{ \
		return IBP_IBAC_ERROR_A_S(nret); \
	} \

int IBACSendRequestAndReceiveResponseV( struct IbacEnv *p_env , int keep_alive_flag , char *app , char **pp_msg , int *p_msg_len , va_list valist )
{
	struct HttpBuffer	*pre_req_buf = NULL ;
	struct HttpBuffer	*pre_rsp_buf = NULL ;
	struct HttpBuffer	*file_req_buf = NULL ;
	struct HttpBuffer	*file_rsp_buf = NULL ;
	struct HttpBuffer	*msg_req_buf = NULL ;
	struct HttpBuffer	*msg_rsp_buf = NULL ;
	int			timeout ;
	int			first_send_flag ;
	int			status_code ;
	char			filename[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			file_id[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	FILE			*fp = NULL ;
	char			*p_req_filename = NULL ;
	int			req_filename_len ;
	int			headers_req_filenames_count ;
	char			*p_rsp_filename = NULL ;
	char			HTTPHEADER_name[ sizeof(IBP_HTTPHEADER_IBP_FILE_NAME)+2 ] ;
	int			i ;
	int			rsp_filelen ;
	char			*file_content = NULL ;
	int			file_len ;
	int			valist_end_flag ;
	
	int			nret = 0 ;
	
	INFOLOGSG( "ibac_api v%s build %s %s ( ibidl v%s, ibutil v%s )" , g_ibp_ibac_api_kernel_version , __DATE__ , __TIME__ , g_ibp_idl_kernel_version , g_ibp_util_kernel_version );
	
	p_env->comm_status = IBAC_COMM_STATUS_BEFORE_SENDING ;
	
	nret = IBMACheckConfigSpaceAttaching( p_env->ibma_conf_space ) ;
	if( nret < 0 )
	{
		FATALLOGSG( "IBMACheckConfigSpaceAttaching failed[%d]" , nret );
		return nret;
	}
	else if( nret > 0 )
	{
		INFOLOGSG( "IBMACheckConfigSpaceAttaching ok , config_space changed" );
		
		IBACDisconnect( p_env );
		
		nret = IBACConnect( p_env , p_env->node ) ;
		if( nret )
		{
			ERRORLOGSG( "IBACConnect failed[%d]" , nret );
			return nret;
		}
	}
	
	ResetHttpSecureEnv( p_env->http_secure_env );
	
	if( p_env->netaddr.sock == -1 )
	{
		ERRORLOGSG( "no connection" );
		return IBP_IBAC_ERROR_NO_CLIENT_SOCKET;
	}
	
	p_env->p_this_app_unit = IBMAQueryApp( p_env->ibma_conf_space , app ) ;
	if( p_env->p_this_app_unit == NULL )
	{
		ERRORLOGSG( "app[%s] not exist" , app );
		return IBP_IBMA_ERROR_APP_NOT_EXIST;
	}
	else
	{
		DEBUGLOGSG( "app[%s] found" , app );
	}
	
	timeout = p_env->p_this_app_unit->timeout ;
	SetHttpTimeout( p_env->pre_http , timeout );
	ResetHttpEnv( p_env->pre_http ) ;
	ResetHttpSecureEnv( p_env->http_secure_env );
	INFOLOGSG( "set total timeout [%d]seconds" , timeout );
	
	first_send_flag = 1 ;
	
_RETRY_PRE_TRANSACTION :
	
	/* 请求前续交易 */
	pre_req_buf = GetHttpRequestBuffer( p_env->pre_http ) ;
	nret = StrcpyfHttpBuffer( pre_req_buf , "POST %s HTTP/1.0" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					"%s: %d" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					HTTP_RETURN_NEWLINE
					, IBP_URI_PRE_TRANSACTION
					, IBP_HTTPHEADER_IBP_CLIENT_NODE , p_env->p_this_node_unit->node
					, IBP_HTTPHEADER_IBP_SERVER_NODE , p_env->p_server_node_unit->node
					, IBP_HTTPHEADER_IBP_APP_CODE , p_env->p_this_app_unit->app
					, IBP_HTTPHEADER_IBP_TRANS_TIMEOUT , timeout
					, HTTP_HEADER_CONNECTION , HTTP_HEADER_CONNECTION__KEEPALIVE ) ;
	if( nret )
	{
		ERRORLOGSG( "app[%s] not exist" , app );
		return IBP_IBAC_ERROR_A_S(nret);
	}
	
	DEBUGHEXLOGSG( GetHttpBufferBase(pre_req_buf,NULL) , GetHttpBufferLength(pre_req_buf) , "HTTP [%d]BYTES" , GetHttpBufferLength(pre_req_buf) )
	
	nret = SendHttpRequest( p_env->netaddr.sock , NULL , p_env->pre_http ) ;
	if( nret )
	{
		ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
		_BEFORE_RETRY_PRE_TRANSACTION
	}
	else
	{
		INFOLOGSG( "SendHttpRequest[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	
	nret = ReceiveHttpResponse( p_env->netaddr.sock , NULL , p_env->pre_http ) ;
	if( nret )
	{
		ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
		_BEFORE_RETRY_PRE_TRANSACTION
	}
	else
	{
		INFOLOGSG( "ReceiveHttpResponse[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	
	pre_rsp_buf = GetHttpResponseBuffer( p_env->pre_http ) ;
	DEBUGHEXLOGSG( GetHttpBufferBase(pre_rsp_buf,NULL) , GetHttpBufferLength(pre_rsp_buf) , "HTTP [%d]BYTES" , GetHttpBufferLength(pre_rsp_buf) )
	
	status_code = GetHttpStatusCode(p_env->pre_http) ;
	if( status_code != HTTP_OK )
	{
		ERRORLOGSG( "GetHttpStatusCode return[%d]" , status_code );
		_BEFORE_RETRY_PRE_TRANSACTION
	}
	
	ResetHttpEnv( p_env->msg_http ) ;
	ResetHttpSecureEnv( p_env->http_secure_env );
	
	msg_req_buf = GetHttpRequestBuffer( p_env->msg_http ) ;
	nret = StrcpyfHttpBuffer( msg_req_buf , "POST %s HTTP/1.0" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					"%s: %d" HTTP_RETURN_NEWLINE
					"%s: %s" HTTP_RETURN_NEWLINE
					, IBP_URI_TRANSACTION
					, IBP_HTTPHEADER_IBP_CLIENT_NODE , p_env->p_this_node_unit->node
					, IBP_HTTPHEADER_IBP_SERVER_NODE , p_env->p_server_node_unit->node
					, IBP_HTTPHEADER_IBP_APP_CODE , p_env->p_this_app_unit->app
					, IBP_HTTPHEADER_IBP_TRANS_TIMEOUT , timeout
					, HTTP_HEADER_CONNECTION , HTTP_HEADER_CONNECTION__KEEPALIVE ) ;
	if( nret )
	{
		ERRORLOGSG( "app[%s] not exist" , app );
		IBACDisconnect( p_env );
		return IBP_IBAC_ERROR_A_S(nret);
	}
	
	/* 请求文件 */
	headers_req_filenames_count = 0 ;
	p_req_filename = (char*)va_arg( valist , char* ) ;
	while( p_req_filename )
	{
		DEBUGLOGSG( "headers_req_filenames_count[%d] p_req_filename[%s]" , headers_req_filenames_count , p_req_filename );
		if( headers_req_filenames_count >= IBP_MAXCNT_REQUEST_FILES )
		{
			ERRORLOGSG( "Too many request files" );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_TOO_MANY_REQUEST_FILES;
		}
		
		timeout = GetHttpElapse( p_env->pre_http )->tv_sec ;
		SetHttpTimeout( p_env->file_http , timeout );
		ResetHttpEnv( p_env->file_http ) ;
		ResetHttpSecureEnv( p_env->http_secure_env );
		INFOLOGSG( "set timeout [%d]seconds" , timeout );
		
		file_req_buf = GetHttpRequestBuffer( p_env->file_http ) ;
		nret = StrcpyfHttpBuffer( file_req_buf , "POST %s HTTP/1.0" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %d" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						, IBP_URI_PUT_FILE
						, IBP_HTTPHEADER_IBP_CLIENT_NODE , p_env->p_this_node_unit->node
						, IBP_HTTPHEADER_IBP_SERVER_NODE , p_env->p_server_node_unit->node
						, IBP_HTTPHEADER_IBP_TRANS_TIMEOUT , timeout
						, IBP_HTTPHEADER_IBP_FILE_NAME , p_req_filename
						, HTTP_HEADER_CONNECTION , HTTP_HEADER_CONNECTION__KEEPALIVE ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcpyfHttpBuffer failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		
		snprintf( pathfilename , sizeof(pathfilename)-1 , "%s/file/%s" , getenv("HOME") , p_req_filename );
		nret = PutFileToHttpBuffer( p_env->file_http , file_req_buf , p_env->http_secure_env , pathfilename , & file_len ) ;
		if( nret )
		{
			ERRORLOGSG( "PutFileToHttpBuffer[%s] failed[%d]" , pathfilename , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		else
		{
			INFOLOGSG( "PutFileToHttpBuffer[%s] ok , file_len[%d]" , pathfilename , file_len );
		}
		
		nret = SendHttpRequest( p_env->netaddr.sock , NULL , p_env->file_http ) ;
		if( nret )
		{
			ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		else
		{
			INFOLOGSG( "SendHttpRequest[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
		}
		
		nret = ReceiveHttpResponse( p_env->netaddr.sock , NULL , p_env->file_http ) ;
		if( nret )
		{
			ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		else
		{
			INFOLOGSG( "ReceiveHttpResponse[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
		}
		
		file_rsp_buf = GetHttpResponseBuffer( p_env->file_http ) ;
		DEBUGHEXLOGSG( GetHttpBufferBase(file_rsp_buf,NULL) , GetHttpBufferLength(file_rsp_buf) , "HTTP [%d]BYTES" , GetHttpBufferLength(file_rsp_buf) )
		
		status_code = GetHttpStatusCode(p_env->file_http) ;
		if( status_code != HTTP_OK )
		{
			ERRORLOGSG( "GetHttpStatusCode return[%d]" , status_code );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		
		p_req_filename = QueryHttpHeaderPtr( p_env->file_http , IBP_HTTPHEADER_IBP_FILE_NAME , & req_filename_len ) ;
		DEBUGLOGSG( "headers_req_filenames_count[%d] rename to p_req_filename[%.*s]" , headers_req_filenames_count , req_filename_len , p_req_filename );
		nret = StrcatfHttpBuffer( msg_req_buf , "%s-%d: %.*s" HTTP_RETURN_NEWLINE
							, IBP_HTTPHEADER_IBP_FILE_NAME , headers_req_filenames_count+1 , req_filename_len , p_req_filename ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		headers_req_filenames_count++;
		
		p_req_filename = (char*)va_arg( valist , char* ) ;
	}
	
	/* 请求交易 */
	timeout = GetHttpElapse( p_env->msg_http )->tv_sec ;
	SetHttpTimeout( p_env->msg_http , timeout );
	INFOLOGSG( "set timeout [%d]seconds" , timeout );
	
	nret = PutMessageToHttpBuffer( p_env->msg_http , msg_req_buf , p_env->http_secure_env , (*pp_msg) , (*p_msg_len) ) ;
	if( nret )
	{
		ERRORLOGSG( "PutMessageToHttpBuffer failed[%d]" , nret );
		IBACDisconnect( p_env );
		return IBP_IBAC_ERROR_A_S(nret);
	}
	
	p_env->comm_status = IBAC_COMM_STATUS_ON_SENDING ;
	
	nret = SendHttpRequest( p_env->netaddr.sock , NULL , p_env->msg_http ) ;
	if( nret )
	{
		ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
		IBACDisconnect( p_env );
		return IBP_IBAC_ERROR_A_S(nret);
	}
	else
	{
		INFOLOGSG( "SendHttpRequest[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	
	p_env->comm_status = IBAC_COMM_STATUS_AFTER_SENDING ;
	
	nret = ReceiveHttpResponse( p_env->netaddr.sock , NULL , p_env->msg_http ) ;
	if( nret )
	{
		ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
		IBACDisconnect( p_env );
		return IBP_IBAC_ERROR_A_S(nret);
	}
	else
	{
		INFOLOGSG( "ReceiveHttpResponse[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	
	msg_rsp_buf = GetHttpResponseBuffer( p_env->msg_http ) ;
	DEBUGHEXLOGSG( GetHttpBufferBase(msg_rsp_buf,NULL) , GetHttpBufferLength(msg_rsp_buf) , "HTTP [%d]BYTES" , GetHttpBufferLength(msg_rsp_buf) )
	
	status_code = GetHttpStatusCode(p_env->msg_http) ;
	if( status_code != HTTP_OK )
	{
		ERRORLOGSG( "GetHttpStatusCode return[%d]" , status_code );
		IBACDisconnect( p_env );
		return IBP_IBAC_ERROR_A_S(nret);
	}
	
	nret = ExtractMessageFromHttpBuffer( p_env->msg_http , GetHttpResponseBuffer(p_env->msg_http) , p_env->http_secure_env , pp_msg , p_msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed[%d]" , nret );
		IBACDisconnect( p_env );
		return HTTP_BAD_REQUEST;
	}
	
	nret = PutMessageToHttpBody( p_env->msg_http , GetHttpResponseBuffer(p_env->msg_http) , pp_msg , (*p_msg_len) ) ;
	if( nret )
	{
		ERRORLOGSG( "PutMessageToHttpBody failed[%d]" , nret );
		IBACDisconnect( p_env );
		return HTTP_BAD_REQUEST;
	}
	
	/* 响应交易 */
	valist_end_flag = 0 ;
	for( i = 0 ; i < IBP_MAXCNT_RESPONSE_FILES ; i++ )
	{
		memset( HTTPHEADER_name , 0x00 , sizeof(HTTPHEADER_name) );
		sprintf( HTTPHEADER_name , "%s-%d" , IBP_HTTPHEADER_IBP_FILE_NAME , i+1 );
		p_rsp_filename = QueryHttpHeaderPtr( p_env->msg_http , HTTPHEADER_name , & rsp_filelen ) ;
		if( p_rsp_filename == NULL )
			break;
		if( rsp_filelen > IBP_MAXLEN_FILENAME )
		{
			ERRORLOGSG( "filename[%.*s] to long" , rsp_filelen , p_rsp_filename );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		
		DEBUGLOGSG( "rsp_filename[%.*s]" , rsp_filelen , p_rsp_filename );
		
		timeout = GetHttpElapse( p_env->msg_http )->tv_sec ;
		SetHttpTimeout( p_env->file_http , timeout );
		ResetHttpEnv( p_env->file_http ) ;
		ResetHttpSecureEnv( p_env->http_secure_env );
		INFOLOGSG( "set timeout [%d]seconds" , timeout );
		
		file_req_buf = GetHttpRequestBuffer( p_env->file_http ) ;
		nret = StrcpyfHttpBuffer( file_req_buf , "POST %s HTTP/1.0" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						"%s: %d" HTTP_RETURN_NEWLINE
						"%s: %.*s" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						HTTP_RETURN_NEWLINE
						, IBP_URI_GET_FILE
						, IBP_HTTPHEADER_IBP_CLIENT_NODE , p_env->p_this_node_unit->node
						, IBP_HTTPHEADER_IBP_SERVER_NODE , p_env->p_server_node_unit->node
						, IBP_HTTPHEADER_IBP_TRANS_TIMEOUT , timeout
						, IBP_HTTPHEADER_IBP_FILE_NAME , rsp_filelen , p_rsp_filename
						, HTTP_HEADER_CONNECTION , HTTP_HEADER_CONNECTION__KEEPALIVE ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcpyfHttpBuffer failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		
		DEBUGHEXLOGSG( GetHttpBufferBase(file_req_buf,NULL) , GetHttpBufferLength(file_req_buf) , "HTTP [%d]BYTES" , GetHttpBufferLength(file_req_buf) )
		
		nret = SendHttpRequest( p_env->netaddr.sock , NULL , p_env->file_http ) ;
		if( nret )
		{
			ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		else
		{
			INFOLOGSG( "SendHttpRequest[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
		}
		
		nret = ReceiveHttpResponse( p_env->netaddr.sock , NULL , p_env->file_http ) ;
		if( nret )
		{
			ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		else
		{
			INFOLOGSG( "ReceiveHttpResponse[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
		}
		
		status_code = GetHttpStatusCode(p_env->file_http) ;
		if( status_code != HTTP_OK )
		{
			ERRORLOGSG( "GetHttpStatusCode return[%d]" , status_code );
			IBACDisconnect( p_env );
			return IBP_IBAC_ERROR_A_S(nret);
		}
		
		file_content = GetHttpBodyPtr( p_env->file_http , & file_len ) ;
		
		memset( file_id , 0x00 , sizeof(file_id) );
		snprintf( file_id , sizeof(file_id)-1 , "%.*s" , rsp_filelen , p_rsp_filename );
		TrimFileId( file_id );
		memset( filename , 0x00 , sizeof(filename) );
		memset( pathfilename , 0x00 , sizeof(pathfilename) );
		fp = IBPCreateTempFile( file_id , filename , pathfilename , "wb" ) ;
		if( fp == NULL )
		{
			ERRORLOGSG( "IBPCreateTempFile failed , errno[%d]" , errno );
			IBACDisconnect( p_env );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		fclose( fp );
		
		file_rsp_buf = GetHttpResponseBuffer( p_env->file_http ) ;
		nret = ExtractFileFromHttpBuffer( p_env->file_http , file_rsp_buf , p_env->http_secure_env , pathfilename , & file_len ) ;
		if( nret )
		{
			ERRORLOGSG( "ExtractFileFromHttpBuffer[%s] [%d]bytes failed , errno[%d]" , pathfilename , file_len , errno );
			IBACDisconnect( p_env );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		else
		{
			INFOLOGSG( "ExtractFileFromHttpBuffer[%s] [%d]bytes ok" , pathfilename , file_len );
		}
		
		if( valist_end_flag == 0 )
		{
			p_rsp_filename = (char*)va_arg( valist , char* ) ;
			if( p_rsp_filename )
			{
				strcpy( p_rsp_filename , filename );
			}
			else
			{
				valist_end_flag = 1 ;
			}
		}
	}
	
	ResetHttpSecureEnv( p_env->http_secure_env );
	
	return 0;
}

int IBACSendRequestAndReceiveResponse( struct IbacEnv *p_env , int keep_alive_flag , char *app , char **pp_msg , int *p_msg_len , ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , p_msg_len );
	nret = IBACSendRequestAndReceiveResponseV( p_env , keep_alive_flag , app , pp_msg , p_msg_len , valist ) ;
	va_end( valist );
	return nret;
}

int IBACDisconnect( struct IbacEnv *p_env )
{
	if( p_env )
	{
		if( p_env->netaddr.sock == -1 )
		{
			ERRORLOGSG( "connection already closed" );
			return IBP_IBAC_ERROR_NO_CLIENT_SOCKET;
		}
		
		close( p_env->netaddr.sock );
		p_env->netaddr.sock = -1 ;
		
		INFOLOGSG( "diconnect[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	
	return 0;
}

int IBACRequesterV( struct IbacEnv *p_env , char *node , char *app , char **pp_msg , int *p_msg_len , va_list valist )
{
	int			nret = 0 ;
	
	nret = IBACConnect( p_env , node ) ;
	if( nret )
	{
		ERRORLOGSG( "IBACConnect failed[%d]" , nret );
		return nret;
	}
	
	nret = IBACSendRequestAndReceiveResponseV( p_env , 0 , app , pp_msg , p_msg_len , valist ) ;
	if( nret )
	{
		ERRORLOGSG( "IBACSendRequestAndReceiveResponseV failed[%d]" , nret );
		return nret;
	}
	
	nret = IBACDisconnect( p_env ) ;
	if( nret )
	{
		ERRORLOGSG( "IBACDisconnect failed[%d]" , nret );
		return nret;
	}
	
	return 0;
}

int IBACRequester( struct IbacEnv *p_env , char *node , char *app , char **pp_msg , int *p_msg_len , ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , p_msg_len );
	nret = IBACRequesterV( p_env , node , app , pp_msg , p_msg_len , valist ) ;
	va_end( valist );
	return nret;
}

int IBACCallV( char *ibma_conf_filename , char *node , char *app , char **pp_msg , int *p_msg_len , va_list valist )
{
	struct IbacEnv		*p_env = NULL ;
	
	int			nret = 0 ;
	
	p_env = IBACCreateEnvirment( ibma_conf_filename ) ;
	if( p_env == NULL )
	{
		ERRORLOGSG( "IBACCreateEnvirment failed[%d]" , nret );
		return IBP_IBAC_ERROR_ALLOC;
	}
	
	nret = IBACRequesterV( p_env , node , app , pp_msg , p_msg_len , valist ) ;
	if( nret )
	{
		ERRORLOGSG( "IBACRequester failed[%d]" , nret );
	}
	
	IBACDestroyEnvirment( p_env );
	INFOLOGSG( "IBACDestroyEnvirment ok" );
	
	return nret;
}

int IBACCall( char *ibma_conf_filename , char *node , char *app , char **pp_msg , int *p_msg_len , ... )
{
	va_list		valist ;
	
	int		nret = 0 ;
	
	va_start( valist , p_msg_len );
	nret = IBACCallV( ibma_conf_filename , node , app , pp_msg , p_msg_len , valist ) ;
	va_end( valist );
	return nret;
}

void IBACResetCommStatus( struct IbacEnv *p_env )
{
	p_env->comm_status = IBAC_COMM_STATUS_BEFORE_SENDING ;
	
	return;
}

int IBACGetCommStatus( struct IbacEnv *p_env )
{
	return p_env->comm_status;
}

