STRUCT					ibma_conf
{
	STRUCT				ibma
	{
		STRING 32		node
		
		STRUCT	log
		{
			STRING 256	iblog_server
			STRING 256	event_output
			STRING 256	main_output
			STRING 6	main_loglevel
			STRING 256	worker_output
			STRING 6	worker_loglevel
		}
	}
	
	STRUCT				ibac
	{
		INT 4			retry_connect_timeval
	}
	
	STRUCT				ibms
	{
		STRUCT			server
		{
			STRING 20	ip
			INT 4		port
		}
	}
}

