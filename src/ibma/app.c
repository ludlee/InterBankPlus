#include "ibma_in.h"

int OnProcess( struct IbmaEnv *p_env )
{
	char			*uri = NULL ;
	int			uri_len ;
	
	int			nret = 0 ;
	
	uri = GetHttpHeaderPtr_URI( p_env->http , & uri_len ) ;
	INFOLOGSG( "uri[%.*s]" , uri_len , uri );
	if( uri_len == sizeof(IBP_URI_DOWNLOAD_CONFIG)-1 && MEMCMP( uri , == , IBP_URI_DOWNLOAD_CONFIG , uri_len ) )
	{
		nret = OnProcessBuildConfigSpace( p_env , GetHttpRequestBuffer(p_env->http) ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessBuildConfigSpace failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessBuildConfigSpace ok" );
		}
	}
#if 0
	else if( uri_len == sizeof(IBP_URI_ALIVE_NOTICE)-1 && MEMCMP( uri , == , IBP_URI_ALIVE_NOTICE , uri_len ) )
	{
		nret = OnProcessAliveNotice( p_env ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "OnProcessAliveNotice failed[%d]" , nret );
			return nret;
		}
		else
		{
			INFOLOGSG( "OnProcessAliveNotice ok" );
		}
	}
#endif
	else
	{
		return HTTP_NOT_FOUND;
	}
	
	return HTTP_OK;
}

static int DownloadAppBin( struct IbmaEnv *p_env , ibp_fetch_config_rspmsg *pst )
{
	int			i ;
	char			*p = NULL ;
	char			so_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			md5_exp[ 32 + 1 ] ;
	ibms_app_reqmsg		ibms_app ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	struct HttpBuffer	*req_buf = NULL ;
	struct HttpBuffer	*rsp_buf = NULL ;
	int			status_code ;
	
	int			nret = 0 ;
	
	for( i = 0 ; i < pst->_apps_count ; i++ )	
	{
		p = getenv("IBAS_SO_PATH") ;
		memset( so_pathfilename , 0x00 , sizeof(so_pathfilename) );
		if( p )
			snprintf( so_pathfilename , sizeof(so_pathfilename)-1 , "%s/%s" , p , pst->apps[i].bin );
		else
			snprintf( so_pathfilename , sizeof(so_pathfilename)-1 , "%s/so/%s" , getenv("HOME") , pst->apps[i].bin );
		
		if( access( so_pathfilename , F_OK ) == 0 )
		{
			memset( md5_exp , 0x00 , sizeof(md5_exp) );
			nret = Md5File( so_pathfilename , md5_exp ) ;
			if( nret )
			{
				ERRORLOGSG( "Md5File[%s] failed[%d]" , so_pathfilename , nret );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			
			if( STRCMP( md5_exp , == , pst->apps[i].md5 ) )
				continue;
			else
				INFOLOGSG( "bin[%s] md5 not match" , so_pathfilename );
		}
		else
		{
			INFOLOGSG( "bin[%s] not exist" , so_pathfilename );
		}
		
		memset( & ibms_app , 0x00 , sizeof(ibms_app_reqmsg) );
		strncpy( ibms_app.bin , pst->apps[i].bin , sizeof(ibms_app.bin)-1 );
		req_msg = NULL ;
		nret = DSCSERIALIZE_JSON_DUP_ibms_app_reqmsg( & ibms_app , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
		if( nret )
		{
			ERRORLOGSG( "DSCSERIALIZE_JSON_DUP_ibms_app_reqmsg failed , errno[%d]" , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		ResetHttpEnv( p_env->http );
		ResetHttpSecureEnv( p_env->http_secure_env );
		
		req_buf = GetHttpRequestBuffer( p_env->http ) ;
		nret = StrcpyfHttpBuffer( req_buf , "POST " IBP_URI_FETCH_BIN " HTTP/1.0" HTTP_RETURN_NEWLINE
						"%s: %s" HTTP_RETURN_NEWLINE
						, HTTP_HEADER_CONNECTION , HTTP_HEADER_CONNECTION__KEEPALIVE ) ;
		if( nret )
		{
			ERRORLOGSG( "StrcatfHttpBuffer failed[%d]" , nret );
			free( req_msg );
			DisconnectFromIbmsServer( p_env );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = PutMessageToHttpBuffer( p_env->http , req_buf , p_env->http_secure_env , req_msg , req_msg_len  ) ;
		free( req_msg );
		if( nret )
		{
			ERRORLOGSG( "PutMessageToHttpBuffer failed[%d]" , nret );
			DisconnectFromIbmsServer( p_env );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		SetHttpTimeout( p_env->http , 60 );
		
		nret = SendHttpRequest( p_env->netaddr.sock , NULL , p_env->http ) ;
		if( nret )
		{
			ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
			DisconnectFromIbmsServer( p_env );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		nret = ReceiveHttpResponse( p_env->netaddr.sock , NULL , p_env->http ) ;
		if( nret )
		{
			ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
			DisconnectFromIbmsServer( p_env );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		rsp_buf = GetHttpResponseBuffer( p_env->http ) ;
		DEBUGHEXLOGSG( GetHttpBufferBase(rsp_buf,NULL) , GetHttpBufferLength(rsp_buf) , "ResponseBuffer" );
		
		status_code = GetHttpStatusCode( p_env->http ) ;
		if( status_code != HTTP_OK )
		{
			ERRORLOGSG( "GetHttpStatusCode return[%d]" , status_code );
			return status_code;
		}
		
		nret = ExtractFileFromHttpBuffer( p_env->http , rsp_buf , p_env->http_secure_env , so_pathfilename , NULL ) ;
		if( nret )
		{
			ERRORLOGSG( "ExtractFileFromHttpBuffer[%s] failed[%d] , errno[%d]" , so_pathfilename , nret , errno );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		memset( md5_exp , 0x00 , sizeof(md5_exp) );
		nret = Md5File( so_pathfilename , md5_exp ) ;
		if( nret )
		{
			ERRORLOGSG( "Md5File[%s] failed[%d]" , so_pathfilename , nret );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		if( STRCMP( md5_exp , != , pst->apps[i].md5 ) )
		{
			ERRORLOGSG( "bin[%s] md5 not match too" , so_pathfilename );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		INFOLOGSG( "fetch bin[%s] md5[%s] ok" , so_pathfilename , md5_exp );
	}
	
	return HTTP_OK;
}

static int RefreshConfigSpace( struct IbmaEnv *p_env , ibp_fetch_config_rspmsg *pst , struct ConfigSpace *p_config_space )
{
	int			i , j ;
	unsigned long		hash_val ;
	struct NodeSpaceUnit	*p_node_space_unit = NULL ;
	struct HostSpaceUnit	*p_current_host_space_unit = p_config_space->config_space_addresses.hosts_array_base ;
	struct AppSpaceUnit	*p_app_space_unit = NULL ;
	
	int			nret = 0 ;
	
	for( i = 0 ; i < pst->_nodes_count ; i++ )
	{
		hash_val = CalcHash(pst->nodes[i].node) % (p_config_space->config_space_attaching->nodes_hash_size) ;
		p_node_space_unit = p_config_space->config_space_addresses.nodes_hash_base + hash_val ;
		for( j = 0 ; j < p_config_space->config_space_attaching->nodes_hash_size ; j++ , p_node_space_unit++ )
		{
			if( p_node_space_unit > p_config_space->config_space_addresses.last_node_unit )
				p_node_space_unit = p_config_space->config_space_addresses.nodes_hash_base ;
			
			if( p_node_space_unit->node[0] == '\0' )
			{
				strncpy( p_node_space_unit->node , pst->nodes[i].node , sizeof(p_node_space_unit->node)-1 );
				p_node_space_unit->invalid = pst->nodes[i].invalid ;
				p_node_space_unit->host_space_base_offset = p_current_host_space_unit - p_config_space->config_space_addresses.hosts_array_base ;
				p_node_space_unit->host_count = pst->nodes[i]._hosts_count ;
				
				p_node_space_unit->key_len = sizeof(p_node_space_unit->key)-1 ;
				nret = Decoding_BASE64( pst->nodes[i].key_base64 , strlen(pst->nodes[i].key_base64) , p_node_space_unit->key , & (p_node_space_unit->key_len) ) ;
				if( nret )
				{
					ERRORLOGSG( "Decoding_BASE64 failed[%d]" , nret );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				
				break;
			}
		}
		if( j >= p_config_space->config_space_attaching->nodes_hash_size )
		{
			FATALLOGSG( "node space not enough" );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		for( j = 0 ; j < pst->nodes[i]._hosts_count ; j++ )	
		{
			strncpy( p_current_host_space_unit->ip , pst->nodes[i].hosts[j].ip , sizeof(p_current_host_space_unit->ip)-1 );
			p_current_host_space_unit->port = pst->nodes[i].hosts[j].port ;
			p_current_host_space_unit->load = 0 ;
			p_current_host_space_unit->invalid = pst->nodes[i].hosts[j].invalid ;
			p_current_host_space_unit++;
		}
	}
	
	for( i = 0 ; i < pst->_apps_count ; i++ )
	{
		hash_val = CalcHash(pst->apps[i].app) % (p_config_space->config_space_attaching->apps_hash_size) ;
		p_app_space_unit = p_config_space->config_space_addresses.apps_hash_base + hash_val ;
		for( j = 0 ; j < p_config_space->config_space_attaching->apps_hash_size ; j++ , p_app_space_unit++ )
		{
			if( p_app_space_unit > p_config_space->config_space_addresses.last_app_unit )
				p_app_space_unit = p_config_space->config_space_addresses.apps_hash_base ;
			
			if( p_app_space_unit->app[0] == '\0' )
			{
				strncpy( p_app_space_unit->app , pst->apps[i].app , sizeof(p_app_space_unit->app)-1 );
				strncpy( p_app_space_unit->desc , pst->apps[i].desc , sizeof(p_app_space_unit->desc)-1 );
				strncpy( p_app_space_unit->bin , pst->apps[i].bin , sizeof(p_app_space_unit->bin)-1 );
				p_app_space_unit->timeout = pst->apps[i].timeout ;
				p_app_space_unit->timeout2 = pst->apps[i].timeout2 ;
				break;
			}
		}
		if( j >= p_config_space->config_space_attaching->apps_hash_size )
		{
			FATALLOGSG( "app space not enough" );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
	}
	
	return HTTP_OK;
}

int OnProcessBuildConfigSpace( struct IbmaEnv *p_env , struct HttpBuffer *b )
{
	ibp_fetch_config_rspmsg	*p_ibp_fetch_config = NULL ;
	char				*rsp_msg = NULL ;
	int				rsp_msg_len ;
	
	struct ConfigSpaceAttaching	config_space_attaching ;
	struct ConfigSpace		config_space ;
	struct ConfigSpace		*p_config_space = NULL ;
	int				i ;
	
	int				nret = 0 ;
	
	/* 解包下发配置 */
	p_ibp_fetch_config = (ibp_fetch_config_rspmsg *)malloc( sizeof(ibp_fetch_config_rspmsg) ) ;
	if( p_ibp_fetch_config == NULL )
	{
		ERRORLOGSG( "malloc failed , errno[%d]" , errno );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	memset( p_ibp_fetch_config , 0x00 , sizeof(ibp_fetch_config_rspmsg) );
	
	nret = ExtractMessageFromHttpBuffer( p_env->http , b , p_env->http_secure_env , & rsp_msg , & rsp_msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "ExtractMessageFromHttpBuffer failed[%d]" , nret );
		free( p_ibp_fetch_config );
		return HTTP_SERVICE_UNAVAILABLE;
	}
	
	nret = DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg( "GB18030" , rsp_msg , & rsp_msg_len , p_ibp_fetch_config ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg failed[%d]" , nret );
		free( p_ibp_fetch_config );
		return HTTP_BAD_REQUEST;
	}
	else
	{
		INFOLOGSG( "DSCDESERIALIZE_JSON_ibp_fetch_config_rspmsg ok" );
	}
	
	if( p_ibp_fetch_config->response_code != 0 )
	{
		ERRORLOGSG( "%s" , p_ibp_fetch_config->response_desc );
		free( p_ibp_fetch_config );
		return HTTP_SERVICE_UNAVAILABLE;
	}
	
	/* 请求下发所有应用包 */
	nret = DownloadAppBin( p_env , p_ibp_fetch_config ) ;
	if( nret != HTTP_OK )
	{
		ERRORLOGSG( "DownloadAppBin failed[%d]" , nret );
		free( p_ibp_fetch_config );
		return nret;
	}
	
	/* 计算配置共享内存总大小 */
	memset( & config_space_attaching , 0x00 , sizeof(struct ConfigSpaceAttaching) );
	config_space_attaching.nodes_hash_size = 0 ;
	config_space_attaching.hosts_array_size = 0 ;
	for( i = 0 ; i < p_ibp_fetch_config->_nodes_count ; i++ )
	{
		config_space_attaching.nodes_hash_size++;
		config_space_attaching.hosts_array_size += p_ibp_fetch_config->nodes[i]._hosts_count ;
	}
	INFOLOGSG( "nodes_hash_size[%d]=[%d]*[%d]" , config_space_attaching.nodes_hash_size*HASH_EXPANSION_FACTOR , config_space_attaching.nodes_hash_size , HASH_EXPANSION_FACTOR );
	INFOLOGSG( "hosts_array_size[%d]" , config_space_attaching.hosts_array_size );
	config_space_attaching.nodes_hash_size *= HASH_EXPANSION_FACTOR ;
	
	config_space_attaching.apps_hash_size = 0 ;
	for( i = 0 ; i < p_ibp_fetch_config->_apps_count ; i++ )
	{
		config_space_attaching.apps_hash_size++;
	}
	INFOLOGSG( "apps_hash_size[%d]=[%d]*[%d]" , config_space_attaching.apps_hash_size*HASH_EXPANSION_FACTOR , config_space_attaching.apps_hash_size , HASH_EXPANSION_FACTOR );
	config_space_attaching.apps_hash_size *= HASH_EXPANSION_FACTOR ;
	
	config_space.shm.size = sizeof(struct ConfigSpaceAttaching) + config_space_attaching.nodes_hash_size * sizeof(struct NodeSpaceUnit) + config_space_attaching.hosts_array_size * sizeof(struct HostSpaceUnit) + config_space_attaching.apps_hash_size * sizeof(struct AppSpaceUnit) ;
	INFOLOGSG( "total_size[%d]=[%d]+[%d]*[%d]+[%d]*[%d]+[%d]*[%d]" , config_space.shm.size , sizeof(struct ConfigSpaceAttaching) , config_space_attaching.nodes_hash_size , sizeof(struct NodeSpaceUnit) , config_space_attaching.hosts_array_size , sizeof(struct HostSpaceUnit) , config_space_attaching.apps_hash_size , sizeof(struct AppSpaceUnit) );
	
	/* 构造配置共享内存 */
	p_env->current_proj_id = 1 ;
	for( i = 0 ; i < 256 ; i++ , p_env->current_proj_id++ )
	{
		if( p_env->current_proj_id > 255 )
			p_env->current_proj_id = 1 ;
		
		/* 创建共享内存 */
		config_space.shm.shmkey = ftok( p_env->ibma_conf_pathfilename , p_env->current_proj_id ) ;
		if( config_space.shm.shmkey == -1 )
		{
			ERRORLOGSG( "ftok[%s][%d] failed , errno[%d]" , p_env->ibma_conf_pathfilename , p_env->current_proj_id , errno );
			free( p_ibp_fetch_config );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		config_space.shm.shmid = shmget( config_space.shm.shmkey , config_space.shm.size , IPC_CREAT|IPC_EXCL|0644 ) ;
		if( config_space.shm.shmid == -1 )
		{
			if( errno == EEXIST ) /* 如果共享内存存在则常识以下一个key创建 */
				continue;
			
			ERRORLOGSG( "shmget[0x%X] failed , errno[%d]" , config_space.shm.shmkey , errno );
			free( p_ibp_fetch_config );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		else
		{
			INFOLOGSG( "shmget[0x%X] ok , shmid[%d]" , config_space.shm.shmkey , config_space.shm.shmid );
		}
		
		config_space.shm.base = shmat( config_space.shm.shmid , NULL , 0 ) ;
		if( config_space.shm.base == NULL )
		{
			ERRORLOGSG( "shmat[%d] failed , errno[%d]" , config_space.shm.shmid , errno );
			free( p_ibp_fetch_config );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		else
		{
			INFOLOGSG( "shmat[%d] ok" , config_space.shm.shmid );
		}
		memset( config_space.shm.base , 0x00 , config_space.shm.size );
		
		/* 格式化配置共享内存分区 */
		memcpy( config_space.shm.base , & config_space_attaching , sizeof(struct ConfigSpaceAttaching) );
		LocateConfigSpaceAddress( config_space.shm.base , & (config_space.config_space_attaching) , & (config_space.config_space_addresses) );
		
		/* 填充数据到配置共享内存 */
		nret = RefreshConfigSpace( p_env , p_ibp_fetch_config , & config_space ) ;
		if( nret != HTTP_OK )
		{
			ERRORLOGSG( "RefreshConfigSpace failed[%d]" , nret );
			free( p_ibp_fetch_config );
			return nret;
		}
		else
		{
			INFOLOGSG( "RefreshConfigSpace ok" );
		}
		
		/* 追加配置共享内存到已连接配置共享内存列表中 */
		p_config_space = (struct ConfigSpace *)malloc( sizeof(struct ConfigSpace) ) ;
		if( p_config_space == NULL )
		{
			ERRORLOGSG( "malloc failed , errno[%d]" , errno );
			free( p_ibp_fetch_config );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		memcpy( p_config_space , & config_space , sizeof(struct ConfigSpace) );
		
		p_env->p_current_config_space = p_config_space ;
		
		/* 刷新索引共享内存指向 */
		p_env->index_space.index_space_attaching->shmkey = p_env->p_current_config_space->shm.shmkey ;
		
		list_add_tail( & (p_config_space->this_node) , & (p_env->config_space_list.this_node) );
		INFOLOGSG( "add config_space[0x%X][%d]" , p_config_space->shm.shmkey , p_config_space->shm.shmid );
		
		break;
	}
	if( i >= 256 )
	{
		ERRORLOGSG( "too many sharememory" );
		free( p_ibp_fetch_config );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	free( p_ibp_fetch_config );
	
	return HTTP_OK;
}

#if 0
int OnProcessAliveNotice( struct IbmaEnv *p_env )
{
	ibp_alive_notice_reqmsg		alive_notice ;
	char				*reqmsg = NULL ;
	int				reqmsg_len ;
	
	struct NodeSpaceUnit		*p_node_unit = NULL ;
	struct HostSpaceUnit		*p_host_unit = NULL ;
	
	int				nret = 0 ;
	
	reqmsg = GetHttpBodyPtr( p_env->http , & reqmsg_len ) ;
	DEBUGLOGSG( "reqmsg[%s]" , reqmsg );
	
	memset( & alive_notice , 0x00 , sizeof(ibp_alive_notice_reqmsg) );
	nret = DSCDESERIALIZE_JSON_ibp_alive_notice_reqmsg( "GB18030" , reqmsg , & reqmsg_len , & alive_notice ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibp_alive_notice_reqmsg failed[%d]" , nret );
		return HTTP_BAD_REQUEST;
	}
	else
	{
		INFOLOGSG( "DSCDESERIALIZE_JSON_ibp_alive_notice_reqmsg ok" );
	}
	
	p_node_unit = QueryNode( p_env->p_current_config_space->config_space_attaching , & (p_env->p_current_config_space->config_space_addresses) , alive_notice.node ) ;
	if( p_node_unit == NULL )
	{
		ERRORLOGSG( "node[%s] not found" , alive_notice.node );
		return HTTP_NOT_FOUND;
	}
	
	p_host_unit = QueryNodeHost( & (p_env->p_current_config_space->config_space_addresses) , p_node_unit , alive_notice.ip , alive_notice.port ) ;
	if( p_host_unit == NULL )
	{
		ERRORLOGSG( "node[%s] host[%s:%d] not found" , alive_notice.node , alive_notice.ip , alive_notice.port );
		return HTTP_NOT_FOUND;
	}
	
	p_host_unit->invalid = alive_notice.invalid ;
	
	return HTTP_OK;
}
#endif

