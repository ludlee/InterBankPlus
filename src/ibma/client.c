#include "ibma_in.h"
#include "ibma_api.h"

int ShowThisNode( char *config_filename )
{
	struct IbmaConfigSpace	*ibma_conf_space = NULL ;
	
	ibma_conf_space = IBMAOpenConfigSpace( config_filename ) ;
	if( ibma_conf_space == NULL )
	{
		printf( "ERROR : IBMAOpenConfigSpace failed\n" );
		return -1;
	}
	
	printf( "%s\n" , IBMAGetThisNodePtr(ibma_conf_space) );
	
	IBMACloseConfigSpace( ibma_conf_space );
	
	return 0;
}

int ShowNodes( char *config_filename )
{
	struct IbmaConfigSpace	*ibma_conf_space = NULL ;
	struct NodeSpaceUnit	*p_node_space_unit = NULL ;
	struct HostSpaceUnit	*p_host_space_unit = NULL ;
	
	ibma_conf_space = IBMAOpenConfigSpace( config_filename ) ;
	if( ibma_conf_space == NULL )
	{
		printf( "ERROR : IBMAOpenConfigSpace failed\n" );
		return -1;
	}
	
	p_node_space_unit = IBMATravelNodes( ibma_conf_space , NULL ) ;
	while( p_node_space_unit )
	{
		printf( "%s %d" , IBMAGetNodePtr(p_node_space_unit) , IBMAGetNodeInvalid(p_node_space_unit) );
		
		p_host_space_unit = IBMATravelNodeHosts( ibma_conf_space , p_node_space_unit , NULL ) ;
		while( p_host_space_unit )
		{
#if 0
			printf( " %s:%d(%d)" , IBMAGetHostIpPtr(p_host_space_unit) , IBMAGetHostPort(p_host_space_unit) , IBMAGetHostInvalid(p_host_space_unit) );
#endif
			printf( " %s:%d" , IBMAGetHostIpPtr(p_host_space_unit) , IBMAGetHostPort(p_host_space_unit) );
			
			p_host_space_unit = IBMATravelNodeHosts( ibma_conf_space , p_node_space_unit , p_host_space_unit ) ;
		}
		
		printf( "\n" );
		
		p_node_space_unit = IBMATravelNodes( ibma_conf_space , p_node_space_unit ) ;
	}
	
	IBMACloseConfigSpace( ibma_conf_space );
	
	return 0;
}

int ShowApps( char *config_filename )
{
	struct IbmaConfigSpace	*ibma_conf_space = NULL ;
	struct AppSpaceUnit	*p_apibma_conf_space_unit = NULL ;
	
	ibma_conf_space = IBMAOpenConfigSpace( config_filename ) ;
	if( ibma_conf_space == NULL )
	{
		printf( "ERROR : IBMAOpenConfigSpace failed\n" );
		return -1;
	}
	
	p_apibma_conf_space_unit = IBMATravelApps( ibma_conf_space , NULL ) ;
	while( p_apibma_conf_space_unit )
	{
		printf( "%s %s %s %d %d\n" , IBMAGetAppPtr(p_apibma_conf_space_unit) , IBMAGetAppDescPtr(p_apibma_conf_space_unit) , IBMAGetAppBinPtr(p_apibma_conf_space_unit) , IBMAGetAppTimeout(p_apibma_conf_space_unit) , IBMAGetAppTimeout2(p_apibma_conf_space_unit) );
		
		p_apibma_conf_space_unit = IBMATravelApps( ibma_conf_space , p_apibma_conf_space_unit ) ;
	}
	
	IBMACloseConfigSpace( ibma_conf_space );
	
	return 0;
}

