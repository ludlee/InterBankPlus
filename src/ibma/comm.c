#include "ibma_in.h"

int ConnectToIbmsServer( struct IbmaEnv *p_env )
{
	ibms_node_reqmsg	ibms_node ;
	char			*req_msg = NULL ;
	int			req_msg_len ;
	struct HttpBuffer	*req_buf = NULL ;
	char			*STATUSCODE = NULL ;
	int			STATUSCODELEN ;
	
	int			nret = 0 ;
	
	p_env->netaddr.sock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ;
	if( p_env->netaddr.sock == -1 )
	{
		ERRORLOGSG( "socket failed , errno[%d]" , errno );
		return -1;
	}
	
	SetHttpNodelay( p_env->netaddr.sock , 1 );
	
	SETNETADDRESS( p_env->netaddr )
	nret = connect( p_env->netaddr.sock , (struct sockaddr *) & (p_env->netaddr.addr) , sizeof(struct sockaddr) ) ;
	if( nret == -1 )
	{
		ERRORLOGSG( "connect[%s:%d] failed , errno[%d]" , p_env->netaddr.ip , p_env->netaddr.port , errno );
		DisconnectFromIbmsServer( p_env );
		return -2;
	}
	else
	{
		INFOLOGSG( "connect[%s:%d] ok" , p_env->netaddr.ip , p_env->netaddr.port );
	}
	GETNETADDRESS_LOCAL( p_env->netaddr )
	
	ResetHttpEnv( p_env->http );
	ResetHttpSecureEnv( p_env->http_secure_env );
	
	memset( & ibms_node , 0x00 , sizeof(ibms_node_reqmsg) );
	strncpy( ibms_node.node , p_env->ibma_conf.ibma.node , sizeof(ibms_node.node)-1 );
	nret = DSCSERIALIZE_JSON_DUP_ibms_node_reqmsg( & ibms_node , "GB18030" , & req_msg , NULL , & req_msg_len ) ;
	if( nret )
	{
		ERRORLOGSG( "DSCSERIALIZE_JSON_DUP_ibms_node_reqmsg failed[%d] , errno[%d]" , nret , errno );
		DisconnectFromIbmsServer( p_env );
		return nret;
	}
	
	req_buf = GetHttpRequestBuffer( p_env->http ) ;
	nret = StrcpyfHttpBuffer( req_buf , "POST " IBP_URI_FETCH_CONFIG " HTTP/1.0" HTTP_RETURN_NEWLINE
						"Connection: Keep-alive" HTTP_RETURN_NEWLINE ) ;
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d]" , nret );
		free( req_msg );
		DisconnectFromIbmsServer( p_env );
		return nret;
	}
	
	nret = PutMessageToHttpBuffer( p_env->http , req_buf , p_env->http_secure_env , req_msg , req_msg_len ) ;
	free( req_msg );
	if( nret )
	{
		ERRORLOGSG( "StrcatfHttpBuffer failed[%d]" , nret );
		DisconnectFromIbmsServer( p_env );
		return nret;
	}
	
	SetHttpTimeout( p_env->http , 60 );
	
	nret = SendHttpRequest( p_env->netaddr.sock , NULL , p_env->http ) ;
	if( nret )
	{
		ERRORLOGSG( "SendHttpRequest failed[%d]" , nret );
		DisconnectFromIbmsServer( p_env );
		return nret;
	}
	
	nret = ReceiveHttpResponse( p_env->netaddr.sock , NULL , p_env->http ) ;
	if( nret )
	{
		ERRORLOGSG( "ReceiveHttpResponse failed[%d]" , nret );
		DisconnectFromIbmsServer( p_env );
		return nret;
	}
	
	STATUSCODE = GetHttpHeaderPtr_STATUSCODE( p_env->http , & STATUSCODELEN ) ;
	if( STATUSCODELEN != 3 || STRNCMP( STATUSCODE , != , "200" , 3 ) )
	{
		ERRORLOGSG( "STATUSCODE[%.*s]" , STATUSCODELEN , STATUSCODE );
		DisconnectFromIbmsServer( p_env );
		return -1;
	}
	
	{
	char		*STATUS_CODE = NULL ;
	int		STATUS_CODE_LEN ;
	
	STATUS_CODE = GetHttpHeaderPtr_STATUSCODE( p_env->http , & STATUS_CODE_LEN );
	
	NOTICELOGSG( "[%s][%s:%d] [POST " IBP_URI_FETCH_CONFIG " HTTP/1.0] -> [%s][%s:%d] [%.*s]"
			, "ibms" , p_env->netaddr.local_ip , p_env->netaddr.local_port
			, p_env->ibma_conf.ibma.node , p_env->netaddr.ip , p_env->netaddr.port
			, STATUS_CODE_LEN , STATUS_CODE );
	}
	
	return 0;
}

void DisconnectFromIbmsServer( struct IbmaEnv *p_env )
{
	INFOLOGSG( "close[%d]" , p_env->netaddr.sock );
	ResetHttpEnv( p_env->http );
	ResetHttpSecureEnv( p_env->http_secure_env );
	close( p_env->netaddr.sock ); p_env->netaddr.sock = -1 ;
	
	return;
}

