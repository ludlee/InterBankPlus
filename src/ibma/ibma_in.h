#ifndef _H_IBMA_IN_
#define _H_IBMA_IN_

#include "ibp_util.h"

#include "IDL_ibma_conf.dsc.h"

extern char     *g_ibp_ibma_kernel_version ;

#define URI_FETCH_CONFIG	"/fetch_config"
#define URI_DOWNLOAD_CONFIG	"/download_config"

struct IndexSpace
{
	struct ShareMemory		shm ;
	
	struct IndexSpaceAttaching	*index_space_attaching ;
} ;

struct ConfigSpace
{
	struct ShareMemory		shm ;
	
	struct ConfigSpaceAttaching	*config_space_attaching ;
	struct ConfigSpaceAddresses	config_space_addresses ;
	
	struct list_head		this_node ;
} ;

struct CommandParameter
{
	char			*_action ;
	char			*_show ;
} ;

struct IbmaEnv
{
	char			*ibma_conf_filename ;
	char			ibma_conf_pathfilename[ IBP_MAXLEN_FILENAME + 1 ] ;
	
	ibma_conf		ibma_conf ;
	struct CommandParameter	cmd_param ;
	
	struct NetAddress	netaddr ;
	struct HttpEnv		*http ;
	struct HttpSecureEnv	*http_secure_env ;
	
	struct IndexSpace	index_space ;
	int			current_proj_id ;
	struct ConfigSpace	config_space_list ;
	struct ConfigSpace	*p_current_config_space ;
} ;

int LoadConfig( char *config_pathfilename , struct IbmaEnv *p_env );

int InitEnvironment( struct IbmaEnv *p_env );
void CleanEnvironment( struct IbmaEnv *p_env );

int _worker( void *pv );

int ConnectToIbmsServer( struct IbmaEnv *p_env );
int FetchConfigCopy( struct IbmaEnv *p_env );
void DisconnectFromIbmsServer( struct IbmaEnv *p_env );

int OnProcess( struct IbmaEnv *p_env );
int OnProcessBuildConfigSpace( struct IbmaEnv *p_env , struct HttpBuffer *b );
#if 0
int OnProcessAliveNotice( struct IbmaEnv *p_env );
#endif

int ShowThisNode( char *config_filename );
int ShowNodes( char *config_filename );
int ShowApps( char *config_filename );

#endif

