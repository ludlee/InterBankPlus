#include "ibma_in.h"

#define PREFIX_DSCLOG_ibma_conf			INFOLOGSG( 
#define NEWLINE_DSCLOG_ibma_conf	
#include "IDL_ibma_conf.dsc.LOG.c"

int LoadConfig( char *config_pathfilename , struct IbmaEnv *p_env )
{
	char		*file_content = NULL ;
	int		file_len ;
	
	int		nret = 0 ;
	
	file_content = StrdupEntireFile( config_pathfilename , & file_len ) ;
	if( file_content == NULL )
	{
		ERRORLOGSG( "StrdupEntireFile[%s] failed" , config_pathfilename );
		return -1;
	}
	
	nret = DSCDESERIALIZE_JSON_ibma_conf( "GB18030" , file_content , & file_len , & (p_env->ibma_conf) ) ;
	free( file_content );
	if( nret )
	{
		ERRORLOGSG( "DSCDESERIALIZE_JSON_ibms_conf[%s] failed[%d]" , config_pathfilename , nret );
		return -1;
	}
	
	DSCLOG_ibma_conf( & (p_env->ibma_conf) );
	
	return 0;
}

