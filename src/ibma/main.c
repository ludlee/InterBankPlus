#include "ibma_in.h"

char		*g_ibp_ibma_kernel_version = IBP_VERSION ;

static void version()
{
	printf( "ibma v%s build %s %s ( ibidl v%s, ibutil v%s )\n" , g_ibp_ibma_kernel_version , __DATE__ , __TIME__ , g_ibp_idl_kernel_version , g_ibp_util_kernel_version );
	printf( "Copyright by calvin 2017\n" );
	printf( "Email : calvinwilliams@163.com\n" );
	return;
}

static void usage()
{
	printf( "USAGE : ibma -f ibma.conf -a [ start ]\n" );
	printf( "                          -s this_node\n" );
	printf( "                          -s [ nodes | apps ]\n" );
	printf( "kill -USR1 : reload log\n" );
	printf( "kill -USR2 : stop with removing config space\n" );
	return;
}

int main( int argc , char *argv[] )
{
	struct IbmaEnv	*p_env = NULL ;
	int		i ;
	
	int		nret = 0 ;
	
	if( argc > 1 )
	{
		p_env = (struct IbmaEnv *)malloc( sizeof(struct IbmaEnv) ) ;
		if( p_env == NULL )
		{
			printf( "malloc failed , errno[%d]\n" , errno );
			return 1;
		}
		memset( p_env , 0x00 , sizeof(struct IbmaEnv) );
		
		for( i = 1 ; i < argc ; i++ )
		{
			if( strcmp( argv[i] , "-v" ) == 0 )
			{
				version();
				exit(0);
			}
			else if( strcmp( argv[i] , "-f" ) == 0 && i + 1 < argc )
			{
				p_env->ibma_conf_filename = argv[++i] ;
			}
			else if( strcmp( argv[i] , "-a" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param._action = argv[++i] ;
			}
			else if( strcmp( argv[i] , "-s" ) == 0 && i + 1 < argc )
			{
				p_env->cmd_param._show = argv[++i] ;
			}
			else
			{
				printf( "Invalid opt[%s]\r\n" , argv[i] );
				usage();
				exit(7);
			}
		}
		
		if( p_env->ibma_conf_filename == NULL )
			p_env->ibma_conf_filename = "ibma.conf" ;
		
		if( p_env->cmd_param._action && STRCMP( p_env->cmd_param._action , == , "start" ) )
		{
			memset( p_env->ibma_conf_pathfilename , 0x00 , sizeof(p_env->ibma_conf_pathfilename) );
			snprintf( p_env->ibma_conf_pathfilename , sizeof(p_env->ibma_conf_pathfilename)-1 , "%s/etc/%s" , getenv("HOME") , p_env->ibma_conf_filename );
			
			nret = LoadConfig( p_env->ibma_conf_pathfilename , p_env ) ;
			if( nret )
			{
				printf( "LoadConfig[%s] failed[%d]\n" , p_env->ibma_conf_pathfilename , nret );
				return 1;
			}
			
			IBPCleanLogEnv();
			IBPInitLogEnv( p_env->ibma_conf.ibma.log.iblog_server , "ibma.main" , p_env->ibma_conf.ibma.log.event_output , IBPConvertLogLevel(p_env->ibma_conf.ibma.log.main_loglevel) , "%s" , p_env->ibma_conf.ibma.log.main_output );
			
			nret = InitEnvironment( p_env ) ;
			if( nret )
			{
				printf( "InitEnvironment failed[%d]\n" , nret );
				return 1;
			}
			
			nret = ConnectToIbmsServer( p_env ) ;
			if( nret )
			{
				printf( "ConnectIbmsServer failed[%d]\n" , nret );
				return 1;
			}
			
			nret = OnProcessBuildConfigSpace( p_env , GetHttpResponseBuffer(p_env->http) ) ;
			if( nret != HTTP_OK )
			{
				printf( "OnProcessBuildConfigSpace failed[%d]" , nret );
				return 1;
			}
			
			SetHttpNonblock( p_env->netaddr.sock );
			SetHttpTimeout( p_env->http , -1 );
			ResetHttpEnv( p_env->http );
			ResetHttpSecureEnv( p_env->http_secure_env );
			
			nret = BindDaemonServer( & _worker , p_env ) ;
			if( nret < 0 )
			{
				printf( "BindDaemonServer failed[%d]\n" , nret );
			}
			
			CleanEnvironment( p_env );
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "this_node" ) )
		{
			ShowThisNode( p_env->ibma_conf_filename );
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "nodes" ) )
		{
			ShowNodes( p_env->ibma_conf_filename );
		}
		else if( p_env->cmd_param._show && STRCMP( p_env->cmd_param._show , == , "apps" ) )
		{
			ShowApps( p_env->ibma_conf_filename );
		}
		else
		{
			printf( "Invalid parameters\n" );
			usage();
			exit(7);
		}
		
		free( p_env );
	}
	else
	{
		usage();
		return 7;
	}
	
	return -nret;
}

