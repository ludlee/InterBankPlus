#ifndef _H_IBMA_API_
#define _H_IBMA_API_

#include "ibp_util.h"

extern char		*g_ibp_ibma_api_kernel_version ;

/*
 * config space
 */

struct IbmaConfigSpace ;

struct IbmaConfigSpace *IBMAOpenConfigSpace( char *ibma_config_filename );
int IBMACheckConfigSpaceAttaching( struct IbmaConfigSpace *ibma_config_space );
void IBMACloseConfigSpace( struct IbmaConfigSpace *ibma_config_space );

int IBMAGetRetryConnectTimeval( struct IbmaConfigSpace *ibma_config_space );

int IBMAGetNodeCount( struct IbmaConfigSpace *ibma_config_space );
char *IBMAGetThisNodePtr( struct IbmaConfigSpace *ibma_config_space );
struct NodeSpaceUnit *IBMAGetThisNode( struct IbmaConfigSpace *ibma_config_space );
struct NodeSpaceUnit *IBMATravelNodes( struct IbmaConfigSpace *ibma_config_space , struct NodeSpaceUnit *p_node_unit );
struct NodeSpaceUnit *IBMAQueryNode( struct IbmaConfigSpace *ibma_config_space , char *node );
char *IBMAGetNodePtr( struct NodeSpaceUnit *p_node_unit );
char IBMAGetNodeInvalid( struct NodeSpaceUnit *p_node_unit );
int IBMAGetNodeHostCount( struct NodeSpaceUnit *p_node_unit );
char *IBMAGetNodeKey( struct NodeSpaceUnit *p_node_unit , int *p_key_len );

struct HostSpaceUnit *IBMATravelNodeHosts( struct IbmaConfigSpace *ibma_config_space , struct NodeSpaceUnit *p_node_unit , struct HostSpaceUnit *p_host_unit );
struct HostSpaceUnit *IBMAQueryNodeHost( struct IbmaConfigSpace *ibma_config_space , struct NodeSpaceUnit *p_node_unit , char *ip , int port );
char *IBMAGetHostIpPtr( struct HostSpaceUnit *p_host_unit );
int IBMAGetHostPort( struct HostSpaceUnit *p_host_unit ); 
int IBMAGetHostLoad( struct HostSpaceUnit *p_host_unit );
#if 0
char IBMAGetHostInvalid( struct HostSpaceUnit *p_host_unit );
#endif
 
int IBMAGetAppCount( struct IbmaConfigSpace *ibma_config_space );
struct AppSpaceUnit *IBMATravelApps( struct IbmaConfigSpace *ibma_config_space , struct AppSpaceUnit *p_app_unit );
struct AppSpaceUnit *IBMAQueryApp( struct IbmaConfigSpace *ibma_config_space , char *app );
char *IBMAGetAppPtr( struct AppSpaceUnit *p_app_unit );
char *IBMAGetAppDescPtr( struct AppSpaceUnit *p_app_unit );
char *IBMAGetAppBinPtr( struct AppSpaceUnit *p_app_unit );
int IBMAGetAppTimeout( struct AppSpaceUnit *p_app_unit );
int IBMAGetAppTimeout2( struct AppSpaceUnit *p_app_unit );

#endif

