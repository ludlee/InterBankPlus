#include "ibmsgv_api.h"

#include "IDL_test_ibptps1_request.dsc.h"
#include "IDL_test_ibptps1_response.dsc.h"

static int TESTTPS1( char *node )
{
	test_ibptps1_request	req_msg ;
	test_ibptps1_response	rsp_msg ;
	
	int			nret = 0 ;
	
	memset( & req_msg , 0x00 , sizeof(test_ibptps1_request) );
	strcpy( req_msg.test_request_header.trans_code , "TESTTPS1" );
	strcpy( req_msg.test_ibptps1.account_no , "603367123412341234" );
	memset( & rsp_msg , 0x00 , sizeof(test_ibptps1_response) );
	nret = IBMSGVCall_JSON( NULL , node , "TESTTPS1" , (void*) & req_msg , & DSCSERIALIZE_JSON_DUP_test_ibptps1_request_V , (void*) & rsp_msg , & DSCDESERIALIZE_JSON_test_ibptps1_response_V , NULL , NULL ) ;
	if( nret )
	{
		printf( "IBMSGVCall failed[%d] , errno[%d]\n" , nret , errno );
		return -1;
	}
	else
	{
		printf( "IBMSGVCall ok\n" );
	}
	
	printf( "response_code[%s] response_desc[%s] - account_no[%s] balance[%.2lf]\n"
		, rsp_msg.test_response_header.response_code , rsp_msg.test_response_header.response_desc
		, rsp_msg.test_ibptps1.account_no , rsp_msg.test_ibptps1.balance );
	
	return 0;
}

static void usage()
{
	printf( "USAGE : TESTTPS1 node\n" );
	return;
}

int main( int argc , char *argv[] )
{
	int		nret = 0 ;
	
	if( argc == 1 + 1 )
	{
		IBPInitLogEnv( "" , "TESTTPS1" , "file::log/event.log" , LOG_LEVEL_DEBUG , "file::log/TESTTPS1.log" );
		nret = TESTTPS1( argv[1] ) ;
		IBPCleanLogEnv();
		return -nret;
	}
	else
	{
		usage();
		exit(7);
	}
}

