#include "ibac_api.h"

static int TESTFILE( char *node , char *msg )
{
	struct IbacEnv		*p_env = NULL ;
	FILE			*fp = NULL ;
	char			tmp_req_filename1[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_req_filename2[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_req_filename3[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_req_filename4[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_rsp_filename1[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_rsp_filename2[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_rsp_filename3[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			tmp_rsp_filename4[ IBP_MAXLEN_FILENAME + 1 ] ;
	char			*p_msg = NULL ;
	int			msg_len ;
	
	int			nret = 0 ;
	
	p_env = IBACCreateEnvirment( NULL ) ;
	if( p_env == NULL )
	{
		printf( "IBACCreateEnvirment failed , errno[%d]\n" , errno );
		return -1;
	}
	else
	{
		printf( "IBACCreateEnvirment ok\n" );
	}
	
	memset( tmp_req_filename1 , 0x00 , sizeof(tmp_req_filename1) );
	fp = IBPCreateTempFile( "AFILE" , tmp_req_filename1 , NULL , "wb" ) ;
	if( fp == NULL )
	{
		printf( "IBPCreateTempFile failed , errno[%d]\n" , errno );
		return -1;
	}
	else
	{
		printf( "IBPCreateTempFile ok , tmp_req_filename1[%s]\n" , tmp_req_filename1 );
		fclose( fp );
	}
	
	memset( tmp_req_filename2 , 0x00 , sizeof(tmp_req_filename2) );
	fp = IBPCreateTempFile( "BFILE" , tmp_req_filename2 , NULL , "wb" ) ;
	if( fp == NULL )
	{
		printf( "IBPCreateTempFile failed , errno[%d]\n" , errno );
		return -1;
	}
	else
	{
		printf( "IBPCreateTempFile ok , tmp_req_filename2[%s]\n" , tmp_req_filename2 );
		fprintf( fp , "2\n" );
		fclose( fp );
	}
	
	memset( tmp_req_filename3 , 0x00 , sizeof(tmp_req_filename3) );
	fp = IBPCreateTempFile( "CFILE" , tmp_req_filename3 , NULL , "wb" ) ;
	if( fp == NULL )
	{
		printf( "IBPCreateTempFile failed , errno[%d]\n" , errno );
		return -1;
	}
	else
	{
		char	buf[ 1023 ] ;
		printf( "IBPCreateTempFile ok , tmp_req_filename3[%s]\n" , tmp_req_filename3 );
		memset( buf , '3' , sizeof(buf) );
		fwrite( buf , sizeof(buf) , 1 , fp );
		fclose( fp );
	}
	
	memset( tmp_req_filename4 , 0x00 , sizeof(tmp_req_filename4) );
	fp = IBPCreateTempFile( "DFILE" , tmp_req_filename4 , NULL , "wb" ) ;
	if( fp == NULL )
	{
		printf( "IBPCreateTempFile failed , errno[%d]\n" , errno );
		return -1;
	}
	else
	{
		char	buf[ 1025 ] ;
		int	i ;
		printf( "IBPCreateTempFile ok , tmp_req_filename4[%s]\n" , tmp_req_filename4 );
		memset( buf , '4' , sizeof(buf) );
		for( i = 0 ; i < 1025 ; i++ )
			fwrite( buf , sizeof(buf) , 1 , fp );
		fclose( fp );
	}
	
	memset( tmp_rsp_filename1 , 0x00 , sizeof(tmp_rsp_filename1) );
	memset( tmp_rsp_filename2 , 0x00 , sizeof(tmp_rsp_filename2) );
	memset( tmp_rsp_filename3 , 0x00 , sizeof(tmp_rsp_filename3) );
	memset( tmp_rsp_filename4 , 0x00 , sizeof(tmp_rsp_filename4) );
	
	p_msg = msg ;
	msg_len = strlen(msg) ;
	nret = IBACRequester( p_env , node , "TESTFILE" , & p_msg , & msg_len , tmp_req_filename1 , tmp_req_filename2 , tmp_req_filename3 , tmp_req_filename4 , NULL , tmp_rsp_filename1 , tmp_rsp_filename2 , tmp_rsp_filename3 , tmp_rsp_filename4 , NULL ) ;
	if( nret )
	{
		printf( "IBACRequester failed[%d] , errno[%d]\n" , nret , errno );
		return -1;
	}
	else
	{
		printf( "IBACRequester ok , msg[%.*s]\n" , msg_len , p_msg );
		printf( "tmp_rsp_filename1[%s]\n" , tmp_rsp_filename1 );
		printf( "tmp_rsp_filename2[%s]\n" , tmp_rsp_filename2 );
		printf( "tmp_rsp_filename3[%s]\n" , tmp_rsp_filename3 );
		printf( "tmp_rsp_filename4[%s]\n" , tmp_rsp_filename4 );
	}
	
	IBACDestroyEnvirment( p_env );
	printf( "IBACDestroyEnvirment ok\n" );
	
	return 0;
}

static void usage()
{
	printf( "USAGE : TESTFILE node msg\n" );
	return;
}

int main( int argc , char *argv[] )
{
	int		nret = 0 ;
	
	if( argc == 1 + 2 )
	{
		IBPInitLogEnv( "" , "TESTFILE" , "file::log/event.log" , LOG_LEVEL_DEBUG , "file::log/TESTFILE.log" );
		nret = TESTFILE( argv[1] , argv[2] ) ;
		IBPCleanLogEnv();
		return -nret;
	}
	else
	{
		usage();
		exit(7);
	}
}

