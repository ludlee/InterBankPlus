#include "ibac_api.h"

static int TESTECHO( char *node , char *msg )
{
	struct IbacEnv		*p_env = NULL ;
	
	char			*p_msg = NULL ;
	int			msg_len ;
	
	int			nret = 0 ;
	
	p_env = IBACCreateEnvirment( NULL ) ;
	if( p_env == NULL )
	{
		printf( "IBACCreateEnvirment failed , errno[%d]\n" , errno );
		return -1;
	}
	else
	{
		printf( "IBACCreateEnvirment ok\n" );
	}
	
	p_msg = msg ;
	msg_len = strlen(msg) ;
	nret = IBACRequester( p_env , node , "TESTECHO" , & p_msg , & msg_len , NULL , NULL ) ;
	if( nret )
	{
		printf( "IBACRequester failed[%d] , errno[%d]\n" , nret , errno );
		return -1;
	}
	else
	{
		printf( "IBACRequester ok , msg[%.*s]\n" , msg_len , p_msg );
	}
	
	IBACDestroyEnvirment( p_env );
	printf( "IBACDestroyEnvirment ok\n" );
	
	return 0;
}

static void usage()
{
	printf( "USAGE : TESTECHO node msg\n" );
	return;
}

int main( int argc , char *argv[] )
{
	int		nret = 0 ;
	
	if( argc == 1 + 2 )
	{
		IBPInitLogEnv( "" , "TESTECHO" , "file::log/event.log" , LOG_LEVEL_DEBUG , "file::log/TESTECHO.log" );
		nret = TESTECHO( argv[1] , argv[2] ) ;
		IBPCleanLogEnv();
		return -nret;
	}
	else
	{
		usage();
		exit(7);
	}
}

