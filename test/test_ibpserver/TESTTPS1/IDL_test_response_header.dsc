	STRUCT	test_response_header			# 响应公共头
	{
		STRING 10	sender_date		# 发起方日期
		STRING 8	sender_time		# 发起方时间
		STRING 20	sender_trans_no		# 发起方流水号
		STRING 10	receiver_date		# 接收方日期
		STRING 8	receiver_time		# 接收方时间
		STRING 20	receiver_trans_no	# 接收方流水号
		STRING 10	response_code		# 交易结果
		STRING 128	response_desc		# 交易结果描述
		STRING 128	response_message	# 额外信息
	}

