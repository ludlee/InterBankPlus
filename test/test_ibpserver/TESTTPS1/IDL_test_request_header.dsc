	STRUCT	test_request_header		# 请求公共头
	{
		STRING 10	sender_date	# 发起方日期
		STRING 8	sender_time	# 发起方时间
		STRING 20	sender_trans_no	# 发起方流水号
		STRING 6	channel_code	# 交易渠道
		STRING 10	branch_no	# 交易网点
		STRING 12	oper_no		# 交易柜员
		STRING 32	login_session	# 签到会话
		STRING 20	trans_code	# 交易码
	}

