#include "ibtm_api.h"

TransFunc TF_QUERY_BRANCH ;
int TF_QUERY_BRANCH( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_IS_BRANCH_LOGINED ;
int TF_IS_BRANCH_LOGINED( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_QUERY_OPER ;
int TF_QUERY_OPER( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_IS_OPER_LOGINED ;
int TF_IS_OPER_LOGINED( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_VERIFY_OPER_SESSION ;
int TF_VERIFY_OPER_SESSION( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

struct TransFuncArray	TFA_IS_BRANCH_LOGINED[] =
	{
		FUNC( "查询网点信息" , TF_QUERY_BRANCH )
		FUNC( "检查网点签到状态" , TF_IS_BRANCH_LOGINED )
		RETURN
	} ;

struct TransFuncArray	TFA_IS_OPER_LOGINED_AND_SESSION_VALID[] =
	{
		FUNC( "查询操作员信息" , TF_QUERY_OPER )
		FUNC( "检查操作员签到状态" , TF_IS_OPER_LOGINED )
		FUNC( "检查操作员会话" , TF_VERIFY_OPER_SESSION )
		RETURN
	} ;

struct TransFuncArray	TFA_PC_IS_LOGINED[] =
	{
		FUNCARRAY( "检查网点签到" , TFA_IS_BRANCH_LOGINED )
		FUNCARRAY( "检查操作员签到和会话" , TFA_IS_OPER_LOGINED_AND_SESSION_VALID )
		RETURN
	} ;

