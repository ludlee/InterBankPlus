#include "ibtm_api.h"

TransFunc TF_AT_TESTTPS1 ;
int TF_AT_TESTTPS1( struct TransProcessEnv *p_tpe )
{
	test_ibptps1_response	*p_rsp_st = NULL ;
	
	int			nret = 0 ;
	
	nret = IBTMGetStructBlock( p_tpe , "test_ibptps1_response" , sizeof(test_ibptps1_response) , (void**) & p_rsp_st ) ;
	if( nret )
	{
		ERRORLOGSG( "IBTMGetStructBlock failed[%d]" , nret );
		return nret;
	}
	
	IBTMFormatResponseCode( p_tpe , p_rsp_st->test_response_header.response_code , sizeof(p_rsp_st->test_response_header.response_code) );
	strcpy( p_rsp_st->test_response_header.response_desc , "���׳ɹ�" );
	
	return 0;
}

