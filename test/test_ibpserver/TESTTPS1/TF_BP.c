#include "ibtm_api.h"

TransFunc TF_BP_TESTTPS1_F0 ;
int TF_BP_TESTTPS1_F0( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_BP_TESTTPS1_F1 ;
int TF_BP_TESTTPS1_F1( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_BP_TESTTPS1_F2 ;
int TF_BP_TESTTPS1_F2( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_BP_TESTTPS1_F3 ;
int TF_BP_TESTTPS1_F3( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_BP_TESTTPS1_F4 ;
int TF_BP_TESTTPS1_F4( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_BP_TESTTPS1_F5 ;
int TF_BP_TESTTPS1_F5( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

TransFunc TF_BP_TESTTPS1_F6 ;
int TF_BP_TESTTPS1_F6( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 1;
}

TransFunc TF_BP_TESTTPS1_F7 ;
int TF_BP_TESTTPS1_F7( struct TransProcessEnv *p_tpe )
{
	INFOLOGG( "..." );
	
	return 0;
}

void do_coredump( struct TransProcessEnv *p_tpe )
{
#if 0
	char	*p = NULL ;
	strcpy( p , "let's core" );
#endif
	
	return;
}

TransFunc TF_BP_TESTTPS1_F8 ;
int TF_BP_TESTTPS1_F8( struct TransProcessEnv *p_tpe )
{
	test_ibptps1_request	*p_req_msg = NULL ;
	test_ibptps1_response	*p_rsp_msg = NULL ;
	
	int			nret = 0 ;
	
	nret = IBTMGetStructBlock( p_tpe , "test_ibptps1_request" , sizeof(test_ibptps1_request) , (void**) & p_req_msg ) ;
	if( nret )
	{
		ERRORLOGG( "IBTMGetStructBlock failed[%d]" , nret );
		return 1;
	}
	
	nret = IBTMGetStructBlock( p_tpe , "test_ibptps1_response" , sizeof(test_ibptps1_response) , (void**) & p_rsp_msg ) ;
	if( nret )
	{
		ERRORLOGG( "IBTMGetStructBlock failed[%d]" , nret );
		return 1;
	}
	
	do_coredump( p_tpe );
	
	strcpy( p_rsp_msg->test_ibptps1.account_no , p_req_msg->test_ibptps1.account_no );
	p_rsp_msg->test_ibptps1.balance = 1000.00 ;
	
	return 0;
}

