/* It had generated by DirectStruct v1.7.0 */
#include "IDL_test_ibptps1_response.dsc.h"

#ifndef FUNCNAME_DSCLOG_test_ibptps1_response
#define FUNCNAME_DSCLOG_test_ibptps1_response	DSCLOG_test_ibptps1_response
#endif

#ifndef PREFIX_DSCLOG_test_ibptps1_response
#define PREFIX_DSCLOG_test_ibptps1_response	printf( 
#endif

#ifndef NEWLINE_DSCLOG_test_ibptps1_response
#define NEWLINE_DSCLOG_test_ibptps1_response	"\n"
#endif

int FUNCNAME_DSCLOG_test_ibptps1_response( test_ibptps1_response *pst )
{
	int	index[10] = { 0 } ; index[0] = 0 ;
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.sender_date[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.sender_date );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.sender_time[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.sender_time );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.sender_trans_no[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.sender_trans_no );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.receiver_date[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.receiver_date );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.receiver_time[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.receiver_time );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.receiver_trans_no[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.receiver_trans_no );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.response_code[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.response_code );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.response_desc[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.response_desc );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_response_header.response_message[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_response_header.response_message );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_ibptps1.account_no[%s]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_ibptps1.account_no );
		PREFIX_DSCLOG_test_ibptps1_response "test_ibptps1_response.test_ibptps1.balance[%.2lf]" NEWLINE_DSCLOG_test_ibptps1_response , pst->test_ibptps1.balance );
	return 0;
}
