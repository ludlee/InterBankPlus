#include "ibmsgv_api.h"
#include "ibtm_api.h"
#include "ibtps_BT_PC_BP_AP_AT.h"

#include "IDL_test_ibptps1_request.dsc.h"
#include "IDL_test_ibptps1_response.dsc.h"

#include "TFA_PC_COMMON.c"

#include "TF_BT.c"
#include "TF_PC.c"
#include "TF_BP.c"
#include "TF_AT.c"

struct TransFuncArray	TFA_BT_TESTTPS1[] =
	{
		FUNC( "交易前处理1" , TF_BT_TESTTPS1 )
		RETURN
	} ;

struct TransFuncArray	TFA_PC_TESTTPS1[] =
	{
		FUNC( "公共检查前数据准备" , TF_PC_TESTTPS1 )
		FUNCARRAY( "公共检查" , TFA_PC_IS_LOGINED )
		RETURN
	} ;

struct TransFuncArray	TFA_BP_TESTTPS1_FA0[] =
	{
		FUNC( "业务处理函数0" , TF_BP_TESTTPS1_F0 )
		RETURN
	} ;

struct TransFuncArray	TFA_BP_TESTTPS1_FA2[] =
	{
		FUNC( "业务处理函数2" , TF_BP_TESTTPS1_F2 )
		RETURN
	} ;

struct TransFuncArray	TFA_BP_TESTTPS1_FA6[] =
	{
		FUNC( "业务处理函数6" , TF_BP_TESTTPS1_F6 )
		RETURN
	} ;

struct TransFuncArray	TFA_BP_TESTTPS1[] =
	{
		FUNC( "业务处理函数1" , TF_BP_TESTTPS1_F1 )
		FUNCARRAY( "业务处理函数数组2" , TFA_BP_TESTTPS1_FA2 )
		IF_IN_TRANSCODES_THEN_FUNC( "业务处理函数数组3" , "TESTTPS1" , TF_BP_TESTTPS1_F3 )
		IF_NOTIN_TRANSCODES_THEN_FUNCARRAY( "业务处理函数数组0" , "TESTTPS1" , TFA_BP_TESTTPS1_FA0 )
		IF_FUNC_THEN( "业务处理函数4" , TF_BP_TESTTPS1_F4 )
			FUNC( "业务处理函数5" , TF_BP_TESTTPS1_F5 )
		ELSE
			FUNC( "业务处理函数0" , TF_BP_TESTTPS1_F0 )
		ENDIF
		IF_FUNCARRAY_THEN( "业务处理函数数组6" , TFA_BP_TESTTPS1_FA6 )
			FUNC( "业务处理函数0" , TF_BP_TESTTPS1_F0 )
		ELSE
			FUNC( "业务处理函数7" , TF_BP_TESTTPS1_F7 )
		ENDIF
		FUNC( "业务处理函数8" , TF_BP_TESTTPS1_F8 )
		RETURN
	} ;

struct TransFuncArray	TFA_AT_TESTTPS1[] =
	{
		FUNC( "交易后处理1" , TF_AT_TESTTPS1 )
		RETURN
	} ;

struct TransProcessSchedule_BT_PC_BP_AP_AT	TPS_TESTTPS1 =
	{
		"交易前处理" , TFA_BT_TESTTPS1 ,
		"公共检查" , TFA_PC_TESTTPS1 ,
		"业务处理" , TFA_BP_TESTTPS1 ,
		"账务处理" , NULL ,
		"交易后处理" , TFA_AT_TESTTPS1 
	} ;

funcIbmsgvTransmain transmain ;
int transmain( void *pv_req_st , void *pv_rsp_st , struct IbasAddonFiles *addon_files )
{
	struct TransProcessEnv	*p_tpe = NULL ;
	
	test_ibptps1_request	*p_req_st = (test_ibptps1_request *) pv_req_st ;
	
	int			nret = 0 ;
	
	p_tpe = IBTMCreateTransProcessEnv( "test_ibptps1_request" , sizeof(test_ibptps1_request) , pv_req_st
					, "test_ibptps1_response" , sizeof(test_ibptps1_response) , pv_rsp_st
					, addon_files
					, p_req_st->test_request_header.trans_code ) ;
	if( p_tpe == NULL )
	{
		ERRORLOGSG( "CreateTransProcessEnv failed[%d]" , nret );
		return -1;
	}
	
	nret = IBTPSExecuteSchedule_BT_PC_BP_AP_AT( p_tpe , & TPS_TESTTPS1 ) ;
	if( nret )
	{
		ERRORLOGSG( "IBTPSExecuteSchedule_BT_PC_BP_AP_AT failed[%d]" , nret );
	}
	else
	{
		INFOLOGSG( "IBTPSExecuteSchedule_BT_PC_BP_AP_AT ok" );
	}
	
	IBTMDestroyTransProcessEnv( p_tpe );
	
	return nret;
	
	return 0;
}

