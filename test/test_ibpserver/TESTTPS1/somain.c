#include "ibmsgv_api.h"

#include "IDL_test_ibptps1_request.dsc.h"
#include "IDL_test_ibptps1_response.dsc.h"

funcIbmsgvTransmain transmain ;

funcIbasSomain somain ;
int somain( struct HttpBuffer *req_body , struct HttpBuffer *rsp_body , struct IbasAddonFiles *addon_files )
{
	test_ibptps1_request	req_st ;
	test_ibptps1_response	rsp_st ;
	
	int			nret = 0 ;
	
	INFOLOGSG( "ENTER TESTTPS1" );
	
	memset( & req_st , 0x00 , sizeof(test_ibptps1_request) );
	memset( & rsp_st , 0x00 , sizeof(test_ibptps1_response) );
	nret = IBMSGVReceiver_JSON( req_body , & DSCDESERIALIZE_JSON_test_ibptps1_request_V , & req_st
				, rsp_body , & DSCSERIALIZE_JSON_DUP_test_ibptps1_response_V , & rsp_st
				, & transmain , addon_files ) ;
	if( nret )
	{
		ERRORLOGSG( "IBMSGVReceiver_JSON failed[%d]" , nret );
	}
	else
	{
		INFOLOGSG( "IBMSGVReceiver_JSON ok" );
	}
	
	INFOLOGSG( "LEAVE TESTTPS1" );
	
	return nret;
}

