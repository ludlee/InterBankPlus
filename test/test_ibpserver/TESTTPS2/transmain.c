#include "ibmsgv_api.h"
#include "ibtm_api.h"
#include "ibtps_BT_PC_BP_AT_AP.h"

#include "IDL_test_ibptps1_request.dsc.h"
#include "IDL_test_ibptps1_response.dsc.h"

#include "TF_BT.c"
#include "TF_PC.c"
#include "TF_BP.c"
#include "TF_AT.c"
#include "TF_AP.c"

struct TransFuncArray	TFA_BT_TESTTPS2[] =
	{
		FUNC( "交易前处理2" , TF_BT_TESTTPS2 )
		RETURN
	} ;

struct TransFuncArray	TFA_PC_TESTTPS2[] =
	{
		FUNC( "公共检查2" , TF_PC_TESTTPS2 )
		RETURN
	} ;

struct TransFuncArray	TFA_BP_TESTTPS2[] =
	{
		FUNC( "业务处理函数2" , TF_BP_TESTTPS2 )
		RETURN
	} ;

struct TransFuncArray	TFA_AT_TESTTPS2[] =
	{
		FUNC( "交易后处理2" , TF_AT_TESTTPS2 )
		RETURN
	} ;

struct TransFuncArray	TFA_AP_TESTTPS2[] =
	{
		FUNC( "账务处理函数2" , TF_AP_TESTTPS2 )
		RETURN
	} ;

struct TransProcessSchedule_BT_PC_BP_AT_AP	TPS_TESTTPS2 =
	{
		"交易前处理" , TFA_BT_TESTTPS2 ,
		"公共检查" , TFA_PC_TESTTPS2 ,
		"业务处理" , TFA_BP_TESTTPS2 ,
		"交易后处理" , TFA_AT_TESTTPS2 ,
		"账务处理" , TFA_AP_TESTTPS2
	} ;

funcIbmsgvTransmain transmain ;
int transmain( void *pv_req_st , void *pv_rsp_st , struct IbasAddonFiles *addon_files )
{
	struct TransProcessEnv	*p_tpe = NULL ;
	
	test_ibptps1_request	*p_req_st = (test_ibptps1_request *) pv_req_st ;
	
	int			nret = 0 ;
	
	p_tpe = IBTMCreateTransProcessEnv( "test_ibptps1_request" , sizeof(test_ibptps1_request) , pv_req_st
					, "test_ibptps1_response" , sizeof(test_ibptps1_response) , pv_rsp_st
					, addon_files
					, p_req_st->test_request_header.trans_code ) ;
	if( p_tpe == NULL )
	{
		ERRORLOGSG( "CreateTransProcessEnv failed[%d]" , nret );
		return -1;
	}
	
	nret = IBTPSExecuteSchedule_BT_PC_BP_AT_AP( p_tpe , & TPS_TESTTPS2 ) ;
	if( nret )
	{
		ERRORLOGSG( "IBTPSExecuteSchedule_BT_PC_BP_AT_AP failed[%d]" , nret );
	}
	else
	{
		INFOLOGSG( "IBTPSExecuteSchedule_BT_PC_BP_AT_AP ok" );
	}
	
	IBTMDestroyTransProcessEnv( p_tpe );
	
	return nret;
}

