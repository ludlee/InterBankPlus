#include "ibtm_api.h"

TransFunc TF_AT_TESTTPS2 ;
int TF_AT_TESTTPS2( struct TransProcessEnv *p_tpe )
{
	test_ibptps1_response	*p_rsp_st = NULL ;
	
	int			nret = 0 ;
	
	nret = IBTMGetStructBlock( p_tpe , "test_ibptps1_response" , sizeof(test_ibptps1_response) , (void**) & p_rsp_st ) ;
	if( nret )
	{
		ERRORLOGSG( "IBTMGetStructBlock failed[%d]" , nret );
		return nret;
	}
	
	IBTMFormatResponseCode( p_tpe , p_rsp_st->test_response_header.response_code , sizeof(p_rsp_st->test_response_header.response_code) );
	strcpy( p_rsp_st->test_response_header.response_desc , "交易已受理" );
	
	nret = IBMSGVReceiver_JSON_SendResponseAhead( & DSCSERIALIZE_JSON_DUP_test_ibptps1_response_V , (void*) p_rsp_st ) ;
	if( nret )
	{
		ERRORLOGSG( "IBMSGVReceiver_JSON_SendResponseAhead failed[%d]" , nret );
		return nret;
	}
	else
	{
		INFOLOGSG( "IBMSGVReceiver_JSON_SendResponseAhead ok" );
	}
	
	return 0;
}

