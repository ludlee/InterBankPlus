#include "ibas_api.h"

funcIbasSomain somain ;
int somain( struct HttpBuffer *req_body , struct HttpBuffer *rsp_body , struct IbasAddonFiles *addon_files )
{
	char			*msg = NULL ;
	int			msg_len ;
	
	int			i ;
	int			count ;
	char			*p_filename = NULL ;
	
	int			nret = 0 ;
	
	INFOLOGSG( "ENTER TESTFILE" );
	
	/* 回射请求报文 */
	msg = GetHttpBufferBase( req_body , & msg_len ) ;
	nret = MemcatHttpBuffer( rsp_body , msg , msg_len ) ;
	if( nret )
	{
		ERRORLOGG( "MemcatHttpBuffer failed[%d] , errno[%d]" , nret , errno );
		return -1;
	}
	else
	{
		INFOHEXLOGG( msg , msg_len , "msg [%d]bytes" , msg_len );
	}
	
	/* 回射所有请求文件 */
	count = IBASGetRequestFileCount( addon_files ) ;
	for( i = 0 ; i < count ; i++ )
	{
		p_filename = IBASGetRequestFile( addon_files , i ) ;
		INFOLOGG( "FILE[%s] " , p_filename );
		
		nret = IBASPutResponseFile( addon_files , p_filename ) ;
		if( nret )
		{
			ERRORLOGG( "IBASPutResponseFile failed[%d]" , nret );
			return -1;
		}
	}
	
	INFOLOGSG( "LEAVE TESTFILE" );
	
	return nret;
}

